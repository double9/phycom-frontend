<?php

namespace Phycom\Frontend\Helpers;


use Phycom\Frontend\Models\Product\SearchProductCategory;

use yii\base\BaseObject;


/**
 * Class ProductCategoryHelper
 * @package Phycom\Frontend\Helpers
 */
class ProductCategoryHelper extends BaseObject
{
    /**
     * @param null $limit
     * @return \Phycom\Frontend\Models\Product\ProductCategoryDataProvider|\yii\data\ActiveDataProvider
     */
    public static function featured($limit = null)
    {
        $searchModel = new SearchProductCategory();
        $searchModel->featured = true;

        $dataProvider = $searchModel->search();
        if ($limit && $dataProvider->pagination) {
            $dataProvider->pagination->setPageSize($limit);
        }
        return $dataProvider;
    }
}
