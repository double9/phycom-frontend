<?php

namespace Phycom\Frontend\Helpers;

use Phycom\Frontend\Models\Post\PostDataProvider;

use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\PostCategory;
use Phycom\Base\Models\Post;

use yii\base\BaseObject;
use Yii;

/**
 * Class PostHelper
 * @package Phycom\Frontend\Helpers
 */
class PostHelper extends BaseObject
{
    /**
     * @return Post|null|yii\db\ActiveRecord
     */
    public static function landingPage()
    {
        $post = Post::find()
            ->where(['type' => PostType::LAND])
            ->andWhere(['status' => PostStatus::PUBLISHED])
            ->orderBy(['published_at' => SORT_DESC])
            ->one();

        return $post;
    }

    /**
     * @return Post|null|yii\db\ActiveRecord
     */
    public static function latestPost()
    {
        $post = Post::find()
            ->where(['type' => PostType::POST])
            ->andWhere(['status' => PostStatus::PUBLISHED])
            ->orderBy(['published_at' => SORT_DESC])
            ->one();

        return $post;
    }

    /**
     * @param PostCategory|string $key - post category model or url key
     * @param int $limit
     * @return PostDataProvider
     *
     * @throws yii\base\Exception
     */
    public static function searchByCategory($key, $limit = 5)
    {
        if (is_string($key)) {
            $category = PostCategory::findByUrlKey($key);
        } else if ($key instanceof PostCategory) {
            $category = $key;
        } else {
            throw new yii\base\InvalidArgumentException('Invalid key given. Expected string or ' . PostCategory::class);
        }

        $dataProvider = Yii::$app->modelFactory->getSearchPost([
            'type' => PostType::POST,
            'categoryId' => $category ? $category->id : -1
        ])->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }

    /**
     * @param int $limit
     * @return PostDataProvider
     * @throws yii\base\Exception
     */
    public static function latest($limit = 3)
    {
        $searchModel = Yii::$app->modelFactory->getSearchPost(['type' => PostType::POST]);
        $dataProvider = $searchModel->search();
        $dataProvider->sort->defaultOrder = ['published_at' => SORT_DESC];

        if ($limit) {
            $dataProvider->pagination->pageSize = $limit;
        }
        return $dataProvider;
    }
}
