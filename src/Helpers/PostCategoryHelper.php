<?php

namespace Phycom\Frontend\Helpers;

use Phycom\Frontend\Models\Post\PostCategoryDataProvider;
use Phycom\Frontend\Models\Post\PostArchiveDataProvider;
use Phycom\Frontend\Models\Post\SearchPostCategory;
use Phycom\Frontend\Models\Post\SearchPostArchive;

use Phycom\Base\Models\Attributes\CategoryStatus;

use yii\base\BaseObject;
use Yii;

/**
 * Class PostCategoryHelper
 * @package Phycom\Frontend\Helpers
 */
class PostCategoryHelper extends BaseObject
{
    /**
     * @param int|null $limit
     * @return PostCategoryDataProvider
     */
    public static function categories($limit = null)
    {
        $searchModel = new SearchPostCategory();
        $searchModel->language = Yii::$app->lang->current;
        $searchModel->topLevel = true;
        $searchModel->status = CategoryStatus::VISIBLE;
        $dataProvider = $searchModel->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = 5;
        }
        return $dataProvider;
    }

    /**
     * @param int|null $limit
     * @return PostArchiveDataProvider
     */
    public static function archives($limit = null)
    {
        $searchArchive = new SearchPostArchive();
        $dataProvider = $searchArchive->search();

        if ($limit) {
            $dataProvider->pagination->pageSize = 5;
        }
        return $dataProvider;
    }
}
