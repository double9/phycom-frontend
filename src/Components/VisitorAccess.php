<?php

namespace Phycom\Frontend\Components;


use Phycom\Base\Components\PhycomComponent;

use yii\base\BootstrapInterface;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class VisitorAccess
 * @package Phycom\Frontend\Components
 */
class VisitorAccess extends PhycomComponent implements BootstrapInterface
{
    /**
     * List of ip's that are whitelisted and should have access to our site
     * @var array
     */
    public array $ipWhitelist = [];

    /**
     * List of ip's that are blacklisted and should not have access to our site
     * @var array
     */
    public array $ipBlacklist = [];

    /**
     * List of country codes that are whitelisted and should have access to our site
     * @var array
     */
    public array $countryWhitelist = [];

    /**
     * List of country codes that are blacklisted and should not have access to our site
     * @var array
     */
    public array $countryBlacklist = [];

    /**
     * @var string|null - a controller action the rejected visitor is redirected
     */
    public ?string $blowOffRoute = '/site/blow-off';

    /**
     * @var string
     */
    public string $logCategory = 'invalid-request';

    /**
     * @var bool - if this is true we reject also cases where we can't find country from the ip
     */
    public bool $strict = false;

    /**
     * @param $isoCode
     * @return bool
     */
    public function isAllowed($ip, $isoCode): bool
    {
        if (!empty($this->ipWhitelist) && in_array($ip, $this->ipWhitelist)) {
            return true;
        }

        if (!empty($this->ipBlacklist) && in_array($ip, $this->ipBlacklist)) {
            return false;
        }

        if (false === $this->strict && is_null($isoCode)) {
            return true;
        }

        if (empty($this->countryBlacklist) && empty($this->countryWhitelist)) {
            return true;
        }

        if (!empty($this->countryWhitelist) && in_array($isoCode, $this->countryWhitelist)) {
            return true;
        }

        if (!empty($this->countryBlacklist) && in_array($isoCode, $this->countryBlacklist)) {
            return false;
        }

        return false;
    }

    public function bootstrap($app)
    {
        if ($this->isEnabled() && (!empty($this->countryBlacklist) || !empty($this->countryWhitelist))) {

            $ip = $app->request->getUserIP();
            $isoCode = $app->geoip->ip($ip)->isoCode;

            if (!$this->isAllowed($ip, $isoCode)) {
                Yii::warning('Access denied for ip ' . $ip . ' located in ' . $isoCode, $this->logCategory);

                if ($this->blowOffRoute) {
                    $blowOffRoute = '/' . ltrim($this->blowOffRoute, '/');
                    $app->response->redirect($blowOffRoute);
                    return false;
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            }
        }
    }
}
