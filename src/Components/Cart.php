<?php

namespace Phycom\Frontend\Components;

use Phycom\Base\Models\Product\Product;
use Phycom\Frontend\Widgets\CartButton;
use Phycom\Frontend\Widgets\CartTotal;
use Phycom\Frontend\Widgets\CartWidget;

use Phycom\Base\Components\BaseCart;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Base\Interfaces\CartItemDeliveryInterface;
use Phycom\Base\Interfaces\CartItemDiscountInterface;

use yii\helpers\ArrayHelper;

/**
 * Class Cart
 *
 * @package Phycom\Frontend\Components
 */
class Cart extends BaseCart
{
    /**
     * @var string CartItemProductInterface class name
     */
    const ITEM_PRODUCT = \Phycom\Base\Interfaces\CartItemProductInterface::class;

    /**
     * @var string CartItemDeliveryInterface class name
     */
    const ITEM_DELIVERY = \Phycom\Base\Interfaces\CartItemDeliveryInterface::class;

    /**
     * @var string CartItemDiscountInterface class name
     */
    const ITEM_DISCOUNT = \Phycom\Base\Interfaces\CartItemDiscountInterface::class;

    /**
     * @var array
     */
    public array $itemTypes;

    public function init()
    {
        parent::init();

        if (!isset($this->itemTypes)) {
            $this->itemTypes = [
                static::ITEM_PRODUCT  => 'product',
                static::ITEM_DELIVERY => 'delivery',
                static::ITEM_DISCOUNT => 'discount'
            ];
        }
    }

    /**
     * Returns all cart item type from the cart
     * @return CartItemProductInterface[]
     */
    public function getProductItems() : array
    {
        return $this->getItems(static::ITEM_PRODUCT);
    }

    /**
     * Returns all items of a delivery type from the cart
     * @return CartItemDeliveryInterface[]
     */
    public function getDeliveryItems() : array
    {
        return $this->getItems(static::ITEM_DELIVERY);
    }

    /**
     * Returns all items of a discount type from the cart
     * @return CartItemDiscountInterface[]
     */
    public function getDiscountItems() : array
    {
        return $this->getItems(static::ITEM_DISCOUNT);
    }

    /**
     * @return int|float
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this->getItems() as $item) {
            if ($item instanceof CartItemDeliveryInterface) {
                $total += $item->getPrice();
            } elseif ($item instanceof CartItemDiscountInterface) {
                $total -= $item->calculateDiscount();
            } else {
                $total += $item->getTotalPrice();
            }
        }
        return $total;
    }

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     *
     * @return int|float
     */
    public function getAttributeTotal(string $attribute, string $itemType = null)
    {
        $sum = 0;
        foreach ($this->getItems($itemType) as $model) {
            $sum += $model->{$attribute};
        }

        return $sum;
    }

    /**
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public function widget(array $options = [])
    {
        $options = ArrayHelper::merge([
            'options'      => ['id' => $this->getWidgetId()],
            'cart'         => $this,
            'rippleEffect' => ['color' => 'pink']
        ], $options);

        return CartWidget::widget($options);
    }

    /**
     * @param Product $product
     * @param string|null $label
     * @param array $widgetOptions
     * @return string
     * @throws \Exception
     */
    public function button(Product $product, string $label = null, array $widgetOptions = [])
    {
        if (!$this->isEnabled()) {
            return '';
        }
        $options = ArrayHelper::merge([
            'label'         => $label ?: '<i class="fas fa-shopping-cart mg-r-0"></i>',
            'cart'          => $this,
            'cartWidgetId'  => '#' . $this->getWidgetId(),
            'route'         => '/cart/add-product',
            'buttonOptions' => ['class' => 'btn btn-primary']
        ], $widgetOptions);

        $options['product'] = $product;

        return CartButton::widget($options);
    }

    /**
     * @param array $options
     * @return string
     */
    public function total(array $options = [])
    {
        return CartTotal::widget($options);
    }


    /**
     * @return string
     */
    protected function getWidgetId() : string
    {
        return static::ID . '-widget';
    }
}
