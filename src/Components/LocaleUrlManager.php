<?php

namespace Phycom\Frontend\Components;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class LocaleUrlManager
 * @package Phycom\Frontend\Components
 */
class LocaleUrlManager extends \codemix\localeurls\UrlManager
{
    public function init()
    {
        // set languages here if not configured
        if (empty($this->languages)) {
            $this->languages = ArrayHelper::getColumn(Yii::$app->lang->getEnabled(), 'code');
        }
        // fallback to default language
        if (empty($this->languages)) {
            $this->languages = [Yii::$app->lang->getDefault()->code];
        }
        parent::init();
    }
}
