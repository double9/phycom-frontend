<?php

namespace Phycom\Frontend\Components;


use Phycom\Frontend\Widgets\CartButton;
use Phycom\Frontend\Widgets\CartWidget;

use Phycom\Base\Components\BaseCart;
use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartItemInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Base\Models\Product\Product;

use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper;

/**
 * Class Wishlist
 *
 * @package Phycom\Frontend\Components
 */
class Wishlist extends BaseCart
{
    const ID = 'wishlist';
    /**
     * @param CartItemInterface $element
     * @param bool $save
     * @return CartInterface
     */
    public function add(CartItemInterface $element, bool $save = true): CartInterface
    {
        if (!$element instanceof CartItemProductInterface) {
            throw new InvalidCallException('Only products are allowed in wishlist');
        }
        return parent::add($element, $save);
    }

    /**
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public function widget(array $options = [])
    {
        $options = ArrayHelper::merge([
            'label'        => '<i class="fas fa-heart"></i>',
            'cart'         => $this,
            'checkoutUrl'  => ['/wishlist'],
            'options'      => ['id' => $this->getWidgetId()],
            'rippleEffect' => ['color' => 'pink']
        ], $options);

        return CartWidget::widget($options);
    }

    /**
     * @param Product $product
     * @param string|null $label
     * @param array $widgetOptions
     * @return string
     * @throws \Exception
     */
    public function button(Product $product, string $label = null, array $widgetOptions = [])
    {
        if (!$this->isEnabled()) {
            return '';
        }
        $options = ArrayHelper::merge([
            'label'               => $label ?: '<i class="fas fa-heart mg-r-0"></i>',
            'notAvailableMessage' => false,
            'cart'                => $this,
            'cartWidgetId'        => '#' . $this->getWidgetId(),
            'route'               => '/wishlist/add-product',
            'buttonOptions'       => ['class' => 'btn btn-inverse wishlist-btn']
        ], $widgetOptions);

        $options['product'] = $product;

        return CartButton::widget($options);
    }

    /**
     * @return string
     */
    protected function getWidgetId() : string
    {
        return static::ID . '-widget';
    }
}
