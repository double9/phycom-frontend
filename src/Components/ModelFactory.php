<?php

namespace Phycom\Frontend\Components;


use Phycom\Base\Models\Order;
use Phycom\Base\Models\Post;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\User;

/**
 * Class ModelFactory
 * @package Phycom\Frontend\Components
 *
 * @method \Phycom\Frontend\Models\Post\SearchPost getSearchPost(array $params = [])
 * @method \Phycom\Frontend\Models\Post\SearchPostArchive getSearchPostArchive(array $params = [])
 * @method \Phycom\Frontend\Models\Post\SearchPostCategory getSearchPostCategory(array $params = [])
 * @method \Phycom\Frontend\Models\Post\CommentForm getPostCommentForm(Post $post, array $params = [])
 * @method \Phycom\Frontend\Models\Post\PostDataProvider getPostDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Post\PostArchiveDataProvider getPostArchiveDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Post\PostAttachmentDataProvider getPostAttachmentDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Post\PostCategoryDataProvider getPostCategoryDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Post\PostCommentDataProvider getPostCommentDataProvider(array $params = [])
 *
 * @method \Phycom\Frontend\Models\Product\SearchProduct getSearchProduct(array $params = [])
 * @method \Phycom\Frontend\Models\Product\SearchProductCategory getSearchProductCategory(array $params = [])
 * @method \Phycom\Frontend\Models\Product\ProductDataProvider getProductDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Product\ProductCategoryDataProvider getProductCategoryDataProvider(array $params = [])
 * @method \Phycom\Frontend\Models\Product\ProductReviewForm getProductReviewForm(Product $product, array $params = [])
 * @method \Phycom\Frontend\Models\Product\ProductSelection getProductSelection(Product $product, array $params = [])
 *
 * @method \Phycom\Frontend\Widgets\Bootstrap4\ActiveForm getActiveForm(array $params = [])
 * @method \Phycom\Frontend\Widgets\Bootstrap4\Modal getModal(array $params = [])
 * @method \Phycom\Frontend\Assets\ButtonHelperAsset getButtonHelperAsset(array $params = [])
 *
 * @method \Phycom\Frontend\Models\OrderForm getOrderForm(User $user = null, Order $order = null, array $params = [])
 * @method \Phycom\Frontend\Models\CartForm getCartForm(Product $product, array $params = [])
 * @method \Phycom\Frontend\Models\SignupForm getSignupForm(array $params = [])
 * @method \Phycom\Frontend\Models\LoginForm getLoginForm(array $params = [])
 * @method \Phycom\Frontend\Models\PasswordResetForm getPasswordResetForm($token, array $params = [])
 * @method \Phycom\Frontend\Models\PasswordResetRequestForm getPasswordResetRequestForm(array $params = [])
 * @method \Phycom\Frontend\Models\PasswordChangeForm getPasswordChangeForm(User $user, array $params = [])
 * @method \Phycom\Frontend\Models\SubscriptionForm getSubscriptionForm(array $params = [])
 * @method \Phycom\Frontend\Models\TermsForm getTermsForm(array $params = [])
 * @method \Phycom\Frontend\Models\PromotionCodeForm getPromotionCodeForm(array $params = [])
 * @method \Phycom\Frontend\Models\UserProfileForm getUserProfileForm(User $user, array $params = [])
 * @method \Phycom\Frontend\Models\UserAddressForm getUserAddressForm(User $user, array $params = [])
 * @method \Phycom\Frontend\Models\ContactForm getContactForm(array $params = [])
 */
class ModelFactory extends \Phycom\Base\Components\ModelFactory
{
    protected array $definitions = [
        'getSearchPost'                 => 'Frontend\Models\Post\SearchPost',
        'getSearchPostArchive'          => 'Frontend\Models\Post\SearchPostArchive',
        'getSearchPostCategory'         => 'Frontend\Models\Post\SearchPostCategory',
        'getPostCommentForm'            => 'Frontend\Models\Post\CommentForm',
        'getPostDataProvider'           => 'Frontend\Models\Post\PostDataProvider',
        'getPostArchiveDataProvider'    => 'Frontend\Models\Post\PostArchiveDataProvider',
        'getPostAttachmentDataProvider' => 'Frontend\Models\Post\PostAttachmentDataProvider',
        'getPostCategoryDataProvider'   => 'Frontend\Models\Post\PostCategoryDataProvider',
        'getPostCommentDataProvider'    => 'Frontend\Models\Post\PostCommentDataProvider',

        'getSearchProduct'               => 'Frontend\Models\Product\SearchProduct',
        'getSearchProductCategory'       => 'Frontend\Models\Product\SearchProductCategory',
        'getProductDataProvider'         => 'Frontend\Models\Product\ProductDataProvider',
        'getProductCategoryDataProvider' => 'Frontend\Models\Product\ProductCategoryDataProvider',
        'getProductReviewForm'           => 'Frontend\Models\Product\ProductReviewForm',
        'getProductSelection'            => 'Frontend\Models\Product\ProductSelection',

        // todo: remove this section and use di container instead

        'getActiveForm'        => 'Frontend\Widgets\Bootstrap4\ActiveForm',
        'getModal'             => 'Frontend\Widgets\Bootstrap4\Modal',
        'getButtonHelperAsset' => 'Frontend\Assets\ButtonHelperAsset',

        'getOrderForm'                => 'Frontend\Models\OrderForm',
        'getCartForm'                 => 'Frontend\Models\CartForm',
        'getSignupForm'               => 'Frontend\Models\SignupForm',
        'getLoginForm'                => 'Frontend\Models\LoginForm',
        'getPasswordResetForm'        => 'Frontend\Models\PasswordResetForm',
        'getPasswordResetRequestForm' => 'Frontend\Models\PasswordResetRequestForm',
        'getPasswordChangeForm'       => 'Frontend\Models\PasswordChangeForm',
        'getSubscriptionForm'         => 'Frontend\Models\SubscriptionForm',
        'getTermsForm'                => 'Frontend\Models\TermsForm',
        'getPromotionCodeForm'        => 'Frontend\Models\PromotionCodeForm',
        'getUserProfileForm'          => 'Frontend\Models\UserProfileForm',
        'getUserAddressForm'          => 'Frontend\Models\UserAddressForm',
        'getContactForm'              => 'Frontend\Models\ContactForm',
    ];
}
