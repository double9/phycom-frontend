
window.DomHelper = {

    /**
     * Check if an element is out of the viewport
     * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
     *
     * @param {HTMLElement} targetNode
     * @return {Object} A set of booleans for each side of the element
     */
    isOutOfViewport: function (targetNode) {
        // Get element's bounding
        let bounding = targetNode.getBoundingClientRect();

        // Check if it's out of the viewport on each side
        let out = {};
        out.top = bounding.top < 0;
        out.left = bounding.left < 0;
        out.bottom = bounding.bottom > (window.innerHeight || document.documentElement.clientHeight);
        out.right = bounding.right > (window.innerWidth || document.documentElement.clientWidth);
        out.any = out.top || out.left || out.bottom || out.right;
        out.all = out.top && out.left && out.bottom && out.right;

        return out;
    },

    /**
     * Check if an element is visible
     *
     * @param {HTMLElement} targetNode
     * @returns {boolean|boolean}
     */
    isVisible: function (targetNode) {
        return targetNode.offsetWidth > 0 && targetNode.offsetHeight > 0
    },


    /**
     * Helper function to get an element's exact position
     *
     * @param {HTMLElement} targetNode
     * @returns {{x: number, y: number}}
     */
    getPosition: function (targetNode) {
        let xPos = 0;
        let yPos = 0;

        while (targetNode) {
            if (targetNode.tagName == "BODY") {
                // deal with browser quirks with body/window/document and page scroll
                let xScroll = targetNode.scrollLeft || document.documentElement.scrollLeft;
                let yScroll = targetNode.scrollTop || document.documentElement.scrollTop;

                xPos += (targetNode.offsetLeft - xScroll + targetNode.clientLeft);
                yPos += (targetNode.offsetTop - yScroll + targetNode.clientTop);
            } else {
                // for all other non-BODY elements
                xPos += (targetNode.offsetLeft - targetNode.scrollLeft + targetNode.clientLeft);
                yPos += (targetNode.offsetTop - targetNode.scrollTop + targetNode.clientTop);
            }

            targetNode = targetNode.offsetParent;
        }
        return {
            x: xPos,
            y: yPos
        };
    },

    /**
     *
     * @param {HTMLElement} targetNode
     * @returns {{x: number, y: number}}
     */
    getCenterPos: function (targetNode) {
        const pos = this.getPosition(targetNode);
        const xPos = pos.x + (targetNode.offsetWidth / 2);
        const yPos = pos.y + (targetNode.offsetHeight / 2);

        return {
            x: xPos,
            y: yPos
        }
    }

};
