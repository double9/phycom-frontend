<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Currency symbol asset bundle.
 */
class CurrencySymbolAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/currency-symbol';
	public $css = [];
	public $js = [
	    'main.js'
	];
	public $publishOptions = ['except' => ['*.less']];

}
