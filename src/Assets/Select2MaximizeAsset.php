<?php

namespace Phycom\Frontend\Assets;


/**
 * Class Select2MaximizeAsset
 * @package Phycom\Frontend\Assets
 */
class Select2MaximizeAsset extends \yii\web\AssetBundle
{
    // The files are not web directory accessible, therefore we need
    // to specify the sourcePath property. Notice the @conquer alias used.
    public $sourcePath = '@Phycom/Frontend/Assets/select2';

    public $js=[
        'maximize-select2-height.min.js',
    ];

    public $depends= [
        Select2Asset::class
    ];
}
