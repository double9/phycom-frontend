<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class DataStorageAsset
 *
 * @package Phycom\Frontend\Assets
 */
class DataStorageAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/utility';

	public $js = [
	    'data-storage.js'
	];
}
