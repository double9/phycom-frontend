
var RippleEffect = (function() {
    'use strict';

    /**
     * @param {{}} [config]
     * @constructor
     */
    let RippleDrop = function (config = {}) {
        this.parentNode = config.parentNode || document.body;
        this.removeDelay = config.removeDelay || 2000;
        this.color = config.color || 'dark';
        this.offsetX = config.offsetX || 5;
        this.offsetY = config.offsetY || 5;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @returns {HTMLDivElement}
     */
    RippleDrop.prototype.drop = function (x, y) {

        let node = document.createElement('div');
            node.classList.add('ripple-effect');
            node.classList.add('ripple-animate');

            if (this.color) {
                node.classList.add(this.color);
            }

            node.style.left = x - this.offsetX + 'px';
            node.style.top = y - this.offsetY + 'px';

        this.parentNode.appendChild(node);

        window.setTimeout(() => {
            if (this.parentNode.contains(node)) {
                this.parentNode.removeChild(node);
            }
        }, this.removeDelay);

        return node;
    };

    /**
     * @param {HTMLElement} targetNode
     */
    RippleDrop.prototype.dropOnNode = function (targetNode) {

        if (!DomHelper.isOutOfViewport(targetNode).any && DomHelper.isVisible(targetNode)) {
            let pos = DomHelper.getCenterPos(targetNode);
            return this.drop(pos.x, pos.y);
        }
        return null;
    };

    return  {

        /**
         * Triggers ripple drop at specified coordinate
         *
         * @param {Number} x
         * @param {Number} y
         * @param {{}} [config]
         * @returns {RippleDrop}
         */
        atPoint: function (x, y, config = {}) {
            let ripple = new RippleDrop(config);
                ripple.drop(x, y);

            return ripple;
        },

        /**
         * Triggers ripple drop at the center of specified node
         *
         * @param {HTMLElement} targetNode
         * @param {{}} [config]
         * @returns {RippleDrop}
         */
        atNode: function (targetNode, config = {}) {
            let ripple = new RippleDrop(config);
                ripple.dropOnNode(targetNode);

            return ripple;
        }
    }

})();


