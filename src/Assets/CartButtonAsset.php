<?php

namespace Phycom\Frontend\Assets;

use Phycom\Base\Assets\ActiveFormHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CartButtonAsset
 * @package Phycom\Frontend\Assets
 */
class CartButtonAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';
    public $js = [
        'price-calculator.js',
        'cart-button.js'
    ];
    public $depends = [
        DataStorageAsset::class,
        JqueryAsset::class,
        ButtonHelperAsset::class,
        CurrencySymbolAsset::class,
        ActiveFormHelperAsset::class
    ];
}
