<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Checkbox collapse asset bundle.
 */
class CheckboxCollapseAsset extends AssetBundle
{
    public $sourcePath = '@phycom/frontend/src/Assets/checkbox-collapse';
    public $css = [

    ];
    public $js = [
        'main.js',
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        JqueryAsset::class
    ];
}
