<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class IonIconAsset
 *
 * @package Phycom\Frontend\Assets
 */
class IonIconAsset extends AssetBundle
{
    public $sourcePath = null;
    public $js = [
        'https://unpkg.com/ionicons@5.0.0/dist/ionicons.js',
    ];
}
