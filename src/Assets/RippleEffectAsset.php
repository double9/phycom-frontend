<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Ripple effect asset bundle.
 */
class RippleEffectAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/ripple-effect';
    public $css = [
        'main.css'
    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        DomHelperAsset::class
    ];
}
