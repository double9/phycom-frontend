<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Select 2 asset bundle.
 */
class Select2WidgetAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/select2';
    public $css = [
        'select2.css'
    ];
    public $js = [];
    public $depends = [
        Select2Asset::class
    ];
}
