(function($) {

    'use strict';

    let rightPanel = '<div class="right-panel"></div>';

    if ($('.head-panel').hasClass('dark')) {
        rightPanel = '<div class="right-panel dark"></div>';
    }

    // add rightpanel for menu wrapper in mobile
    $('body').append(rightPanel).addClass('push-menu');

    let $navbarToggler = $('.head-panel .navbar-toggler');

    // show/hide menu for mobile only
    $navbarToggler.on('click', function(){
        if ($('body').hasClass('show-right-menu')) {
            $('body').removeClass('show-right-menu');
        } else {
            $('body').addClass('show-right-menu');
        }
        return false;
    });

    // function to move menu-right from .headpanel to .rightpanel vice versa
    // fires only when in mobile
    function moveMenuToRight() {
        if ($navbarToggler.css('display') === 'block') {
            $('.menu-right').appendTo('.right-panel');
        } else {
            $('.menu-right').appendTo('.head-panel .container');
        }
    }

    // calls a function to move .menu-right from .headpanel to .rightpanel
    moveMenuToRight();
    $(window).resize(function(){
        moveMenuToRight();
    });

})(jQuery);
