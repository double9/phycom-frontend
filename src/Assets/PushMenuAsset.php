<?php

namespace Phycom\Frontend\Assets;

use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Push-menu asset bundle.
 */
class PushMenuAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/push-menu';
    public $css = [
        'main.css'
    ];
    public $js = [
        'main.js'
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        BootstrapAsset::class,
        JqueryAsset::class
    ];
}
