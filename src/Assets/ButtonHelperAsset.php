<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ButtonHelperAsset
 * @package Phycom\Frontend\Assets
 */
class ButtonHelperAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/button-helper';
	public $js = [
	    'button.js'
	];
	public $depends = [
        JqueryAsset::class
	];
}
