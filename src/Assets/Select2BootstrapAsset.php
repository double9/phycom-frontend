<?php

namespace Phycom\Frontend\Assets;

use yii\bootstrap\BootstrapAsset;

/**
 * Class Select2BootstrapAsset
 * @package Phycom\Frontend\Assets
 */
class Select2BootstrapAsset extends \yii\web\AssetBundle
{
    // The files are not web directory accessible, therefore we need
    // to specify the sourcePath property. Notice the @conquer alias used.
    public $sourcePath = '@Phycom/Frontend/Assets/select2';

    public $css = [
        'select2-bootstrap.min.css',
    ];

    public $depends = [
        BootstrapAsset::class,
        Select2Asset::class
    ];
}
