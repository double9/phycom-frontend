<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Dom helper utility asset bundle.
 */
class DomHelperAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/utility';

    public $js = [
        'dom-helper.js'
    ];
}
