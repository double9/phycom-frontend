<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class PubSubAsset
 *
 * @package Phycom\Frontend\Assets
 */
class PubSubAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/pubsub-js/src';

    public $js = [
        'pubsub.js'
    ];

}
