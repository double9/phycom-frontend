<?php

namespace Phycom\Frontend\Assets;

use Phycom\Base\Assets\ActiveFormHelperAsset;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Order and cart page asset bundle.
 */
class CheckoutAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/checkout';
	public $css = [];
	public $js = [
		'main.js',
        'promotion.js'
	];
	public $publishOptions = ['except' => ['*.less']];
	public $depends = [
	    CartTableAsset::class,
        JqueryAsset::class,
        ButtonHelperAsset::class,
        ActiveFormHelperAsset::class,
        CurrencySymbolAsset::class
	];
}
