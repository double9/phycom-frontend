<?php

namespace Phycom\Frontend\Assets;


use yii\web\AssetBundle;

/**
 * Class CartWidgetAsset
 *
 * @package Phycom\Frontend\Assets
 */
class CartWidgetAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';

    public $js = [
        'cart-widget.js'
    ];

    public $depends = [
        DataStorageAsset::class,
        RippleEffectAsset::class,
        DomHelperAsset::class
    ];
}
