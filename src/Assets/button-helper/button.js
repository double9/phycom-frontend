
var ButtonHelper = (function ($) {

    const DEFAULT_LOADING_TXT = '<i class="fas fa-spinner fa-spin"></i>'; //'loading...';

    return {
        loading: function ($btn) {

            const btnNode = $btn[0];
            const loadingText = btnNode.dataset.loadingText || DEFAULT_LOADING_TXT;

            if ($btn.html() !== loadingText) {;

                DataStorage.put(btnNode, 'originalText', $btn.html());

                $btn.css('min-width', $btn.outerWidth() + 'px');
                $btn.html(loadingText);
                $btn.prop('disabled', true);
                $btn.addClass('disabled');
            }
        },
        reset: function ($btn) {

            const btnNode = $btn[0];
            const loadingText = btnNode.dataset.loadingText || DEFAULT_LOADING_TXT,
                  originalText = DataStorage.get(btnNode, 'originalText') || '';

            $btn.css('min-width', '');

            if ($btn.html() === loadingText) {
                $btn.html(originalText);
                $btn.prop('disabled', false);
                $btn.removeClass('disabled');
            }
        }
    };
})(jQuery);
