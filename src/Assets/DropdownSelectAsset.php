<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Dropdown se asset bundle.
 */
class DropdownSelectAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/dropdown-select';
    public $css = [];
    public $js = [
        'dropdown.js',
    ];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
    public $depends = [
        JqueryAsset::class
    ];
}
