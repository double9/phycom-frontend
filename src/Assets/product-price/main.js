
var ProductPrice = (function($) {
    'use strict';

    const DEFAULT_CURRENCY_CODE = 'EUR';

    const ProductPriceWidget = function (config) {
        this.$el = $(config.target);
        if (!this.$el.length) {
            throw new Error('Element ' + config.target + ' not found');
        }
        this.el = this.$el[0];

        this.currencyCode = config.currencyCode || DEFAULT_CURRENCY_CODE;
        this.price = config.price || null;
        this.prevPrice = config.prevPrice || null;
        this.url = config.url || false;

        this.$el.data('productPrice', this);
        this.init();
    };

    ProductPriceWidget.prototype.init = function () {

    };

    ProductPriceWidget.prototype.showSpinner = function () {

    };

    ProductPriceWidget.prototype.hideSpinner = function () {

    };

    ProductPriceWidget.prototype.requestPrice = async function (formData) {

        this.showSpinner();

        const request = new Request(this.url, {
            method: 'POST',
            body: formData
        });

        let price = await fetch(request).then((response) => response.json());
        this.update.apply(this, price);

        this.hideSpinner();
    };

    ProductPriceWidget.prototype.update = function (price, prevPrice) {

        let $price = $('.product-price-item', this.el);
            $price.attr('data-value', price);
            $price.text(CurrencySymbol.createPriceLabel(price, this.currencyCode));

            this.price = price;

        let $prevPrice = $('.product-prev-price-item', this.el);
        if (typeof prevPrice !== 'undefined' &&  $prevPrice.length) {
            $prevPrice.attr('data-value', prevPrice);
            $prevPrice.text(CurrencySymbol.createPriceLabel(prevPrice, this.currencyCode));

            this.prevPrice = prevPrice;
        }
    };


    return {
        init: function (config) {
            return new ProductPriceWidget(config);
        }
    };

})(jQuery);
