<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class CartUpdateBtnAsset
 *
 * @package Phycom\Frontend\Assets
 */
class CartUpdateBtnAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';

    public $js = [
        'cart-update-btn.js'
    ];

    public $depends = [
        CartEventsAsset::class,
        ButtonHelperAsset::class,
        PubSubAsset::class
    ];
}
