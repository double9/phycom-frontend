<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * moment.js asset bundle.
 */
class MomentAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/moment/min';
    public $js = [
        'moment-with-locales.min.js'
    ];
}
