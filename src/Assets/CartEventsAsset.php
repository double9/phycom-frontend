<?php

namespace Phycom\Frontend\Assets;


use yii\web\AssetBundle;

/**
 * Class CartEventsAsset
 *
 * @package Phycom\Frontend\Assets
 */
class CartEventsAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';

    public $js = [
        'events.js'
    ];

    public $depends = [];
}
