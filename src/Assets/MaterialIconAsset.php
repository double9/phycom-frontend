<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class MaterialIconAsset
 *
 * @package Phycom\Frontend\Assets
 */
class MaterialIconAsset extends AssetBundle
{
    public $sourcePath = null;
    public $css = [
        'https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp',
    ];
}
