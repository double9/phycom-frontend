<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CartTableAsset
 *
 * @package Phycom\Frontend\Assets
 */
class CartTableAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';

    public $js = [
        'cart-table.js'
    ];

    public $depends = [
        CurrencySymbolAsset::class,
        CartEventsAsset::class,
        PubSubAsset::class,
        JqueryAsset::class,
    ];
}
