<?php

namespace Phycom\Frontend\Assets;

use yii\web\JqueryAsset;
use yii\web\AssetBundle;

/**
 * Class SpinnerAsset
 *
 * @package Phycom\Frontend\Assets
 */
class SpinnerAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/spinner';
	public $css = [
        'spinner.css',
	];
	public $js = [
        'spinner.js'
	];
	public $publishOptions = [
        'except' => [
        	'*.less',
            '*.scss'
        ]
	];
	public $depends = [
	    JqueryAsset::class
    ];
}
