var cart = (function ($) {

    var instance = null;

    const TYPE_PRODUCT = 'product';
    const TYPE_DELIVERY = 'delivery';
    const TYPE_DISCOUNT = 'discount';
    const TYPE_OTHER = 'other';


    var Cart = function(el, currencyCode, callback) {
        this.currencyCode = currencyCode;
        this.items = [];
        this.el = el;
        this.$el = $(el);
        if (!this.$el.length) {
            throw 'Element ' + el + ' was not found';
        }
        this.updateUrl = this.$el.data('update-url');
        this.init(callback);

        this.promises = [];
    };

    Cart.prototype.init = function (callback) {
        this.$container = $('> tbody', this.$el[0]);
        var items = [];
        var self = this;
        this.$container.children().each(function(index, el) {
            items.push($(el).data());
        });
        this.items = items;
        this.$el.on('click', 'button.remove-row', function (e) {
            e.preventDefault();
            var $el = $(this),
                btn = $el.button('loading'),
                id = $el.closest('tr').data('id');

            self.removeById(id, function () {
                btn.button('reset');
            });
        });

        this.$el.on('focusout, mouseout', 'input.qty', function (e) {
            var qty = parseInt($(this).val());
            if (!isNaN(qty)) {
                self.changeQuantity($(this).closest('tr').data('id'), qty);
            }
        });

        callback();
    };


    Cart.prototype.destroy = function () {
        this.$el.off('click', 'button.remove-row');
        this.$el.off('focusout','input.qty');
    };

    Cart.prototype.getTotalProductCount = function () {
        var count = 0;
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === TYPE_PRODUCT) {
                count++;
            }
        }
        return count;
    };

    Cart.prototype.getTotal = function () {
        var total = 0;
        for (var i=0; i<this.items.length; i++) {
            total += (this.items[i].price * this.items[i].quantity);
        }
        return total;
    };

    Cart.prototype.addOrUpdateJs = function (data, labels, removeBtn) {
        var html = this.renderLine(data, labels, removeBtn),
            item = this.findItemById(data.id);

        if (item) {
            item.type = data.type;
            item.code = data.code;
            item.price = data.price;
            item.quantity = data.quantity || 1;
            item.ref = data.ref || null;
            this.$container.find('tr[data-id="'+data.id+'"]').replaceWith(html);
        } else {
            this.items.push(data);
            this.$container.append(html);
        }
        this.updateTotal();
    };

    Cart.prototype.removeByCode = function (code) {
        var self = this,
            promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].code === code) {
                this.items.splice(i, 1);
            }
        }
        this.$container.find('tr[data-code="'+code+'"]').each(function (index, el) {
            var $item = $(el);
            promises.push(self.removeItemFromSession($item.data('id')));
            $item.remove();
            self.update();
        });
        this.promises.concat(promises);
        return Promise.all(promises);
    };

    Cart.prototype.removeByType = function (type) {
        var self = this,
            promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === type) {
                this.items.splice(i, 1);
            }
        }
        this.$container.find('tr[data-type="'+type+'"]').each(function (index, el) {
            var $item = $(el);
            promises.push(self.removeItemFromSession($item.data('id')));
            $item.remove();
            self.update();
        });

        this.promises.concat(promises);
        return Promise.all(promises);
    };

    Cart.prototype.removeById = function (id) {
        var self = this,
            type = null,
            key = this.findItemKeyById(id);

        if (key > -1) {
            type = this.items[key].type;
            this.items.splice(key, 1);
        }

        this.$container.find('tr[data-id="'+id+'"]').remove();
        this.update();

        const promise = this.removeItemFromSession(id).then(function () {
            if (type === TYPE_DISCOUNT) {
                self.showPromotionForm();
            }
        });

        this.promises.push(promise);
        return promise;
    };

    Cart.prototype.findByType = function (type) {
        let items = [];
        for (let i=0; i<this.items.length; i++) {
            if (this.items[i].type === type) {
                items.push(this.items[i]);
            }
        }
        return items;
    };

    Cart.prototype.showPromotionForm = function () {
        if (typeof promotion != 'undefined') {
            promotion.show();
        }
    };

    Cart.prototype.removeItemFromSession = function(id) {

        const url = this.$el.data('remove-url') + '?id=' + id;
        return this.post(url)
            .then(() => this.updateDynamicLines())
            .then(() => {
                if (!this.getTotalProductCount()) {
                    window.location.reload();
                }
            });
    };

    Cart.prototype.changeQuantity = async function (id, qty, callback) {
        callback = callback || function(){};
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].id === id) {

                let url = this.updateUrl + '?id=' + id + '&qty=' + qty;

                const promise = this.post(url)
                    .then(() => this.updateDynamicLines())
                    .then(() => callback())
                    .then(() => {
                        this.checkState();
                    });

                this.items[i].quantity = qty;

                var totalPrice = this.items[i].price * this.items[i].quantity;
                this.$container.find('tr[data-id="'+id+'"] td.price').html(CurrencySymbol.createPriceLabel(totalPrice, this.currencyCode));
                this.update();

                this.promises.push(promise);
                return promise;
            }
        }
    };

    Cart.prototype.renderLine = function (d, p, removeBtn) {
        p = p || {};
        removeBtn = removeBtn || false;

        return '<tr data-id="' + d.id + '" data-type="' + d.type + '" data-code="' + d.code + '" data-price="' + d.price + '" data-quantity="' + (d.quantity || 1) + '" data-ref="' + (d.ref || '') + '">' +
            '<td class="code">'+ d.code + '</td>' +
            '<td class="photo"></td>' +
            '<td class="title"><strong>' + (p.label || d.label || '')  + '</strong></td>' +
            '<td>' + (d.type === TYPE_PRODUCT ? (d.quantity || 1) : '') + '</td>' +
            '<td class="price">' + (p.price || CurrencySymbol.createPriceLabel(d.price, this.currencyCode)) + '</td>' +
            '<td class="clearfix">' + (removeBtn ? this.renderRemoveBtn() : '') + '</td>' +
            '</tr>';
    };

    Cart.prototype.renderRemoveBtn = function () {
        return '<button class="remove-row btn btn-danger btn-xs btn-flat pull-right"><i class="mdi mdi-remove mdi-lg"></i></button>';
    };


    Cart.prototype.findItemKeyById = function (id) {
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].id === id) {
                return i;
            }
        }
        return -1;
    };

    /**
     * @returns {Promise<void>}
     */
    Cart.prototype.checkState = async function () {

        // console.log('check status');

        let delay = function (msec, value) {
            return new Promise(done => window.setTimeout((() => done(value)), msec));
        }

        let isFinished = function (promise) {
            return Promise.race([delay(0, false), promise.then(() => true, () => true)]);
        }

        for (let i = this.promises.length -1; i >= 0 ; i--){
            let wasFinished = await isFinished(this.promises[i]);
            if (wasFinished) {
                this.promises.splice(i, 1);
            }
        }

        if (!this.promises.length) {
            this.updateComplete();
        }
    };

    Cart.prototype.updateComplete = function () {
        // console.log('cart updated !');
        PubSub.publish(CART_UPDATED, {cart: this});
    };

    Cart.prototype.findItemById = function (id) {
        var key = this.findItemKeyById(id);
        return key > -1 ? this.items[key] : null;
    };

    Cart.prototype.updateDynamicLines = function () {
        // console.log('update dynamic lines');
        var promises = [];
        for (var i=0; i<this.items.length; i++) {
            if (this.items[i].type === TYPE_DISCOUNT) {
                promises.push(this.updateLine(this.items[i].id));
            }
        }
        return Promise.all(promises);
    };

    Cart.prototype.updateLine = function (id) {
        const i = this.findItemKeyById(id);

        if (i > -1) {
            this.$container.find('tr[data-id="'+id+'"]').addClass('loading');

            const promise = this.get(this.updateUrl, {id: id});

            promise.then((res) => {
                if (res.error) {
                    return promise.reject(res.error);
                }

                this.$container.find('tr[data-id="'+id+'"]').removeClass('loading');
                this.addOrUpdateJs(res, null, res.type !== TYPE_DELIVERY);
                this.items[i].price = res.price;
                this.update();

                return res;
            });
        }
        return null;
    };

    Cart.prototype.getDeliveryMethod = function () {
        let line = this.findByType(TYPE_DELIVERY)[0] || null;
        return line ? line.ref : null;
    };

    Cart.prototype.getDeliveryInfo = function () {
        let deliveryMethod = this.getDeliveryMethod();
        if (typeof shippingBox !== 'undefined') {
            return shippingBox.getInstance().exportSelectedValues(deliveryMethod);
        }
        return {method: deliveryMethod};
    }

    Cart.prototype.update = function () {
        // clear the cart if no products left
        if (!this.getTotalProductCount()) {
            this.items = [];
            this.$container.find('tr').each(function (index, el) {
                $(el).remove();
            });
        }
        this.updateTotal();
        this.updateTotalCount();
        PubSub.publish(PRICE_UPDATE, {amount: this.getTotal(), count: this.getTotalProductCount()});
    };

    Cart.prototype.updateTotalCount = function () {
        var $el = $('#cart-menu .item-count');
        var totalCount = this.getTotalProductCount();
        $el.text(totalCount);
        if (totalCount === 0) {
            $el.fadeOut(200, function() {
                $('#cart-menu').addClass('empty');
            });
        }
        return this;
    };

    Cart.prototype.updateTotal = function () {
        this.$el.find('.grand-total').html(CurrencySymbol.createPriceLabel(this.getTotal(), this.currencyCode));
        return this;
    };

    Cart.prototype.get = async function (url, params) {
        let requestUrl = url;
        if (typeof params !== 'undefined') {
            requestUrl += '?' + new URLSearchParams(params);
        }
        const response = await fetch(requestUrl, {
            method: 'GET',
            headers: this.getRequestHeaders()
        });
        return response.json();
    };

    Cart.prototype.post = async function (url, data) {
        const response = await fetch(url, {
            method: 'POST',
            headers: this.getRequestHeaders(),
            body: typeof data !== 'undefined' ? JSON.stringify(data) : ''
        });
        return response.json();
    };

    Cart.prototype.getRequestHeaders = function () {
        const csrfToken = document.querySelector('meta[name="csrf-token"]').content;
        return {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-Token': csrfToken,
            'Content-Type': 'application/json',
        };
    };

    /**
     * Public accessor methods
     */
    return {
        getPrototype: function () {
            return Cart.prototype;
        },
        init: function (el, currencyCode, callback) {
            currencyCode = currencyCode || 'EUR';
            callback = callback || function(){};
            this.destroy();
            return instance = new Cart(el, currencyCode, callback);
        },
        destroy: function() {
            if (instance && instance instanceof Cart) {
                instance.destroy();
            }
        },
        addOrUpdateJs: function (data, labels) {
            labels = labels || {};
            if (instance && instance instanceof Cart) {
                var removeBtn = data.type !== TYPE_DELIVERY;

                data.quantity = Number(data.quantity || 1);

                if ($.isFunction(data.price)) {
                    data.price().then(price => {
                        data.price = price;
                        labels.price = CurrencySymbol.createPriceLabel(price, instance.currencyCode);
                        instance.addOrUpdateJs(data, labels, removeBtn);
                    });
                } else {
                    data.price = Number(data.price);
                    instance.addOrUpdateJs(data, labels, removeBtn);
                }
            }
        },
        removeByCode: function (code, callback) {
            callback = callback || function(){};
            instance.removeByCode(code).then(callback).then(() => {instance.checkState()});
        },
        removeByType: function (code, callback) {
            callback = callback || function(){};
            instance.removeByType(code).then(callback).then(() => {instance.checkState()});
        },
        removeById: function (id, callback) {
            callback = callback || function(){};
            instance.removeById(id).then(callback).then(() => {instance.checkState()});
        },

        checkState: function () {
            return instance.checkState();
        },
        /**
         * @returns {int}
         */
        productCount: function() {
            if (instance && instance instanceof Cart) {
                return instance.getTotalProductCount();
            }
            return 0;
        }
    };

})(jQuery);
