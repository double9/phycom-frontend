
var CartWidget = (function() {
    'use strict';

    /**
     * @param {{}} config
     * @constructor
     */
    let CartBasketWidget = function (config) {
        this.el = document.querySelector(config.el);
        if (!this.el) {
            throw new Error('Element ' + config.el + ' not found');
        }
        this.navbarToggler = config.navbarToggler || false;
        this.rippleEffect = config.rippleEffect || false;
        DataStorage.put(this.el, 'CartBasketWidget', this);
    }

    /**
     * @param count - total count of products in cart
     * @param amount - total amount of products in cart
     */
    CartBasketWidget.prototype.add = function (count, amount) {
        this.updateCount(count);

        // trigger ripple effect
        if (this.rippleEffect) {

            let rippleTargetNode = this.el.querySelector('.item-count');

            if (!DomHelper.isVisible(rippleTargetNode) || DomHelper.isOutOfViewport(rippleTargetNode).any) {
                let navbarToggler = this.getNavbarToggler();
                if (navbarToggler && DomHelper.isVisible(navbarToggler) && !DomHelper.isOutOfViewport(navbarToggler).any) {
                    rippleTargetNode = navbarToggler;
                }
            }

            RippleEffect.atNode(rippleTargetNode, this.rippleEffect);
        }
    }

    /**
     * @param {number} count
     */
    CartBasketWidget.prototype.updateCount = function (count) {
        let countContainer = this.el.querySelector('.item-count');

        countContainer.textContent = String(count);

        if (!countContainer.classList.contains('is-visible')) {
            countContainer.classList.add('is-visible');
            this.el.classList.remove('empty');
        }
    };

    /**
     * @returns {null|HTMLElement}
     */
    CartBasketWidget.prototype.getNavbarToggler = function () {
        if (this.navbarToggler) {
            return document.querySelector(this.navbarToggler);
        }
        return null;
    }

    return {
        create: function (config) {
            return new CartBasketWidget(config);
        }
    };

})();
