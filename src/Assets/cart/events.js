const PRICE_UPDATE = 'cart.priceUpdate';      // cart price update
const CART_UPDATED = 'cart.updated';          // cart update is complete
