var CartUpdateBtn = (function($) {
    'use strict';

    var CartUpdateBtnWidget = function (config) {

        this.el = document.querySelector(config.selector);
        this.updateUrl = config.updateUrl;

        if (!this.el) {
            throw new Error('Element ' + config.selector + ' not found');
        }
        this.token = null;
        this.init();
    }

    CartUpdateBtnWidget.prototype.init = function () {

        this.el.addEventListener('click', (e) => {
            e.preventDefault();
            ButtonHelper.loading($(this.el));
            if (this.token) {
                PubSub.unsubscribe(this.token);
            }
            this.token = PubSub.subscribe(CART_UPDATED, (msg, data) => {
                this.saveState(data.cart)
                    .then(() => this.reload(data));
            });

            window.cart.checkState();

        }, false);
    };

    CartUpdateBtnWidget.prototype.saveState = async function(cart) {

        let deliveryInfo = cart.getDeliveryInfo();

        let data = {};

        if (typeof deliveryInfo.method != 'undefined') {
            data.deliveryMethod = deliveryInfo.method;
        }
        if (typeof deliveryInfo.area != 'undefined') {
            data.deliveryArea = deliveryInfo.area;
        }
        if (typeof deliveryInfo.date != 'undefined') {
            data.deliveryDate = deliveryInfo.date;
        }
        if (typeof deliveryInfo.time != 'undefined') {
            data.deliveryTime = deliveryInfo.time;
        }

        return this.post(this.updateUrl, data);
    };

    CartUpdateBtnWidget.prototype.reload = function (data) {

        // ButtonHelper.reset($(this.el));
        if (this.token) {
            PubSub.unsubscribe(this.token);
            this.token = null;
        }
        location.reload();
    };

    CartUpdateBtnWidget.prototype.post = async function (url, data) {
        const response = await fetch(url, {
            method: 'POST',
            headers: this.getRequestHeaders(),
            body: typeof data !== 'undefined' ? JSON.stringify(data) : ''
        });
        return response.json();
    };

    CartUpdateBtnWidget.prototype.getRequestHeaders = function () {
        const csrfToken = document.querySelector('meta[name="csrf-token"]').content;
        return {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-Token': csrfToken,
            'Content-Type': 'application/json',
        };
    };

    return {
        init: function (config) {
            return new CartUpdateBtnWidget(config);
        }
    };

})(jQuery);
