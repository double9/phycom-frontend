var CartTotal = (function($) {
    'use strict';

    var CartTotalWidget = function (config) {

        this.currencyCode = config.currencyCode || 'EUR';
        this.el = document.querySelector(config.selector);

        if (!this.el) {
            throw new Error('Element ' + config.selector + ' not found');
        }
        this.init();
    }

    CartTotalWidget.prototype.init = function () {
        PubSub.subscribe(PRICE_UPDATE, (msg, data) => this.update(data.amount));
    };

    CartTotalWidget.prototype.update = function (amount) {
        this.el.innerHTML = CurrencySymbol.createPriceLabel(amount, this.currencyCode);
    };

    return {
        init: function (config) {
            return new CartTotalWidget(config);
        }
    };

})(jQuery);
