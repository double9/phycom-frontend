<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ProductPriceAsset
 * @package Phycom\Frontend\Assets
 */
class ProductPriceAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/product-price';
    public $js = [
        'main.js',
    ];
    public $depends = [
        JqueryAsset::class,
        CurrencySymbolAsset::class
    ];
}
