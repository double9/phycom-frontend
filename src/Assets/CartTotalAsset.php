<?php

namespace Phycom\Frontend\Assets;

use yii\web\AssetBundle;

/**
 * Class CartTotalAsset
 *
 * @package Phycom\Frontend\Assets
 */
class CartTotalAsset extends AssetBundle
{
    public $sourcePath = '@Phycom/Frontend/Assets/cart';

    public $js = [
        'cart-total.js'
    ];

    public $depends = [
        CurrencySymbolAsset::class,
        CartEventsAsset::class,
        PubSubAsset::class
    ];
}
