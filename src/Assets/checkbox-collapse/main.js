var checkboxCollapse = (function ($) {

    const CheckboxCollapse = function(el) {
        this.input = document.querySelector(el);
        this.container = this.input.closest('.checkbox-option')
        this.init();
    };

    CheckboxCollapse.prototype.init = function() {

        let target = this.container.querySelector('.collapse');
        let $target = $(target);

        $(this.input).bind('click dblclick', function (evt) {
            if ($(this).is(':checked')) {
                $target.slideDown('fast');
            } else {
                $target.slideUp('fast');
            }
        })
    };

    return {
        init: function (el) {
            return new CheckboxCollapse(el)
        }
    }

})(jQuery);
