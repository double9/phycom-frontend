<?php

namespace Phycom\Frontend\Assets;


use rmrevin\yii\fontawesome\CdnFreeAssetBundle as FontAwesomeAsset;

use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Default frontend application asset bundle.
 */
class DefaultAsset extends AssetBundle
{
	public $sourcePath = '@Phycom/Frontend/Assets/default';
	public $css = [
		'main.css'
	];
	public $js = [
	];
    public $publishOptions = ['except' => ['*.less', '*.scss']];
	public $depends = [
        YiiAsset::class,
		BootstrapAsset::class,
        FontAwesomeAsset::class,
        MaterialIconAsset::class,
        IonIconAsset::class,
        LodashAsset::class
	];
}
