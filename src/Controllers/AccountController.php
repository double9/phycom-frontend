<?php

namespace Phycom\Frontend\Controllers;


use Phycom\Base\Helpers\FlashMsg;
use Phycom\Frontend\Models\PasswordChangeForm;

use yii\filters\AccessControl;
use Yii;

/**
 * Class AccountController
 * @package Phycom\Frontend\Controllers
 */
class AccountController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actionChangePassword()
    {
        $model = new PasswordChangeForm(Yii::$app->user->identity);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                FlashMsg::success(Yii::t('phycom/frontend/main', 'Your password was successfully changed.'));
            } else {
                FlashMsg::error($model->errors);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
