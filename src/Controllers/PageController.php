<?php

namespace Phycom\Frontend\Controllers;

use Phycom\Frontend\Models\Post\SearchPost;

use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Translation\PostTranslation;

use yii\base\ViewNotFoundException;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Class PageController
 * @package Phycom\Frontend\Controllers
 */
class PageController extends BaseController
{
    /**
     * @param $key
     * @return string
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    public function actionView($key)
    {
        $model = $this->findPageByUrlKey($key);

        // tries to find a named template - same as post identifier - if it's not found falls back to view
        try {
            return $this->render($model->identifier, ['page' => $model]);
        } catch (ViewNotFoundException $e) {
            return $this->render('view', ['page' => $model]);
        }
    }

    /**
     * @param $urlKey
     * @return SearchPost|null|yii\db\ActiveRecord
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    protected function findPageByUrlKey($urlKey)
    {
        $query = SearchPost::find()
            ->alias('p')
            ->select('p.*')
            ->innerJoin(['t' => PostTranslation::tableName()], [
                'and',
                't.post_id = p.id',
                ['t.url_key' => $urlKey]
            ])
            ->where(['p.status' => PostStatus::PUBLISHED, 'p.type' => PostType::PAGE]);

        if ($post = $query->one()) {
            return $post;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
