<?php

namespace Phycom\Frontend\Controllers;


use yii\base\Action;
use Yii;

class BlowOffAction extends Action
{
    /**
     * @var string the view file to be rendered. If not set, it will take the value of [[id]].
     * That means, if you name the action as "error" in "SiteController", then the view name
     * would be "error", and the corresponding view file would be "views/site/error.php".
     */
    public string $view = 'blow-off';

    /**
     * @var string|null the name of the error when the exception name cannot be determined.
     * Defaults to "Error".
     */
    public ?string $defaultTitle = null;

    /**
     * @var string|null the message to be displayed when the exception message contains sensitive information.
     * Defaults to "An internal server error occurred.".
     */
    public ?string $defaultMessage = null;

    /**
     * @var string|null
     */
    public ?string $countryCode = null;

    /**
     * @var string|null
     */
    public ?string $ip = null;

    /**
     * @var string|false|null the name of the layout to be applied to this error action view.
     * If not set, the layout configured in the controller will be used.
     * @see \yii\base\Controller::$layout
     */
    public string|false|null $layout = false;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if ($this->defaultMessage === null) {
            $this->defaultMessage = Yii::t('phycom/frontend', 'Dear visitor your IP address was automatically banned for being a proxy of type VPN.');
        }

        if ($this->defaultTitle === null) {
            $this->defaultTitle = Yii::t('phycom/frontend', 'Access denied');
        }
    }

    /**
     * Runs the action.
     *
     * @return string result content
     */
    public function run()
    {
        if ($this->layout !== null) {
            $this->controller->layout = $this->layout;
        }

        Yii::$app->getResponse()->setStatusCode(400);

        if (Yii::$app->getRequest()->getIsAjax()) {
            return $this->renderAjaxResponse();
        }

        return $this->renderHtmlResponse();
    }


    /**
     * Builds string that represents the exception.
     * Normally used to generate a response to AJAX request.
     * @return string
     */
    protected function renderAjaxResponse(): string
    {
        return $this->defaultTitle . ': ' . $this->defaultMessage;
    }

    /**
     * Renders a view that represents the exception.
     * @return string
     */
    protected function renderHtmlResponse(): string
    {
        return $this->controller->render($this->view ?: $this->id, $this->getViewRenderParams());
    }

    /**
     * Builds array of parameters that will be passed to the view.
     * @return array
     */
    protected function getViewRenderParams(): array
    {
        return [
            'title'       => $this->defaultTitle,
            'message'     => $this->defaultMessage,
            'ip'          => $this->ip,
            'countryCode' => $this->countryCode
        ];
    }
}
