<?php

namespace Phycom\Frontend\Controllers;

use Phycom\Base\Helpers\Currency;
use Phycom\Base\Interfaces\CartItemInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Yii;

/**
 * Class WishlistController
 *
 * @package Phycom\Frontend\Controllers
 */
class WishlistController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'add-product',
                            'add-product-validate',
                            'update',
                            'remove',
                            'clear'
                        ],
                        'allow' => true,
                        'roles' => ['?','@']
                    ]
                ],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['items' => Yii::$app->wishlist->getItems()]);
    }

    /**
     * @param int $id - product id
     * @param int $quantity
     * @return int|array
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    public function actionAddProduct($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());

        if (!$item = $model->createOrderItem()) {
            return $model->errors;
        }

        return Yii::$app->wishlist->add($item)->getCount(CartItemProductInterface::class);
    }

    /**
     * @param int $id - product id
     * @param null $quantity
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\base\UnknownPropertyException
     */
    public function actionAddProductValidate($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());
        /**
         * @var ActiveForm|string $ActiveForm
         */
        $ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');
        return $ActiveForm::validate($model);
    }


    /**
     * Updates wishlist item
     *
     * @param $id - wishlist item id
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $id = is_numeric($id) ? (int) $id : $id;

        if ($item = Yii::$app->wishlist->getItem($id)) {
            if ($qty = Yii::$app->request->get('qty')) {
                $item->setQuantity($qty);
                Yii::$app->cart->save();
            } else {
                $qty = method_exists($item, 'getQuantity') ? $item->getQuantity() : 1;
            }

            $itemType = null;
            foreach (Yii::$app->wishlist->itemTypes as $type => $alias) {
                if ($item instanceof $type) {
                    $itemType = $alias;
                }
            }

            return [
                'id'       => $item->getUniqueId(),
                'code'     => $item->getCode(),
                'price'    => Currency::toDecimal($item->getPrice()),
                'type'     => $itemType,
                'quantity' => $qty,
                'label'    => $item->getLabel()
            ];
        }
        return ['error' => Yii::t('phycom/frontend/main', 'Item not found')];
    }


    /**
     * Removes an item from wishlist session
     *
     * @param $id - wishlist item id
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionRemove($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $id = is_numeric($id) ? (int) $id : $id;

        if (Yii::$app->wishlist->hasItem($id)) {
            Yii::$app->wishlist->remove($id);
        }
        if (Yii::$app->wishlist->getCount(CartItemInterface::class) === 0) {
            Yii::$app->wishlist->clear();
        }
        return [];
    }

    /**
     * Clears the wishlist and redirect to previous page
     * @return Response
     */
    public function actionClear()
    {
        Yii::$app->wishlist->clear();
        return $this->redirect(Yii::$app->request->referrer);
    }


}
