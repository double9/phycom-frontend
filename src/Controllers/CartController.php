<?php

namespace Phycom\Frontend\Controllers;

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use Phycom\Base\Modules\Payment\Models\PaymentMethodSelectionForm;
use Phycom\Base\Modules\Delivery\Models\ShippingForm;
use Phycom\Base\Interfaces\CartItemProductInterface;
use Phycom\Base\Interfaces\CartItemInterface;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Helpers\FlashMsg;
use Phycom\Base\Models\Order;

use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Class CartController
 * @package Phycom\Frontend\Controllers
 */
class CartController extends BaseController
{

    public $defaultAction = 'checkout';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'checkout',
                            'make-payment',
                            'apply-discount',
                            'add-product',
                            'add-product-validate',
                            'update',
                            'update-state',
                            'remove',
                            'clear',
                            'submit-order'
                        ],
                        'allow' => true,
                        'roles' => ['?','@']
                    ]
                ],
            ]
        ];
    }

    /**
     * Renders cart contents or redirects to previous page if the cart is empty
     * @return string|Response
     * @throws yii\base\InvalidConfigException
     */
    public function actionCheckout()
    {
        if (Yii::$app->cart->getCount(CartItemInterface::class) > 0) {
            if (Yii::$app->user->isGuest) {
                Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
            }
            return $this->render('checkout-one');
        }
        if (Yii::$app->cart->getCount() > 0) {
            Yii::$app->cart->clear();
        }
        FlashMsg::warning(Yii::t('phycom/frontend/main', 'Shopping cart is empty'));
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * Renders the payment page on 2-step checkout
     * @return string|Response
     */
    public function actionMakePayment()
    {
        if ($orderId = Yii::$app->session->get('order')) {
            $order = Yii::$app->modelFactory->getOrder()::findOne($orderId);
            return $this->render('payment', ['order' => $order]);
        }
        FlashMsg::warning(Yii::t('phycom/frontend/main', 'Order not found or has expired'));
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionApplyDiscount()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $form = Yii::$app->modelFactory->getPromotionCodeForm();
        $form->load(Yii::$app->request->post());

        if (!$discount = $form->apply()) {
            return ['error' => $form->lastError];
        }

        return [
            'id'    => $discount->getUniqueId(),
            'code'  => $discount->getCode(),
            'label' => $discount->getLabel(),
            'price' => Currency::toDecimal($discount->getPrice()),
            'type'  => 'discount'
        ];
    }

    /**
     * @param int $id - product id
     * @param null $quantity
     * @return array
     * @throws NotFoundHttpException
     * @throws yii\base\UnknownPropertyException
     */
    public function actionAddProductValidate($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());
        /**
         * @var ActiveForm|string $ActiveForm
         */
        $ActiveForm = Yii::$app->modelFactory->getClassName('ActiveForm');
        return $ActiveForm::validate($model);
    }

    /**
     * @param int $id - product id
     * @param int $quantity
     * @return array - total count of items in cart and price
     * @throws NotFoundHttpException
     * @throws yii\base\Exception
     */
    public function actionAddProduct($id, $quantity = null)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (!$product = Yii::$app->modelFactory->getProduct()::findOne($id)) {
            throw new NotFoundHttpException('The requested product ' . $id . ' does not exist.');
        }
        $model = Yii::$app->modelFactory->getCartForm($product);
        if ($quantity) {
            $model->quantity = $quantity;
        }
        $model->load(Yii::$app->request->post());

        if (!$item = $model->createOrderItem()) {
            return $model->errors;
        }

        Yii::$app->cart->add($item);

        // remove delivery items here as the parcel size might have been changed when product is added
        foreach (Yii::$app->cart->getDeliveryItems() as $item) {
            Yii::$app->cart->remove($item->getUniqueId());
        }
        return [
            Yii::$app->cart->getCount(CartItemProductInterface::class),
            Currency::toDecimal(Yii::$app->cart->getTotal())
        ];
    }

    /**
     * Updates cart item
     *
     * @param $id - cart item id
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        $id = is_numeric($id) ? (int) $id : $id;

        if ($item = Yii::$app->cart->getItem($id)) {
            if ($qty = Yii::$app->request->get('qty')) {
                $item->setQuantity($qty);
                Yii::$app->cart->save();
            } else {
                $qty = method_exists($item, 'getQuantity') ? $item->getQuantity() : 1;
            }

            $itemType = null;
            foreach (Yii::$app->cart->itemTypes as $type => $alias) {
                if ($item instanceof $type) {
                    $itemType = $alias;
                }
            }

            return [
                'id'       => $item->getUniqueId(),
                'code'     => $item->getCode(),
                'price'    => Currency::toDecimal($item->getPrice()),
                'type'     => $itemType,
                'quantity' => $qty,
                'label'    => $item->getLabel()
            ];
        }
        return ['error' => Yii::t('phycom/frontend/main', 'Item not found')];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateState()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);
        $data = Json::decode(Yii::$app->request->getRawBody());

        if ($deliveryMethod = ($data['deliveryMethod'] ?? null)) {
            Yii::$app->session->set('delivery_method', $deliveryMethod);
        }
        if ($deliveryArea = ($data['deliveryArea'] ?? null)) {
            Yii::$app->session->set('delivery_area', $deliveryArea);
        }
        if ($deliveryDate = ($data['deliveryDate'] ?? null)) {
            Yii::$app->session->set('delivery_date', $deliveryDate);
        }
        if ($deliveryTime = ($data['deliveryTime'] ?? null)) {
            Yii::$app->session->set('delivery_time', $deliveryTime);
        }

        return [];
    }


    /**
     * Removes an item from cart session
     *
     * @param $id - cart item id
     * @return array
     * @throws yii\web\NotFoundHttpException
     */
    public function actionRemove($id)
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

//        $id = is_numeric($id) ? (int) $id : $id;
        $id = (string) $id;

        if (Yii::$app->cart->hasItem($id)) {
            Yii::$app->cart->remove($id);
        }
        if (Yii::$app->cart->getCount(CartItemInterface::class) === 0) {
            Yii::$app->cart->clear();
        }
        return [];
    }

    /**
     * Clears the cart and redirect to home
     * @return Response
     */
    public function actionClear()
    {
        Yii::$app->cart->clear();
        return $this->redirect(Yii::$app->homeUrl);
    }

    /**
     * Creates an order from the cart contents
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionSubmitOrder()
    {
        $this->ajaxOnly(Response::FORMAT_JSON);

        if (empty(Yii::$app->cart->getItems())) {
            $msg = Yii::t('phycom/frontend/main', 'Shopping cart is empty or session has expired');
            FlashMsg::warning($msg);
            return ['error' => $msg, 'url' => Url::toRoute('/site/index')];
        }
        /**
         * @var ShippingForm|object $model
         */
        $model = Yii::createObject(ShippingForm::class);
        $model->load(Yii::$app->request->post());
        if (!$model->validate() || !$model->validateDeliveryDate(Yii::$app->cart->getProductItems())) {
            return ['error' => $model->lastError];
        }
        foreach (Yii::$app->cart->getDeliveryItems() as $item) {
            Yii::$app->cart->remove($item->getUniqueId());
        }
        $shipment = $model->createShipment(Yii::$app->cart->getProductItems());

        if (!$shipment->getSelectedRate()) {
            return ['error' => Yii::t('phycom/frontend/main', 'Shipping rate is not selected')];
        }

        if ($shipment->hasErrorMessages()) {
            return ['error' => $shipment->messages[0]];
        } else {
            Yii::$app->cart->add($shipment->getShipmentLine());
        }

        /**
         * @var PaymentMethodSelectionForm $paymentSelection
         */
        $paymentSelection = Yii::createObject(PaymentMethodSelectionForm::class);
        if ($paymentSelection->load(Yii::$app->request->post()) && !$paymentSelection->validate()) {
            return ['error' => $paymentSelection->messages[0]];
        }

        $order = $this->loadOrderFromSession();
        $orderForm = Yii::$app->modelFactory->getOrderForm(Yii::$app->user->identity, $order);
        $orderForm->load(Yii::$app->request->post());
        $redirect = Url::toRoute(['/cart/make-payment']);

        try {
            if ($order && $orderForm->update(Yii::$app->cart->getItems())) {
                $this->saveOrderToSession($order);
                return $this->ajaxSuccess([
                    'msg' => Yii::t('phycom/frontend/main', 'Order {order_id} updated', ['order_id' => $order->id]),
                    'url' => $redirect
                ]);
            }
            if ($order = $orderForm->create(Yii::$app->cart->getItems())) {
                $this->saveOrderToSession($order);
                return $this->ajaxSuccess([
                    'msg' => Yii::t('phycom/frontend/main', 'Order {order_id} created', ['order_id' => $order->id]),
                    'url' => $redirect
                ]);
            }
        } catch (yii\base\InvalidArgumentException $e) {
            if ($e->getCode() === 1) {
                Yii::$app->cart->clear();
                $msg = Yii::t('phycom/frontend/main', 'Shopping cart is empty');
                return $this->ajaxError($msg);
            }
            throw $e;
        }
        return ['error' => $orderForm->lastError];
    }

    /**
     * @param Order $order
     */
    protected function saveOrderToSession(Order $order)
    {
        Yii::$app->session->set('order', $order->id);

        if ($order->shipment) {
            Yii::$app->session->set('delivery_method', $order->shipment->method);

            if ($order->shipment->to_address) {
                Yii::$app->session->set('delivery_address', $order->shipment->to_address);
            }
            if ($order->shipment->deliveryArea) {
                Yii::$app->session->set('delivery_area', $order->shipment->deliveryArea->area_code);
            }
            if ($order->shipment->delivery_date) {
                Yii::$app->session->set('delivery_date', $order->shipment->delivery_date->format('Y-m-d'));
            }
            if ($order->shipment->delivery_time) {
                Yii::$app->session->set('delivery_time', $order->shipment->delivery_time);
            }
        }
    }

    /**
     * @return Order|null
     */
    protected function loadOrderFromSession(): ?Order
    {
        return Yii::$app->session->get('order') ? Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order')) : null;
    }
}
