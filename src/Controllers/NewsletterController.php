<?php

namespace Phycom\Frontend\Controllers;


use Phycom\Base\Helpers\FlashMsg;
use Phycom\Frontend\Models\SubscriptionForm;

use Yii;


/**
 * Class NewsletterController
 * @package Phycom\Frontend\Controllers
 */
class NewsletterController extends BaseController
{
    public function actionSubscribe()
    {
        $model = new SubscriptionForm();
        if ($model->load(Yii::$app->request->post()) && $model->subscribe()) {
            FlashMsg::success(Yii::t('phycom/frontend/main', '{email} was successfully subscribed', ['email' => $model->email]));
        } else {
            FlashMsg::error($model->errors);
        }

        $url = Yii::$app->request->referrer;
        if (substr($url, -1) === '/') {
            $url = substr($url, 0, -1);
        }
        if (substr($url, -2) !== '#a') {
            $url .= '#a';
        }
        return $this->redirect($url);
    }
}
