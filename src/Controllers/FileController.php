<?php

namespace Phycom\Frontend\Controllers;


use Phycom\Base\Actions\FileDownloadAction;

/**
 * Class FileController
 *
 * @package Phycom\Frontend\Controllers
 */
class FileController extends BaseController
{
	public function actions()
	{
		return [
            'download' => [
                'class'  => FileDownloadAction::class,
                'inline' => true
            ],
		];
	}
}
