<?php

namespace Phycom\Frontend\Controllers;

use yii\base\InvalidArgumentException;
use Yii;

/**
 * Class PaymentReturnController
 * @package Phycom\Frontend\Controllers
 */
class PaymentReturnController extends BaseController
{
    public $enableCsrfValidation = false;
    /**
     * @return yii\web\Response
     */
    public function actionIndex()
    {
        $status = Yii::$app->request->get('status');

        switch ($status) {

            case 'success':
                if (Yii::$app->session->get('order')) {
                    Yii::$app->session->remove('order');
                }
                Yii::$app->cart->clear();
                return $this->render('/cart/payment-success', ['message' => Yii::t('phycom/frontend/payment', 'Payment was successfully completed. An order confirmation message will be sent to your email shortly.')]);

            case 'failed':
            case 'cancel':
                return $this->render('/cart/payment-failed', ['message' => Yii::t('phycom/frontend/payment', 'Payment was cancelled')]);

            case 'error':
                return $this->render('/cart/payment-failed', ['message' => Yii::t('phycom/frontend/payment', 'Error processing payment')]);

            default:
                throw new InvalidArgumentException('Invalid payment return status');
        }
    }
}
