<?php

namespace Phycom\Frontend\Controllers;

use Phycom\Base\Models\Traits\ComponentTrait;
use Phycom\Base\Models\Traits\WebControllerTrait;
use Phycom\Base\Models\Country;

use yii\web\NotFoundHttpException;
use yii\web\Controller;
use Yii;

/**
 * This is the base controller class that every frontend controller should extend.
 *
 * Class BaseController
 * @package Phycom\Frontend\Controllers
 */
class BaseController extends Controller
{
	use WebControllerTrait;
	use ComponentTrait;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
	    if (!Yii::$app->session->get('country')) {
            $country = Country::findOne(['code' => Yii::$app->geoip->ip()->isoCode]);
            if ($country && in_array($country, Yii::$app->country->countries)) {
                Yii::$app->country->setCountry($country->code);
                Yii::$app->session->set('country', $country->code);
            }
        }
	    return parent::beforeAction($action);
    }

    /**
     * @param mixed $component
     * @throws yii\web\NotFoundHttpException
     */
    protected function checkEnabled($component)
    {
        if (!$this->isComponentEnabled($component)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//	public function afterAction($action, $result)
//	{
//		$result = parent::afterAction($action, $result);
//		$routesNotRecorded = ['site/status', 'file/download'];
//		$routesRecorded = ['user/upload-avatar'];
//
//		if (!Yii::$app->user->isGuest && (!Yii::$app->request->isAjax || in_array($action->controller->route, $routesRecorded)) && !in_array($action->controller->route, $routesNotRecorded)) {
//			UserActivity::logEffectiveUserUrl();
//		}
//		return $result;
//	}


}
