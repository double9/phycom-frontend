<?php


namespace GeoIPUnit;

use PHPUnit_Framework_TestCase;
use Phycom\Frontend\Modules\GeoIP\Components\GeoIP;

class TestCase extends PHPUnit_Framework_TestCase {
    protected function result($ip = null) {
        $geoIp = new GeoIP();
        return $geoIp->ip($ip);
    }
}
