<?php

namespace Phycom\Frontend\Widgets;

use yii\widgets\LinkPager as BaseLinkPager;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class LinkPager
 * @package Phycom\Frontend\Widgets
 */
class LinkPager extends BaseLinkPager
{
    /**
     * @var bool
     */
    public bool $displayDots = true;
    /**
     * @var string
     */
    public string $dotLabel = '...';

    /**
     * @param Pagination|null $pagination
     * @param array $navOptions
     * @param array $options
     * @return string
     * @throws \Exception
     */
	public static function create(Pagination $pagination = null, array $navOptions = [], array $options = [])
    {
        $widget = static::widget(ArrayHelper::merge([
            'pagination'                    => $pagination,
            'prevPageLabel'                 => '<i class="fas fa-angle-left"></i>',
            'nextPageLabel'                 => '<i class="fas fa-angle-right"></i>',
            'pageCssClass'                  => 'page-item',
            'nextPageCssClass'              => 'page-item next',
            'prevPageCssClass'              => 'page-item prev',
            'firstPageCssClass'             => 'page-item first',
            'lastPageCssClass'              => 'page-item last',
            'options'                       => [
                'class' => 'pagination pagination-rounded pagination-inverse'
            ],
            'linkOptions'                   => [
                'class' => 'page-link'
            ],
            'disabledListItemSubTagOptions' => [
                'tag'   => 'span',
                'class' => 'page-link'
            ],
            'displayDots'                   => true,
            'maxButtonCount'                => 3,
            'firstPageLabel'                => true,
            'lastPageLabel'                 => true
        ], $options));

        return Html::tag('nav', $widget, ArrayHelper::merge([
            'aria-label' => Yii::t('phycom/frontend/main', 'Page navigation'),
            'class'      => 'mg-t-20 d-flex justify-content-center'
        ], $navOptions));
    }

    /**
     * @return string
     */
    protected function renderPageButtons()
    {
        if ($this->displayDots) {
            return $this->renderPageButtonsDotted();
        }
        return parent::renderPageButtons();
    }

    /**
     * @return string
     */
    protected function renderPageButtonsDotted()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();



        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
        }

        // internal page-range
        [$beginPage, $endPage] = $this->getPageRange();

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false && $beginPage > 0) {
            $buttons[] = $this->renderPageButton($firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
            $buttons[] = $this->renderPageButton($this->dotLabel, 0, $this->pageCssClass, true, false);
        }

        // internal pages
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttons[] = $this->renderPageButton($i + 1, $i, null, $this->disableCurrentPageButton && $i == $currentPage, $i == $currentPage);
        }

        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false && $endPage < $pageCount - 1) {
            $buttons[] = $this->renderPageButton($this->dotLabel, 0, $this->pageCssClass, true, false);
            $buttons[] = $this->renderPageButton($lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }


        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'ul');
        return Html::tag($tag, implode("\n", $buttons), $options);
    }
}
