<?php

namespace Phycom\Frontend\Widgets;


use Phycom\Frontend\Assets\ProductPriceAsset;

use Phycom\Base\Helpers\Currency;
use Phycom\Base\Helpers\f;
use Phycom\Base\Models\Product\Product;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\base\Widget;
use Yii;

/**
 * Class ProductPrice
 * @package Phycom\Frontend\Widgets
 */
class ProductPrice extends Widget
{
    public string $tag = 'div';
    /**
     * @var array
     */
    public array $options = [];
    /**
     * @var Product
     */
    public Product $product;
    /**
     * @var int|float
     */
    public $units;
    /**
     * @var array
     */
    public array $priceOptions = ['class' => 'product-price-item'];
    /**
     * @var array
     */
    public array $prevPriceOptions = ['class' => 'product-prev-price-item'];
    /**
     * @var string
     */
    public string $currencyCode;
    /**
     * @var string
     */
    public string $url;
    /**
     * @var bool|array
     */
    public bool|array $unitPrice = false;
    /**
     * @var string
     */
    public string $unitTemplate = '{price}/{unitLabel}';

    public function init()
    {
        parent::init();
        if (!isset($this->currencyCode)) {
            $this->currencyCode = Yii::$app->formatter->currencyCode;
        }
        if (!isset($this->url)) {
            $this->url = Yii::$app->urlManagerFrontend->createUrl(['product/get-price', 'id' => $this->product->id]);
        }
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $this->options['id'] = $this->options['id'] ?? $this->getId();
        Html::addCssClass($this->options, 'product-price');

        if (!empty($this->product->prices)) {
            echo $this->renderPrice();
            $this->registerJs();
        }
    }

    /**
     * @return string
     */
    protected function renderPrice()
    {
        $priceHelper = $this->product->getPrice($this->units);
        $priceModel = $priceHelper->getProductPrice();
        $prices = $priceHelper->getPrices();

        $options = $this->options;
        $options['data-currency-code'] = $this->currencyCode;
        $options['data-url'] = $this->url;
        $options['data-units'] = $priceModel->num_units;

        $priceOptions = $this->priceOptions;

        if (
            is_array($this->unitPrice)
            && $priceModel->num_units == 1
            && (empty($this->unitPrice) || in_array($priceModel->unit_type->value, $this->unitPrice))
        ) {
            $priceOptions['data-units'] = $priceModel->num_units;
            $priceOptions['data-unit-label'] = $priceModel->unit_type->getUnitLabel();
        }

        if (count($prices) === 1) {
            $priceOptions = [$priceOptions];
        } else {
            $priceOptions = [$priceOptions, $this->prevPriceOptions];
            $options['data-discount'] = 1;
        }
        $priceData = [];
        foreach ($prices as $key => $price) {
            $priceData[] = [$price, $priceOptions[$key]];
        }

        $html = Html::beginTag($this->tag, $options);
        foreach (array_reverse($priceData) as $priceItem) {
            $html .= $this->renderPriceItem($priceItem[0], $priceItem[1]);
        }
        $html .= Html::endTag($this->tag);
        return $html;
    }

    /**
     * @param number $price
     * @param array $options
     * @return string
     */
    protected function renderPriceItem($price, array $options = [])
    {
        $priceTag = 'span';
        if (isset($options['tag'])) {
            $priceTag = $options['tag'];
            unset($options['tag']);
        }
        $options['data-value'] = Currency::toDecimal($price);

        if (array_key_exists('data-unit-label', $options)) {
            $priceDisplay = str_replace(
                ['{price}', '{units}', '{unitLabel}'],
                [f::currency($price), $options['data-units'], $options['data-unit-label']],
                $this->unitTemplate
            );
        } else {
            $priceDisplay = f::currency($price);
        }

        return Html::tag($priceTag, $priceDisplay, $options);
    }

    protected function registerJs()
    {
        $id = $this->options['id'];
        $view = $this->getView();
        ProductPriceAsset::register($view);

        $price = $this->product->getPrice($this->units)->getPrice();
        $prevPrice = $this->product->getPrice($this->units)->getPrevPrice();

        $jsConfig = Json::encode([
            'target'       => '#' . $id,
            'price'        => Currency::toDecimal($price),
            'prevPrice'    => $prevPrice ? Currency::toDecimal($prevPrice) : null,
            'currencyCode' => $this->currencyCode,
            'url'          => $this->url
        ]);

        $view->registerJs("jQuery(function(){ProductPrice.init($jsConfig)});");
    }
}
