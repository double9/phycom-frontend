<?php

namespace Phycom\Frontend\Widgets;

use Phycom\Base\Interfaces\CartInterface;

use Phycom\Frontend\Assets\CartTotalAsset;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Widget;
use yii\di\Instance;
use yii\helpers\Json;
use yii\i18n\Formatter;
use Yii;

/**
 * Class CartTotal
 *
 * @package Phycom\Frontend\Widgets
 */
class CartTotal extends Widget
{
    /**
     * @var CartInterface|string
     */
    public $cart = 'cart';

    /**
     * @var Formatter|string
     */
    public $formatter = 'formatter';

    /**
     * @var string
     */
    public string $tag = 'span';

    /**
     * @var array|string[]
     */
    public array $options = ['class' => 'grand-total'];


    public function init()
    {
        parent::init();
        $this->cart = Instance::ensure($this->cart, CartInterface::class);
        $this->formatter = Instance::ensure($this->formatter, Formatter::class);
    }

    public function run()
    {
        $value = $this->formatter->asCurrency($this->cart->getTotal());

        $options = ArrayHelper::merge($this->options, ['id' => $this->getId()]);
        $html = Html::tag($this->tag, $value, $options);

        $jsConfig = Json::encode([
            'selector'     => '#' . $this->getId() . '.grand-total',
            'currencyCode' => $this->formatter->currencyCode
        ]);

        $view = $this->getView();
        CartTotalAsset::register($view);
        $view->registerJs("CartTotal.init($jsConfig);", $view::POS_END);

        return $html;
    }
}
