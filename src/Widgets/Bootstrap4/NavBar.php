<?php

namespace Phycom\Frontend\Widgets\Bootstrap4;

use yii\bootstrap4\Widget;
use yii\bootstrap4\Nav;
use yii\bootstrap4\BootstrapPluginAsset;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
/**
 * Class NavBar
 * @package Phycom\Frontend\Widgets\Bootstrap4
 */
class NavBar extends Widget
{
    public $brandTop = false;
	/**
	 * @var array the HTML attributes for the widget container tag. The following special options are recognized:
	 *
	 * - tag: string, defaults to "nav", the name of the container tag.
	 *
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $options = [];
	/**
	 * @var array the HTML attributes for the container tag. The following special options are recognized:
	 *
	 * - tag: string, defaults to "div", the name of the container tag.
	 *
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $containerOptions = [];

    /**
     * @var array
     */
	public array $collapseOptions = ['class' => ''];
	/**
	 * @var string|boolean the text of the brand or false if it's not used. Note that this is not HTML-encoded.
	 * @see http://getbootstrap.com/components/#navbar
	 */
	public $brandLabel = false;
	/**
	 * @var array|string|boolean $url the URL for the brand's hyperlink tag. This parameter will be processed by [[\yii\helpers\Url::to()]]
	 * and will be used for the "href" attribute of the brand link. Default value is false that means
	 * [[\yii\web\Application::homeUrl]] will be used.
	 * You may set it to `null` if you want to have no link at all.
	 */
	public $brandUrl = false;
	/**
	 * @var array the HTML attributes of the brand link.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $brandOptions = [];
	/**
	 * @var string text to show for screen readers for the button to toggle the navbar.
	 */
	public $screenReaderToggleText = 'Toggle navigation';
	/**
	 * @var boolean whether the navbar content should be included in an inner div container which by default
	 * adds left and right padding. Set this to false for a 100% width navbar.
	 */
	public $renderInnerContainer = true;
	/**
	 * @var array the HTML attributes of the inner container.
	 * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
	 */
	public $innerContainerOptions = [];

	public $visibleItemsLeft = [];

	public $visibleItemsRight = [];

	public $toggleButton = true;

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();
		$this->clientOptions = false;
		if (empty($this->options['class'])) {
			Html::addCssClass($this->options, ['navbar', 'navbar-default']);
		} else {
			Html::addCssClass($this->options, ['widget' => 'navbar']);
		}
		if (empty($this->options['role'])) {
			$this->options['role'] = 'navigation';
		}
		$options = $this->options;
		$tag = ArrayHelper::remove($options, 'tag', 'nav');
		echo Html::beginTag($tag, $options);

		if ($this->brandTop) {
            echo Html::beginTag('div', ['class' => 'container']);
            echo Html::tag('div', $this->brandTop, ['class' => 'brand-container']);
            echo Html::endTag('div');
        }

		if ($this->renderInnerContainer) {
			if (!isset($this->innerContainerOptions['class'])) {
				Html::addCssClass($this->innerContainerOptions, 'container');
			}
			echo Html::beginTag('div', $this->innerContainerOptions);
		}

		echo Html::beginTag('div', ['class' => 'navbar-visible']);

            echo Html::beginTag('div', ['class' => 'navbar-header navbar-left']);
                if ($this->brandLabel !== false) {
                    Html::addCssClass($this->brandOptions, ['widget' => 'navbar-brand']);
                    echo Html::a($this->brandLabel, $this->brandUrl === false ? Yii::$app->homeUrl : $this->brandUrl, $this->brandOptions);
                }
                if (!empty($this->visibleItemsLeft)) {
                    echo Nav::widget([
                        'options' => ['class' => 'nav navbar-nav navbar-left'],
                        'items' => $this->visibleItemsLeft,
                    ]);
                }
            echo Html::endTag('div');


            echo Html::beginTag('div', ['class' => 'navbar-header navbar-right']);
                if (!empty($this->visibleItemsRight)) {
                    echo Nav::widget([
                        'options' => ['class' => 'nav navbar-nav navbar-left', 'style' => 'margin-left: 15px;'],
                        'items' => $this->visibleItemsRight,
                    ]);
                }
                echo $this->renderToggleButton();
            echo Html::endTag('div');
//		echo Html::tag('div', '', ['class' => 'visible-xs-block clearfix']);

        echo Html::endTag('div');

		$options = $this->collapseOptions;
		Html::addCssClass($options, ['collapse' => 'collapse', 'widget' => 'navbar-collapse']);
		$tag = ArrayHelper::remove($options, 'tag', 'div');
		echo Html::beginTag($tag, $options);
	}

	/**
	 * Renders the widget.
	 */
	public function run()
	{
		$tag = ArrayHelper::remove($this->containerOptions, 'tag', 'div');
		echo Html::endTag($tag);
		if ($this->renderInnerContainer) {
			echo Html::endTag('div');
		}
		$tag = ArrayHelper::remove($this->options, 'tag', 'nav');
		echo Html::endTag($tag);
		BootstrapPluginAsset::register($this->getView());
	}

	/**
	 * Renders collapsible toggle button.
	 * @return string the rendering toggle button.
	 */
	protected function renderToggleButton()
	{
		if (!$this->toggleButton) {
			return '<div style="display: inline-block; margin-right: 20px;"></div>';
		}

		$bar = Html::tag('span', '', ['class' => 'navbar-toggler-icon']);
		$screenReader = "<span class=\"sr-only\">{$this->screenReaderToggleText}</span>";

        return Html::button("{$screenReader}\n{$bar}", [
            'class'       => 'navbar-toggler',
            'data-toggle' => 'collapse',
            'data-target' => "#{$this->options['id']} .navbar-collapse",
        ]);
	}
}
