<?php

namespace Phycom\Frontend\Widgets\Bootstrap4;

use Phycom\Base\Helpers\JsExpression;
use Phycom\Base\Helpers\PhoneHelper;
use Phycom\Base\Models\Country;

use Phycom\Frontend\Assets\CheckboxCollapseAsset;
use Phycom\Frontend\Widgets\Select2;

use dosamigos\ckeditor\CKEditor as Editor;
use borales\extensions\phoneInput\PhoneInput;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

/**
 * Class ActiveField
 * @package Phycom\Frontend\Widgets\Bootstrap4
 */
class ActiveField extends \yii\bootstrap4\ActiveField
{
    /**
     * @param array $options
     * @return ActiveField
     */
    public function honeyPot(array $options = []) : ActiveField
    {
        $this->options = [];
        $this->enableError = false;
        $this->enableLabel = false;
        $this->inputTemplate = "{input}";
        $this->template = "{input}";
        $this->inputOptions = [
            'style'       => 'position:absolute; left:-3000px;',
            'placeholder' => Yii::t('phycom/frontend/main', 'If you can see this, leave this blank.')
        ];

        return $this->textInput($options);
    }


    /**
     * @param bool $prompt
     * @param array $options
     * @return ActiveField
     */
    public function boolean($prompt = false, array $options = []) : ActiveField
    {
        if ($prompt) {
            $options = ArrayHelper::merge([
                'prompt' => is_string($prompt) ? $prompt :  Yii::t('phycom/frontend/main', 'Select')
            ], $options);
        }
        if (!isset($options['value'])) {
            $options['value'] = (int) $this->model->{$this->attribute};
        }
        return $this->dropdownList([
            '1' => Yii::t('phycom/frontend/main', 'Yes'),
            '0' => Yii::t('phycom/frontend/main', 'No'),
        ], $options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function numberDialInput(array $options = []) : ActiveField
    {
        $plusLabel = isset($options['plusLabel']) ? $options['plusLabel'] : '<i class="fas fa-plus"></i>';
        $minusLabel = isset($options['minusLabel']) ? $options['minusLabel'] : '<i class="fas fa-minus"></i>';

        if (isset($options['plusLabel'])) {
            unset($options['plusLabel']);
        }
        if (isset($options['minusLabel'])) {
            unset($options['minusLabel']);
        }

        $buttonOptions = ['class' => 'btn btn-outline-secondary'];
        if (isset($options['buttonOptions'])) {
            $buttonOptions = $options['buttonOptions'];
            unset($options['buttonOptions']);
        }

        $onclick = new JsExpression(<<<JS
        function(button, action){
            let field = button.closest(".number-dial-input"),
                input = field.getElementsByTagName("input")[0],
                downBtn = field.querySelector('.input-group-prepend .btn'),
                upBtn = field.querySelector('.input-group-append .btn'),
                min = input.getAttribute("min") || null,
                max = input.getAttribute("max") || null, 
                value = input.value;
            
            input[action]();
            
            if (null !== min && Number(input.value) <= Number(min)) {
                downBtn.setAttribute('disabled', 'disabled');
            } else {
                downBtn.removeAttribute('disabled');
            }
            
            if (null !== max && Number(input.value) >= Number(max)) {
                upBtn.setAttribute('disabled', 'disabled');
            } else {
                upBtn.removeAttribute('disabled');
            }
            
            if (input.value !== value) {
                input.dispatchEvent(new Event('change', {bubbles: true, cancelable: true}));
            }
        }
JS
);
        $minusButtonOptions = ArrayHelper::merge($buttonOptions, [
            'onclick' => "($onclick)(this, 'stepDown');"
        ]);
        if (isset($options['min']) && $options['min'] >= $this->model->{$this->attribute}) {
            $minusButtonOptions['disabled'] = 'disabled';
        }

        $plusButtonOptions = ArrayHelper::merge($buttonOptions, [
            'onclick' => "($onclick)(this, 'stepUp');"
        ]);
        if (isset($options['max']) && $options['min'] >= $this->model->{$this->attribute}) {
            $minusButtonOptions['disabled'] = 'disabled';
        }

        $this->inputTemplate = '<div class="input-group number-dial-input">' .
            '<div class="input-group-prepend">' . Html::button($minusLabel, $minusButtonOptions) . '</div>' .
                '{input}' .
            '<div class="input-group-append">' . Html::button($plusLabel, $plusButtonOptions) . '</div>' .
        '</div>';

        $options = ArrayHelper::merge([], $options);

        return $this->input('number', $options);
    }

    /**
     * @param string|bool $phoneNumberAttribute
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function phoneInput2($phoneNumberAttribute = 'phoneNumber', $options = []) : ActiveField
    {
        if (!$phoneNumberAttribute || !is_string($phoneNumberAttribute)) {
            throw new yii\base\InvalidArgumentException('Invalid phone number attribute');
        }
        $countries = Yii::$app->country->countries;
        $options = ArrayHelper::merge([
            'jsOptions' => [
                'separateDialCode' => false,
                'nationalMode' => true,
                'allowExtensions' => true,
                'formatOnDisplay' => true,
                'autoHideDialCode' => true,
                'preferredCountries' => Yii::$app->country->preferredCountries,
                'onlyCountries' => $countries,
                'allowDropdown' => empty($countries) || count($countries) > 1,
                'initialCountry' => 'auto',
                'geoIpLookup' => new JsExpression(<<<JS
                    function (callback) {
                        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    }
JS
                )
            ],
            'options' => [
                'class' => 'form-control'
            ],
        ], $options);

        $field = $this->widget(PhoneInput::class, $options);
        $phoneId = yii\helpers\Html::getInputId($this->model, $this->attribute);
        $phoneNumberId = yii\helpers\Html::getInputId($this->model, $phoneNumberAttribute);

        $field->template = "{label}\n{phone_number_input}\n{input}\n{hint}\n{error}";
        $field->parts['{phone_number_input}'] = yii\helpers\Html::activeHiddenInput($this->model, $phoneNumberAttribute);

        $this->form->options['oncountrychange'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onsubmit'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';
        $this->form->options['onfocusout'] = '$("#'.$phoneNumberId.'").val($("#'.$phoneId.'").intlTelInput("getNumber"));';

        $phoneCode = $this->model->$phoneNumberAttribute ? PhoneHelper::getPhoneCode($this->model->$phoneNumberAttribute) : null;
        $countryCode = $phoneCode ? strtolower(Country::findOneByPhoneCode($phoneCode)->code) : Yii::$app->country->defaultCountry;
        $this->form->getView()->registerJs("$('#$phoneId').intlTelInput('setCountry', '".$countryCode."');");

        Html::addCssClass($this->options, 'phone-input');

        return $field;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function checkboxCollapse(array $options = []) : ActiveField
    {
        $this->checkTemplate = "<div class=\"custom-control custom-checkbox\">\n{input}\n{label}\n{error}\n{hint}\n</div>";
        $this->options['class'] = 'collapsed d-inline-block';

        $collapseOptions = ArrayHelper::merge([
            'aria-expanded' => 'false',
        ], $options['collapse']);

        unset($options['collapse']);

        $view = Yii::$app->getView();
        CheckboxCollapseAsset::register($view);
        $inputId = $this->getInputId();
        $view->registerJs(";checkboxCollapse.init('#$inputId');");

        return $this->checkbox($collapseOptions)->label($this->model->getAttributeLabel($this->attribute), $options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function customCheckbox(array $options = []) : ActiveField
    {
        if (!isset($options['label'])) {
            $label = Html::tag('span', '', ['class' => 'custom-control-label']);
            $label .= Html::tag('span', $this->model->getAttributeLabel($this->attribute), ['class' => 'custom-control-description']);
            $options['label'] = $label;
        }

        $defaultOptions = [
            'class' => 'form-check-input custom-control-input',
            'labelOptions' => ['class' => 'custom-control custom-checkbox']
        ];

        $options = ArrayHelper::merge($defaultOptions, $options);

        $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
        $this->parts['{label}'] = '';

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        return $this;
    }

    public function dateTimePicker(array $options = []) : ActiveField
    {
        return $this->widget(DateTimePicker::class, ArrayHelper::merge([
            'clientOptions' => [
                'locale' => substr(Yii::$app->language, 0, 2),
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'bottom'
                ]
            ]
        ], $options));
    }

    public function datePicker(array $options = []) : ActiveField
    {
        $options = ArrayHelper::merge([
            'options' => ['type' => 'date'],
            'clientOptions' => [
                'locale' => substr(Yii::$app->language, 0, 2),
                'format' => 'YYYY-MM-DD',
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'bottom'
                ]
            ]
        ], $options);
        return $this->widget(DateTimePicker::class, $options);
    }

    public function timePicker(array $options = []) : ActiveField
    {
        return $this->widget(DateTimePicker::class, ArrayHelper::merge([
            'options' => ['type' => 'time'],
            'clientOptions' => [
                'locale' => substr(Yii::$app->language, 0, 2),
                'format' => 'HH:mm',
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'bottom'
                ]
            ]
        ], $options));
    }

    /**
     * Renders the CKEditor widget
     *
     * @param array $options
     * @return $this
     * @throws \Exception
     */
    public function editor(array $options = []) : ActiveField
    {
        $height = 345;

        return $this->widget(Editor::class, [
            'options' => ArrayHelper::merge(['rows' => 3, 'style' => 'height: '.$height.'px;'], $options),
            'preset' => 'custom',
            'clientOptions' => [
                'height' => $height - 71,
                'toolbar' => [
                    ['name' => 'styles', 'items' => ['Format']],
                    ['name' => 'clipboard', 'items' => ['Undo', 'Redo']],
                    ['name' => 'links', 'items' => ['Link']],
                    ['name' => 'basicstyles', 'items' => ['Bold','Italic','Strike','-','RemoveFormat']],
                    ['name' => 'paragraph', 'items' => ['NumberedList','BulletedList', '-', 'Outdent','Indent','-','Blockquote']]
                ]
            ]
        ]);
    }


    public function dropDownList2($items, array $options = []) : ActiveField
    {
        Html::addCssClass($options, 'select2 select2-xs');
        $this->labelOptions['style'] = 'display: block;';
        return $this->dropDownList($items, ArrayHelper::merge([
            'data-placeholder' => $this->model->getAttributeLabel($this->attribute)
        ], $options));
    }

    public function dropDownSelect2($items, array $options = []) : ActiveField
    {
        return $this->widget(Select2::class, ArrayHelper::merge(['items' => $items], $options));
    }

    /**
     * @param $items
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function dropDownButton($items, array $options = []) : ActiveField
    {
        $dropdownItems = [];
        foreach ($items as $value => $label) {
            $dropdownItems[] = ['label' => $label, 'value' => $value, 'url' => '#weight-' . $value, 'options' => ['data-value' => $value]];
        }

        return $this->widget(DropdownButtonInput::class, ArrayHelper::merge([
            'model' => $this->model,
            'attribute' => $this->attribute,
            'value' => $this->model->{$this->attribute},
            'dropdown' => ['items' => $dropdownItems],
            'buttonOptions' => ['class' => 'btn btn-default']
        ], $options));
    }

    /**
     * @param $value
     * @param array $options
     * @return ActiveField
     */
    public function hidden($value, array $options = []) : ActiveField
    {
        $this->template = '{input}';
        yii\helpers\Html::addCssClass($this->options, 'no-margin');
        $this->label(false);
        $this->hiddenInput($options);
        $this->inputOptions['value'] = $value;
        return $this;
    }

    /**
     * @param string $url
     * @param array $options
     * @return ActiveField
     * @throws \Exception
     */
    public function autocomplete($url, array $options = []) : ActiveField
    {
        return $this->widget(Select2::class, ArrayHelper::merge([
            'settings' => [
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => $url,
                    'delay' => 300,
                    'dataType' => 'json'
                ]
            ]
        ], $options));
    }
}
