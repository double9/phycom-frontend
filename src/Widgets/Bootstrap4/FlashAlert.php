<?php

namespace Phycom\Frontend\Widgets\Bootstrap4;

use yii\bootstrap4\Alert;

/**
 * Class FlashAlert
 *
 * @package Phycom\Frontend\Widgets\Bootstrap4
 */
class FlashAlert extends \Phycom\Base\Widgets\FlashAlert
{
    public string $alertClassName = Alert::class;
}
