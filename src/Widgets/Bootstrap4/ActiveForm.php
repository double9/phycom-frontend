<?php

namespace Phycom\Frontend\Widgets\Bootstrap4;

use Phycom\Base\Assets\ActiveFormHelperAsset;
use Phycom\Frontend\Models\Behaviors\HoneypotBehavior;

use yii\base\Model;

/**
 * Class ActiveForm
 * @package Phycom\Frontend\Widgets\Bootstrap4
 */
class ActiveForm extends \yii\bootstrap4\ActiveForm
{
    public $fieldClass = ActiveField::class;

    public function run()
    {
        $html = parent::run();
        ActiveFormHelperAsset::register($this->getView());
        return $html;
    }

    /**
     * @inheritdoc
     * @return ActiveField the created ActiveField object
     */
    public function field($model, $attribute, $options = [])
    {
        return parent::field($model, $attribute, $options);
    }

    /**
     * @param Model $model
     * @return ActiveField|string
     */
    public function honeyPotField($model)
    {
        if ($honeyPotBehavior = $model->getBehavior('honeypot')) {
            /**
             * @var HoneypotBehavior $honeyPotBehavior
             */
            return $this->field($model, $honeyPotBehavior->getHoneyPotAttribute())->honeyPot();
        }
        return '';
    }
}
