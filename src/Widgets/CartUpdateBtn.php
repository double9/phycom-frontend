<?php

namespace Phycom\Frontend\Widgets;

use Phycom\Base\Interfaces\CartInterface;

use Phycom\Frontend\Assets\CartUpdateBtnAsset;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Widget;
use yii\di\Instance;
use yii\helpers\Json;
use Yii;

/**
 * Class CartUpdateBtn
 *
 * @package Phycom\Frontend\Widgets
 */
class CartUpdateBtn extends Widget
{
    /**
     * @var CartInterface|string
     */
    public $cart = 'cart';

    /**
     * @var string
     */
    public string $label;

    /**
     * @var array|string[]
     */
    public array $options = ['class' => 'btn btn-primary'];

    /**
     * @var string
     */
    public string $cssClass = 'update-cart';

    /**
     * @var string
     */
    public string $updateUrl;


    public function init()
    {
        parent::init();
        $this->cart = Instance::ensure($this->cart, CartInterface::class);

        if (!isset($this->label)) {
            $this->label = Yii::t('phycom/frontend/checkout', 'Update cart');
        }
        if (!isset($this->updateUrl)) {
            $this->updateUrl = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/cart/update-state']);
        }
    }

    public function run()
    {
        $options = ArrayHelper::merge($this->options, ['id' => $this->getId()]);
        Html::addCssClass($options, $this->cssClass);

        $html = Html::a($this->label, Yii::$app->request->url, $options);

        $jsConfig = Json::encode([
            'selector'  => '#' . $this->getId() . '.' . $this->cssClass,
            'updateUrl' => $this->updateUrl
        ]);

        $view = $this->getView();
        CartUpdateBtnAsset::register($view);
        $view->registerJs("CartUpdateBtn.init($jsConfig);", $view::POS_END);

        return $html;
    }
}
