<?php

namespace Phycom\Frontend\Widgets;

use Phycom\Base\Interfaces\CartInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;

use Phycom\Frontend\Assets\CartWidgetAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\di\Instance;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class CartBasket
 *
 * @package Phycom\Frontend\Widgets
 */
class CartWidget extends Widget
{
    /**
     * @var array
     */
    public array $options = ['id' => 'cart-widget'];

    /**
     * @var array
     */
    public array $labelOptions = [];

    /**
     * @var array
     */
    public array $countOptions = [];
    /**
     * @var bool
     */
    public bool $showCount = true;

    /**
     * @var string
     */
    public string $countItemType = CartItemProductInterface::class;

    /**
     * @var string
     */
    public string $label = '<i class="fas fa-shopping-basket"></i>';

    /**
     * @var string|array|string[]
     */
    public $checkoutUrl = ['/cart/checkout'];

    /**
     * @var array|string
     */
    public $rippleEffect = ['color' => 'dark'];

    /**
     * @var string - a toggler button html selector when on mobile screen. Ripple effect is triggered there if cart widget is not visible
     */
    public string $navbarToggler = '.navbar-toggler';

    /**
     * @var CartInterface|string
     */
    public $cart = 'cart';

    /**
     * @var int
     */
    protected int $count;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->cart = Instance::ensure($this->cart, CartInterface::class);
        $this->count = $this->cart->getCount($this->countItemType);
    }

    public function run()
    {
        $this->id = $this->options['id'] ?? $this->getId();

        $view = $this->getView();
        CartWidgetAsset::register($view);
        $jsOptions = Json::encode([
            'el'            => '#' . $this->id,
            'rippleEffect'  => $this->rippleEffect,
            'navbarToggler' => $this->navbarToggler
        ]);
        $view->registerJs("CartWidget.create($jsOptions);");
        return $this->renderCartButton();
    }

    /**
     * @return string
     */
    protected function renderCartButton()
    {
        $options = $this->options;
        Html::addCssClass($options, 'cart-widget');

        return Html::a(
            $this->renderLabel() . ($this->showCount ? $this->renderCount() : ''),
            Url::toRoute($this->checkoutUrl),
            $options
        );
    }

    /**
     * @return string
     */
    protected function renderLabel()
    {
        $options = $this->labelOptions;
        Html::addCssClass($options, 'cart-widget-label');

        return Html::tag('span', $this->label, $options);
    }


    /**
     * @return string
     */
    protected function renderCount()
    {
        $options = $this->countOptions;
        Html::addCssClass($options, 'item-count');

        return Html::tag('span', $this->count, $options);
    }
}
