<?php

namespace Phycom\Frontend\Models\Behaviors;

use yii\base\Behavior;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\base\ModelEvent;
use Yii;

/**
 * Class HoneypotBehavior
 *
 * @package Phycom\Base\Models\Behaviors
 * @property Model $owner
 */
class HoneypotBehavior extends Behavior
{
    /**
     * @var string
     */
    private $honeyPotAttribute;

    /**
     * @param string $attributeName
     */
    public function setAttribute(string $attributeName)
    {
        if (empty($attributeName)) {
            throw new InvalidArgumentException('Invalid Honeypot attribute');
        }
        $this->honeyPotAttribute = $attributeName;
    }

    public function events()
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => [$this, 'validateHoneyPot']
        ];
    }

    public function attach($owner)
    {
        parent::attach($owner);
    }

    /**
     * @param ModelEvent $event
     */
    public function validateHoneyPot ($event)
    {
        $attribute = $this->honeyPotAttribute;
        if (isset($this->owner->$attribute) && strlen($this->owner->$attribute)) {
            $this->owner->addError(
                $attribute,
                Yii::t('phycom/frontend/main', '{attribute} is invalid', [
                    'attribute' => $this->owner->getAttributeLabel($attribute)
                ])
            );
            $event->isValid = false;
        }
    }

    /**
     * @return string
     */
    public function getHoneyPotAttribute()
    {
        return $this->honeyPotAttribute;
    }
}
