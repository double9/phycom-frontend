<?php

namespace Phycom\Frontend\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\DiscountRule;

use Yii;
use yii\db\Expression;


/**
 * Class PromotionCodeForm
 * @package Phycom\Frontend\Models
 */
class PromotionCodeForm extends yii\base\Model
{
    const PROMO_CODE_FIELD = 'code';

    use ModelTrait;

    public $code;

    protected $model;

    public function rules()
    {
        return [
            [static::PROMO_CODE_FIELD, 'trim'],
            [static::PROMO_CODE_FIELD, 'string', 'max' => 255],
            [static::PROMO_CODE_FIELD, 'required', 'enableClientValidation' => false],
            [static::PROMO_CODE_FIELD, 'validateCode'],
            [static::PROMO_CODE_FIELD, 'exist', 'targetClass' => get_class(Yii::$app->modelFactory->getDiscountRule()), 'targetAttribute' => 'code'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => Yii::t('phycom/frontend/discount-rule', 'Discount code')
        ];
    }


    public function attributeHints()
    {
        return [
            'code' => Yii::t('phycom/frontend/discount-rule', 'Enter your coupon code if you have one')
        ];
    }


    public function getModel($attribute = self::PROMO_CODE_FIELD)
    {
        if (!$this->model && $this->$attribute) {
            $condition = Yii::$app->commerce->promoCodes->caseSensitive
                ? ['code' => $this->$attribute]
                : new Expression('LOWER(code) = LOWER(:value)', ['value' => $this->$attribute]);
            $this->model = Yii::$app->modelFactory->getDiscountRule()::findOne($condition);
        }
        return $this->model;
    }


    public function validateCode($attribute, $params, $validator)
    {
        $model = $this->getModel($attribute);

        if (!$model || !$model->isValid) {
            $this->addError($attribute, Yii::t('phycom/frontend/discount-rule', 'Invalid code'));
        }
    }

    /**
     * @return false|DiscountRule
     */
    public function apply()
    {
        if ($this->validate()) {
            Yii::$app->cart->add($this->getModel());
            return $this->model;
        }
        return false;
    }
}
