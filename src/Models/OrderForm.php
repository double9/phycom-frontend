<?php

namespace Phycom\Frontend\Models;

use Phycom\Base\Helpers\PhoneHelper;
use Phycom\Base\Models\Attributes\AddressField;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Helpers\TransactionHelper;

use Phycom\Base\Interfaces\CartItemDeliveryInterface;
use Phycom\Base\Interfaces\CartItemProductInterface;

use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\OrderStatus;
use Phycom\Base\Models\Attributes\OrderItemMeta;
use Phycom\Base\Models\Attributes\ShipmentStatus;
use Phycom\Base\Models\Attributes\ShipmentType;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;

use Phycom\Base\Models\Email;
use Phycom\Base\Models\Order;
use Phycom\Base\Models\OrderItem;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\ShipmentItem;
use Phycom\Base\Models\User;

use Phycom\Base\Modules\Delivery\Interfaces\DeliveryMethodInterface;
use Phycom\Base\Modules\Delivery\Methods\DeliveryMethod;
use Phycom\Base\Modules\Delivery\Models\DeliveryType;
use Phycom\Base\Modules\Delivery\Module as DeliveryModule;

use Phycom\Base\Validators\PhoneInputValidator;

use yii\base\InvalidConfigException;
use yii\base\Model;
use Yii;

/**
 * Class OrderForm
 * @package Phycom\Frontend\Models
 */
class OrderForm extends Model
{
	use ModelTrait;

	public $email;
	public $firstName;
	public $lastName;
	public $companyName;

	public $register;
    /**
     * @var bool if this is true then order is issued to same person as marked on delivery address form. By default this is false as default implementation do not ask Personal in formation at delivery address
     */
	public $sameAsRecipient = false;

	public $phone;
	public $phoneNumber;
	public $comment;

	/**
	 * @var User|null
	 */
	protected ?User $user;

    /**
     * @var Order|null
     */
	protected ?Order $order;

	public function __construct(User $user = null, Order $order = null, array $config = [])
	{
		$this->user = $user;
		$order && $this->loadOrder($order);
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		if (empty($this->phone) && $this->user && $this->user->phone) {
			$phone = $this->user->phone;
			$this->phone = $phone->phone_nr;
			$this->phoneNumber = $phone->fullNumber;
		}
	}

    /**
     * @param Order $order
     */
	public function loadOrder(Order $order)
    {
        if ($order->user_id && $this->user && $order->user_id !== $this->user->id) {
            throw new yii\base\InvalidArgumentException('User does not match with order user');
        }
        $this->phone = $order->phone_number;
        $this->phoneNumber = '+' . $order->phone_code . $order->phone_number;
        $this->comment = $order->comment;
        $this->email = $order->email;
        $this->firstName = $order->first_name;
        $this->lastName = $order->last_name;
        $this->companyName = $order->company_name;

        $this->order = $order;
    }

	public function rules()
	{
		return [
			[['firstName','lastName', 'companyName'], 'trim'],
			[
			    ['firstName','lastName', 'email'],
                'required',
                'when' => function () {
				    return Yii::$app->user->isGuest && !$this->sameAsRecipient;
			    },
                'whenClient' => "function (attribute, value) {return !parseInt($('#orderform-sameasrecipient').val());}",
            ],
			[['firstName','lastName', 'companyName'], 'string', 'min' => 2, 'max' => 255],

			['email', 'trim'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique',
				'targetClass' => Email::class,
				'filter' => function ($query) {
					/**
					 * @var yii\db\Query $query
					 */
					$q = $query
						->innerJoin(['u' => User::tableName()], 'e.user_id = u.id')
						->from(['e' => Email::tableName()])
                        ->where(['e.email' => $this->email])
						->andWhere('e.user_id IS NOT NULL')
						->andWhere(['not', ['e.status' => ContactAttributeStatus::DELETED]]);

					return $q;
				},
				'when' => function() {
					return (Yii::$app->user->isGuest && $this->register) || (!Yii::$app->user->isGuest && !Yii::$app->user->identity->hasEmail($this->email));
				},
				'message' => Yii::t('phycom/frontend/main', 'This email address has already been taken.')
			],

			[
			    ['phoneNumber', 'phone'],
                'required',
                'when' => function () {
                    return (Yii::$app->user->isGuest || !Yii::$app->user->identity->phone) && !$this->sameAsRecipient;
                },
                'whenClient' => "function (attribute, value) {return !parseInt($('#orderform-sameasrecipient').val());}",
            ],
			['phone', 'trim'],
			['phone', PhoneInputValidator::class],

			['comment', 'trim'],
			['comment', 'string', 'max' => 1000],

			[['register', 'sameAsRecipient'], 'boolean']
		];
	}

	public function attributeLabels()
	{
        return [
            'comment'         => Yii::t('phycom/frontend/main', 'Comment'),
            'phone'           => Yii::t('phycom/frontend/main', 'Phone'),
            'email'           => Yii::t('phycom/frontend/user', 'Email'),
            'firstName'       => Yii::t('phycom/frontend/user', 'First name'),
            'lastName'        => Yii::t('phycom/frontend/user', 'Last name'),
            'companyName'     => Yii::t('phycom/frontend/user', 'Company name'),
            'register'        => Yii::t('phycom/frontend/user', 'Check if you want to create an account'),
            'sameAsRecipient' => Yii::t('phycom/frontend/user', 'Purchaser is same as recipient'),
        ];
	}

	/**
	 * @param CartItemProductInterface[] $cartItems
	 * @return Order|null|false
     * @throws \Exception
	 */
	public function create(array $cartItems)
	{
	    if ($deliveryLine = $this->getFirstDeliveryLine($cartItems)) {
	        if ($this->sameAsRecipient) {
                $this->fillAttributesFromDeliveryLine($deliveryLine);
            }
        }
	    if (!$this->validate()) {
			return false;
		}

	    $transactionHelper = new TransactionHelper();

		$order = $transactionHelper->run(function (yii\db\Transaction $transaction) use ($cartItems) {

			if ($this->register && !$this->checkCreateUser()) {
				Yii::error('Error creating user', __METHOD__);
				return $this->rollback($transaction);
			}
			if ($this->user && !empty($this->phoneNumber) && !$this->checkCreateUserPhone()) {
				return $this->rollback($transaction);
			}
			$order = Yii::$app->modelFactory->getOrder();
			$this->fillOrderAttributes($order);
			if (!$order->save()) {
				return $this->rollback($transaction, $order->errors);
			}
			if ($errors = $this->createOrderItems($order, $cartItems)) {
				return $this->rollback($transaction, $errors);
			}
            if ($errors = $this->createShipments($order, $cartItems)) {
                return $this->rollback($transaction, $errors);
            }
			return $order;
		});
        $this->order = $order ?: null;
		$this->checkLogin();
		return $order;
	}

    /**
     * @param CartItemProductInterface[]|array $cartItems
     * @return Order|bool
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
	public function update(array $cartItems)
	{
        if ($deliveryLine = $this->getFirstDeliveryLine($cartItems)) {
            if ($this->sameAsRecipient) {
                $this->fillAttributesFromDeliveryLine($deliveryLine);
            }
        }
        if (!$this->validate()) {
			return false;
        }
		if (!$order = $this->order) {
            $this->addError('error', Yii::t('phycom/frontend/main', 'Order was not found'));
		    return false;
        }
		if (!$order->status->in([OrderStatus::NEW, OrderStatus::PENDING_PAYMENT])) {
			$this->addError('error', Yii::t('phycom/frontend/main', 'Invalid order {id}', ['id' => $order->id]));
			return false;
		}
		$transaction = Yii::$app->db->beginTransaction();
		try {

			if ($this->register && !$this->checkCreateUser()) {
				Yii::error('Error creating user', __METHOD__);
				return $this->rollback($transaction);
			}
			$this->fillOrderAttributes($order);
			if (!$order->update()) {
				return $this->rollback($transaction, $order->errors);
			}
			if ($errors = $this->updateOrderItems($order, $cartItems)) {
				return $this->rollback($transaction, $errors);
			}
            foreach ($order->shipments as $shipment) {
                $shipment->delete();
            }
            unset($order->shipments);

			if ($errors = $this->createShipments($order, $cartItems)) {
                return $this->rollback($transaction, $errors);
            }

			$transaction->commit();
			$this->checkLogin();
			return $order;

		} catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
		}
	}

    /**
     * @param array $cartItems
     * @return CartItemDeliveryInterface|null
     */
    protected function getFirstDeliveryLine(array $cartItems): ?CartItemDeliveryInterface
    {
        foreach ($cartItems as $item) {
            if ($item instanceof CartItemDeliveryInterface) {
                return $item;
            }
        }
        return null;
    }


    /**
     * @param CartItemDeliveryInterface $delivery
     * @throws InvalidConfigException
     */
	protected function fillAttributesFromDeliveryLine(CartItemDeliveryInterface $delivery)
    {
        // don't fill the address attributes in case self pickup as this is the shop address not client address
        if ($delivery->getMethod() === DeliveryModule::METHOD_SELF_PICKUP) {
            return;
        }
        /**
         * @var AddressField $address
         */
        $address = Yii::createObject(AddressField::class, [$delivery->getToAddress()]);

        $this->firstName = $address->firstName;
        $this->lastName = $address->lastName;
        $this->companyName = $address->company;
        if ($address->phone) {
            $this->phone = PhoneHelper::getNationalPhoneNumber($address->phone);
            $this->phoneNumber = $address->phone;
        }
        $this->email = $address->email;
    }

	protected function createShipments(Order $order, array $cartItems)
    {
        foreach ($cartItems as $cartItem) {
            if ($cartItem instanceof CartItemDeliveryInterface) {

                $shipment = Yii::$app->modelFactory->getShipment();
                $shipment->order_id = $order->id;
                $shipment->method = $cartItem->getMethod();
                $shipment->carrier_name = $cartItem->getCarrier();
                $shipment->carrier_service = $cartItem->getService();
                $shipment->carrier_delivery_days = $cartItem->getDeliveryDays();
                $shipment->delivery_date = $cartItem->getDeliveryDate();
                $shipment->delivery_time = $cartItem->getDeliveryTime();
                $shipment->duration_terms = $cartItem->getDurationTerms();
                $shipment->dispatch_at = $cartItem->getDispatchAt();

                $shipment->shipment_id = $cartItem->getShipmentId();
                $shipment->rate_id = $cartItem->getRateId();

                // TODO: implement return address and from address also
                if ($toAddressJson = $cartItem->getToAddress()) {
                    $shipment->to_address = yii\helpers\Json::decode($toAddressJson);
                }
                $shipment->carrier_area = $cartItem->getAreaCode();

                /**
                 * @var DeliveryMethodInterface|DeliveryMethod $module
                 */
                $module = Yii::$app->getModule('delivery')->getModule($cartItem->getMethod());

                switch ($module->getType()->value) {
                    case DeliveryType::DROP_OFF:
                        $shipment->type = ShipmentType::SELF_PICKUP;
                        break;
                    case DeliveryType::DELIVERY:
                        $shipment->type = ShipmentType::DELIVERY;
                        break;
                    default:
                        $shipment->type = ShipmentType::DELIVERY;
                }
                $shipment->status = ShipmentStatus::PENDING;

                if (!$shipment->save()) {
                    return $shipment->errors;
                }
                // TODO: add here only products related to that particular shipment
                foreach ($cartItems as $item) {
                    // add only products not delivery lines or other metadata
                    if ($cartItem instanceof CartItemDeliveryInterface || !$item->product_id) {
                        continue;
                    }
                    $orderItem = OrderItem::findOne(['order_id' => $shipment->order_id, 'product_id' => $item->product_id]);

                    $shipmentItem = new ShipmentItem();
                    $shipmentItem->shipment_id = $shipment->id;
                    $shipmentItem->order_item_id = $orderItem->id;

                    if (!$shipmentItem->save()) {
                        return $shipmentItem->errors;
                    }
                }
            }
        }
        return null;
    }

	protected function fillOrderAttributes(Order $order)
	{
        $order->language_code = substr(Yii::$app->language, 0, 2);

	    if ($this->user) {
            $order->user_id = $this->user->id;
            $order->first_name = $this->user->first_name;
            $order->last_name = $this->user->last_name;
            $order->company_name = $this->user->company_name;
            $order->email = (string) $this->user->email;

            if ($phone = $this->user->phone) {
                $order->phone_code = $phone->country_code;
                $order->phone_number = $phone->phone_nr;
            }
        }

        if (!empty($this->firstName)) {
            $order->first_name = $this->firstName;
        }
        if (!empty($this->lastName)) {
            $order->last_name = $this->lastName;
        }
        if (!empty($this->companyName)) {
            $order->company_name = $this->companyName;
        }
        if (!empty($this->email)) {
            $order->email = $this->email;
        }
        if (!empty($this->phoneNumber)) {
            $order->phone_number = \Phycom\Base\Helpers\PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
            $order->phone_code = \Phycom\Base\Helpers\PhoneHelper::getPhoneCode($this->phoneNumber);
        }
        if (!empty($this->comment)) {
            $order->comment = $this->comment;
        }
		if ($order->isNewRecord) {
			$order->status = OrderStatus::NEW;
			$order->generateNumber();
		}
	}

    /**
     * @param Order $order
     * @param CartItemProductInterface[]|array $cartItems
     * @return array|null
     * @throws Yii\base\Exception
     * @throws InvalidConfigException
     */
	protected function createOrderItems(Order $order, array $cartItems)
	{
        $this->checkNumProducts($cartItems);
		foreach ($cartItems as $cartItem) {
			$orderItem = Yii::$app->modelFactory->getOrderItem();
			$orderItem->order_id = $order->id;
			$orderItem->code = $cartItem->getCode();

			$product = Yii::$app->modelFactory->getProduct()::findBySku($orderItem->code);
			if ($product) {
				$orderItem->product_id = $product->id;
				$orderItem->populateRelation('product', $product);
			} else {
				$orderItem->meta = Yii::createObject([
				    'class' => OrderItemMeta::class,
                    'title' => $cartItem->getLabel()
                ]);
			}

			if (method_exists($cartItem, 'getOptions') && $cartItem->getOptions() !== null) {
				$orderItem->product_attributes = $cartItem->getOptions();
			}
			$orderItem->quantity = method_exists($cartItem, 'getQuantity') ? $cartItem->getQuantity() : 1;
			$orderItem->num_units = method_exists($cartItem, 'getNumUnits') ? $cartItem->getNumUnits() : null;
            $orderItem->price_data = method_exists($cartItem, 'getPriceData') ? $cartItem->getPriceData() : [];
			$orderItem->price = $cartItem->getPrice();
			$orderItem->calculateTotal();

			$this->beforeSaveOrderItem($orderItem);

			if (!$orderItem->save()) {
				return $orderItem->errors;
			}
		}
		return null;
	}

    /**
     * @param Order $order
     * @param CartItemProductInterface[]|array $cartItems
     * @return array|null
     * @throws Yii\base\Exception
     * @throws \Throwable
     * @throws InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
	protected function updateOrderItems(Order $order, array $cartItems)
    {
        $this->checkNumProducts($cartItems);
        $items = [];
        foreach ($cartItems as $cartItem) {

            $code = $cartItem->getCode();
            $orderItem = null;
            foreach ($order->orderItems as $item) {
                if ($item->code === $code) {
                    $orderItem = $item;
                    break;
                }
            }
            if (!$orderItem) {
                $orderItem = Yii::$app->modelFactory->getOrderItem();
                $orderItem->code = $code;
                $orderItem->order_id = $order->id;
            }

            $product = Yii::$app->modelFactory->getProduct()::findBySku($orderItem->code);
            if ($product) {
                $orderItem->product_id = $product->id;
                $orderItem->populateRelation('product', $product);
            } else {
                /**
                 * @var OrderItemMeta|object $meta
                 */
                $meta = Yii::createObject(['class' => OrderItemMeta::class, 'title' => $cartItem->getLabel()]);
                $orderItem->meta = $meta;
            }

            if (method_exists($cartItem, 'getOptions') && $cartItem->getOptions() !== null) {
                $orderItem->product_attributes = $cartItem->getOptions();
            }
            $orderItem->quantity = method_exists($cartItem, 'getQuantity') ? $cartItem->getQuantity() : 1;
            $orderItem->num_units = method_exists($cartItem, 'getNumUnits') ? $cartItem->getNumUnits() : null;
            $orderItem->price_data = method_exists($cartItem, 'getPriceData') ? $cartItem->getPriceData() : [];
            $orderItem->price = $cartItem->getPrice();
            $orderItem->calculateTotal();

            $this->beforeSaveOrderItem($orderItem);

            if (!$orderItem->save()) {
                return $orderItem->errors;
            }
            $items[] = $orderItem->id;
        }
        $remove = Yii::$app->modelFactory->getOrderItem()::find()->where(['order_id' => $order->id])->andWhere(['not', ['id' => $items]]);
        foreach ($remove->all() as $item) {
            if (!$item->delete()) {
                return $item->errors;
            }
        }
        unset($order->orderItems);
        return null;
    }

    protected function checkNumProducts(array $cartItems)
    {
        $numProducts = 0;
        foreach ($cartItems as $cartItem) {
            if (!$cartItem instanceof CartItemDeliveryInterface) {
                $numProducts++;
            }
        }

        if ($numProducts === 0) {
            throw new yii\base\Exception('No products found in cart', 1);
        }
    }


	protected function beforeSaveOrderItem(OrderItem $orderItem)
	{

	}

	protected function checkLogin()
	{
		if (Yii::$app->user->isGuest && $this->register) {
			Yii::$app->user->login($this->user);
		}
	}

	protected function checkCreateUser()
	{
		if (!$this->user) {
			$transaction = Yii::$app->db->beginTransaction();
			try {

				$user = Yii::$app->modelFactory->getUser()::findByEmail($this->email, [
					UserStatus::ACTIVE,
					UserStatus::PENDING,
					UserStatus::PENDING_ACTIVATION,
					UserStatus::HIDDEN
				]);

				if ($user) {
					$this->addError('email', Yii::t('phycom/frontend/main', 'This email has been used before. Please login to continue'));
					return false;
				} else {
					$user = Yii::$app->modelFactory->getUser();
					$user->generateAuthKey();
				}
				$user->first_name = $this->firstName;
				$user->last_name = $this->lastName;
				$user->company_name = $this->companyName;
                $user->type = UserType::CLIENT;
                $user->status = UserStatus::PENDING_ACTIVATION;

				if (!$user->save()) {
					return $this->rollback($transaction, $user->errors);
				}

				if (!$user->email || (string)$user->email !== $this->email) {
					$email = new Email();
					$email->user_id = $user->id;
					$email->status = ContactAttributeStatus::UNVERIFIED;
					$email->email = $this->email;

					if (!$email->save()) {
						return $this->rollback($transaction, $email->errors);
					}
				}
				if ($this->register) {
					$passwordResetRequest = new PasswordResetRequestForm();
					$passwordResetRequest->email = $this->email;
					$passwordResetRequest->sendEmail();
				}
				$transaction->commit();
				return $this->user = $user;

			} catch (\Exception $e) {
				$transaction->rollBack();
				throw $e;
			}
		}
		return $this->user;
	}


	protected function checkCreateUserPhone()
	{
		if (!$this->user) {
            Yii::error('User was not found when trying to create phone in checkout', __METHOD__);
			return false;
		}
		if ($phone = $this->user->hasPhone($this->phoneNumber)) {
		    return $phone;
        }

		$phone = new Phone();
		$phone->user_id = $this->user->id;
		$phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
		$phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);
		$phone->status = ContactAttributeStatus::UNVERIFIED;

		if (!$phone->save()) {
			Yii::error($phone->errors);
			$errMsg = Yii::t('phycom/frontend/main', 'Error saving phone number');
			$this->addError('phone', $errMsg);
			return false;
		}
		return $phone;
	}
}
