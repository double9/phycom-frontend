<?php
namespace Phycom\Frontend\Models;


use Phycom\Base\Models\User;

use Phycom\Frontend\Models\Behaviors\HoneypotBehavior;
use yii\base\Model;
use yii\base\InvalidArgumentException;
use Yii;

/**
 * Password reset form in case of a lost password
 *
 * Class PasswordResetForm
 * @package Phycom\Frontend\Models
 */
class PasswordResetForm extends Model
{
    public $name;

    public $password;
    /**
     * @var \Phycom\Base\Models\User
     */
    private $user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidArgumentException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        if (!$this->user = User::findByPasswordResetToken($token)) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['name', 'safe']
        ];
    }

    public function attributeLabels()
    {
	    return [
	        'password' => Yii::t('phycom/frontend/main', 'Password')
	    ];
    }

	/**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
