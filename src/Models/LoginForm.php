<?php

namespace Phycom\Frontend\Models;

use Phycom\Frontend\Models\Behaviors\HoneypotBehavior;
use Yii;

/**
 * Class LoginForm
 *
 * @package Phycom\Frontend\Models
 */
class LoginForm extends \Phycom\Base\Models\LoginForm
{
    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['username'] = Yii::t('phycom/frontend/main', 'Email');
		return $labels;
	}
}
