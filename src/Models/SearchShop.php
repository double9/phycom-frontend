<?php

namespace Phycom\Frontend\Models;

use Geocoder\Model\Coordinates;
use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\AddressMeta;
use Phycom\Base\Models\Attributes\ShopStatus;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Shop;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use yii\db\Query;
use Yii;

/**
 * Class SearchShop
 * @package Phycom\Frontend\Models
 */
class SearchShop extends Shop implements SearchModelInterface
{
	public $phone;

	public $createdFrom;

	public $createdTo;

	public $updatedFrom;

	public $updatedTo;

	public $province;

	public $lat;

	public $lng;

	public $radius;

	public $hideInactive = true;

	public function rules()
	{
		return [
			[['id', 'vendor_id', 'phone', 'order'], 'integer'],
            [['lat', 'lng', 'radius'], 'number'],
			[['created_at', 'updated_at', 'createdFrom', 'createdTo', 'updatedFrom', 'updatedTo', 'status', 'type'], 'safe'],
			[['name', 'province'], 'string', 'max' => 255]
		];
	}

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $formName
     * @return ActiveDataProvider
     * @throws \yii\base\Exception
     */
	public function search(array $params = [], string $formName = '')
	{
		$query = $this->createSearchQuery();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['order' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

		$this->sort($dataProvider->sort);

		if (!($this->load($params, $formName) && $this->validate())) {
			return $dataProvider;
		}

        $query->andFilterWhere([
            's.id'        => $this->id,
            's.vendor_id' => $this->vendor_id,
            's.type'      => is_array($this->type)
                ? array_map(fn($item) => (string) $item, $this->type)
                : (string) $this->type,
            's.status'    => is_array($this->status)
                ? array_map(fn($item) => (string) $item, $this->status)
                : (string) $this->status,
        ]);

		if ($this->lat && $this->lng && $this->radius) {

            /**
             * https://blog.fedecarg.com/2009/02/08/geo-proximity-search-the-haversine-equation/
             */
		    $radius = $this->radius;
            $lngMin = $this->lng - $radius / abs(cos(deg2rad($this->lat)) * 69);
            $lngMax = $this->lng + $radius / abs(cos(deg2rad($this->lat)) * 69);
            $latMin = $this->lat - ($radius / 69);
            $latMax = $this->lat + ($radius / 69);

            $query->andWhere(new Expression('am.lat >= :latMin AND am.lat <= :latMax AND am.lng >= :lngMin AND am.lng <= :lngMax', [
                'latMin' => $latMin,
                'latMax' => $latMax,
                'lngMin' => $lngMin,
                'lngMax' => $lngMax
            ]));

        } else {
            $query->andFilterWhere(['a.province' => $this->province]);
        }
		$query->andFilterWhere(['like','p.phone_nr', $this->phone]);
		return $dataProvider;
	}

	protected function sort(Sort $sort)
	{

	}

    /**
     * @return ActiveQuery
     * @throws \yii\base\Exception
     */
	protected function createSearchQuery()
	{
		$query = Yii::$app->modelFactory->getShop()::find();
		$query->select([
			's.*',
            'CONCAT(p.country_code, p.phone_nr) AS phone'
		]);
		$query->from(['s' => Shop::tableName()]);

		$query->innerJoin(['a' => Address::tableName()], 'a.shop_id = s.id');
        $query->leftJoin(['am' => AddressMeta::tableName()], 'am.address_id = a.id');

		$query->leftJoin(['p' => 'LATERAL('.
			(new Query())
				->select('pp.*')
				->from(['pp' => Phone::tableName()])
				->where('pp.shop_id = s.id')
				->andWhere('pp.status != :status_deleted')
				->orderBy(['pp.id' => SORT_DESC . ' NULLS LAST','pp.updated_at' => SORT_DESC])
				->limit(1)
				->createCommand()
				->sql
			.')'
		], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

		$query->where(['not', ['s.status' => ShopStatus::DELETED]]);

		if ($this->hideInactive) {
		    $query->andWhere(['not', ['s.status' => ShopStatus::INACTIVE]]);
        }

        return $query;
	}
}
