<?php

namespace Phycom\Frontend\Models\Vendor;

use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Attributes\VendorStatus;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Address;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Vendor;

use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\data\Sort;
use yii\db\Query;
use Yii;

/**
 * Class SearchVendor
 *
 * @package Phycom\Frontend\Models
 */
class SearchVendor extends Vendor implements SearchModelInterface
{
    use SearchQueryFilter;

    public ?string $address = null;
    public ?string $phone = null;
    public ?string $createdFrom = null;
    public ?string $createdTo = null;
    public ?string $updatedFrom = null;
    public ?string $updatedTo = null;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'legal_name', 'reg_number'], 'string', 'max' => 255],
            [['address'], 'string'],
            [['created_at', 'updated_at', 'createdFrom', 'createdTo', 'updatedFrom', 'updatedTo', 'status', 'type', 'phone'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'address' => Yii::t('phycom/frontend/main', 'Address'),
            'phone'   => Yii::t('phycom/frontend/main', 'Phone'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $formName
     * @return VendorDataProvider
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params = [], string $formName = ''): VendorDataProvider
    {
        $query = $this->createSearchQuery();
        /**
         * @var VendorDataProvider|object $dataProvider
         */
        $dataProvider = Yii::createObject([
            'class'      => VendorDataProvider::class,
            'query'      => $query,
            'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

        $this->sort($dataProvider->sort);

        if (!($this->load($params, $formName) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'v.id'     => $this->id,
            'v.status' => (string)$this->status,
            'v.type'   => (string)$this->type
        ]);

        $query->filterText('v.name', $this->name);
        $query->filterText('v.legal_name', $this->legal_name);
        $query->andFilterWhere(['like','p.phone_nr', $this->phone]);
        $query->filterFullName(['a.street', 'a.district', 'a.city', 'a.province', 'a.postcode', 'a.locality'], $this->address);
        $query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
        $query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);

        return $dataProvider;
    }

    protected function sort(Sort $sort)
    {

    }

    /**
     * @return ActiveQuery
     * @throws Exception
     */
    protected function createSearchQuery(): ActiveQuery
    {
        $query = static::find();
        $query->select([
            'v.*',
            'row_to_json(a) AS address',
            'CONCAT(p.country_code, p.phone_nr) AS phone'
        ]);
        $query->from(['v' => Vendor::tableName()]);

        $query->leftJoin(['a' => 'LATERAL('.
            (new Query())
                ->select('aa.*')
                ->from(['aa' => Address::tableName()])
                ->where('aa.vendor_id = v.id')
                ->andWhere('aa.status != :status_deleted')
                ->orderBy(['aa.id' => SORT_DESC . ' NULLS LAST','aa.updated_at' => SORT_DESC])
                ->limit(1)
                ->createCommand()
                ->sql
            .')'
        ], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

        $query->leftJoin(['p' => 'LATERAL('.
            (new Query())
                ->select('pp.*')
                ->from(['pp' => Phone::tableName()])
                ->where('pp.vendor_id = v.id')
                ->andWhere('pp.status != :status_deleted')
                ->orderBy(['pp.id' => SORT_DESC . ' NULLS LAST','pp.updated_at' => SORT_DESC])
                ->limit(1)
                ->createCommand()
                ->sql
            .')'
        ], 'true', ['status_deleted' => ContactAttributeStatus::DELETED]);

        $query->where(['not', ['v.status' => VendorStatus::DELETED]]);

        if (isset($this->main) && null !== $this->main) {
            $query->andWhere(['v.main' => (bool)$this->main]);
        }

        return $query;
    }
}
