<?php

namespace Phycom\Frontend\Models\Vendor;

use yii\data\ActiveDataProvider;

/**
 * Class VendorDataProvider
 * @package Phycom\Frontend\Models\Vendor
 *
 * @property SearchVendor[] $models
 * @method SearchVendor[] getModels()
 */
class VendorDataProvider extends ActiveDataProvider
{

}
