<?php
namespace Phycom\Frontend\Models;


use Phycom\Base\Models\User;

use yii\base\Model;
use Yii;

/**
 * Password update form for a regular password update
 *
 * Class PasswordUpdateForm
 * @package Phycom\Frontend\Models
 */
class PasswordChangeForm extends Model
{
    public $password;
    public $newPassword;
    public $repeatPassword;

    /**
     * @var \Phycom\Base\Models\User
     */
    private $user;

    /**
     * @param User $user
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidArgumentException if token is empty or not valid
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'newPassword', 'repeatPassword'], 'required'],
            [['password', 'newPassword', 'repeatPassword'], 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('phycom/frontend/main', "Passwords don't match")],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'       => Yii::t('phycom/frontend/main', 'Password'),
            'newPassword'    => Yii::t('phycom/frontend/main', 'New Password'),
            'repeatPassword' => Yii::t('phycom/frontend/main', 'Repeat Password'),
        ];
    }

    public function afterValidate()
    {
        if (!$this->user->validatePassword($this->password)) {
            $this->addError('password', Yii::t('phycom/frontend/main', 'Wrong password'));
        }
        parent::afterValidate();
    }

    /**
     * Updates password.
     *
     * @return bool if password was reset.
     */
    public function update()
    {
        if ($this->validate()) {
            $user = $this->user;
            $user->setPassword($this->newPassword);
            return $user->save(false);
        }
        return false;
    }
}
