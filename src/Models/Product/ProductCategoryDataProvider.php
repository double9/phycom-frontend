<?php

namespace Phycom\Frontend\Models\Product;

use yii\data\ActiveDataProvider;

/**
 * Class ProductCategoryDataProvider
 * @package Phycom\Frontend\Models\Product
 *
 * @property SearchProductCategory[] $models
 * @method SearchProductCategory[] getModels()
 */
class ProductCategoryDataProvider extends ActiveDataProvider
{

}
