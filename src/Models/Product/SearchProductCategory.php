<?php

namespace Phycom\Frontend\Models\Product;

use Phycom\Base\Helpers\Url;
use Phycom\Base\Models\Traits\SearchQueryFilter;
use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Attributes\CategoryStatus;;
use Phycom\Base\Models\Attributes\ProductStatus;
use Phycom\Base\Models\Language;
use Phycom\Base\Models\Product\ProductCategory;
use Phycom\Base\Components\ActiveQuery;
use Phycom\Base\Models\Product\ProductTag;
use Phycom\Base\Models\Traits\TreeBuilder;
use Phycom\Base\Models\Translation\ProductCategoryTranslation;

use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\Expression;
use Yii;

/**
 * Class SearchProductCategory
 * @package Phycom\Frontend\Models\Product
 *
 * @property string $label
 * @property SearchProduct[] $activeProducts
 */
class SearchProductCategory extends ProductCategory implements SearchModelInterface
{
	use SearchQueryFilter;
	use TreeBuilder;

	/**
	 * @var Language
	 */
	public $language;

	public $title;
	public $urlKey;
//	public $urlLanguage;
	public $description;

	public $updatedFrom;
	public $updatedTo;

	public $createdFrom;
	public $createdTo;

	public $topLevel = false;
	public $path;

	protected $productTagOrder = [
		ProductTag::TAG_FEATURED,
		ProductTag::TAG_NEW,
		ProductTag::TAG_BESTSELLER,
		ProductTag::TAG_DISCOUNTED
	];

    public function init()
	{
		parent::init();
		$this->language = Yii::$app->lang->current;
	}

	public function rules()
	{
		return [
			[['id', 'shop_id', 'created_by', 'parent_id'], 'integer'],
			[['title'], 'string'],
            [['topLevel', 'featured'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
			[['created_at','updated_at','createdFrom','createdTo','updatedFrom','updatedTo', 'status'], 'safe'],
            [['urlKey'], 'string', 'max' => 255],
		];
	}

	public function getLabel()
	{
		return $this->title;
	}

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::toFeRoute($this->getRoute());
    }

	public function getRoute()
    {
        return ['product/index', 'category' => $this->urlKey, 'url-language' => Yii::$app->language];
    }

	public function search(array $params = [], $recursive = false)
	{
		$query = $recursive ? $this->createRecursiveSearchQuery() : $this->createSearchQuery();
		$dataProvider = new ProductCategoryDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['order' => SORT_ASC]],
			'pagination' => false,
		]);

		$this->sort($dataProvider->sort);

		if ($this->load($params) && !$this->validate()) {
		    return $dataProvider;
        }

		$query->andFilterWhere([
			'c.id' => $this->id,
			'c.status' => (string)$this->status,
            'c.featured' => $this->featured,
            't1.url_key' => $this->urlKey
		]);

		$query->filterMultiAttribute(['t1.title','t2.title'], $this->title);
		$query->filterDateRange('c.created_at', $this->createdFrom, $this->createdTo);
		$query->filterDateRange('c.updated_at', $this->updatedFrom, $this->updatedTo);

		return $dataProvider;
	}


	public function searchMenuItems($setParentUrl = true, $recursive = false)
    {
        $dataProvider = $this->search([], $recursive);
        $items = $this->buildTree($dataProvider->getModels(), null, $setParentUrl);
        return $items;
    }

    /**
     * @param array $status
     * @param array $config - search model configuration
     * @return ProductDataProvider|ActiveDataProvider
     * @throws yii\base\Exception
     */
    public function searchProducts($status = [ProductStatus::ACTIVE, ProductStatus::INACTIVE], array $config = [])
    {
        $searchModel = Yii::$app->modelFactory->getSearchProduct($config);
        $searchModel->categoryId = $this->id;
        return $searchModel->search([], $status);
    }

    /**
     * @return array|SearchProduct[]|yii\db\ActiveQuery
     * @throws yii\base\Exception
     */
	public function getActiveProducts()
	{
		return $this->searchProducts(ProductStatus::ACTIVE)->getModels();
	}

	protected function sort(Sort $sort)
	{
		$sort->attributes['title'] = [
			'asc' => [new Expression('COALESCE(t1.title, t2.title) ASC')],
			'desc' => [new Expression('COALESCE(t1.title, t2.title) DESC')],
		];
	}

    /**
     * @return ActiveQuery
     * @throws yii\base\Exception
     */
	protected function createRecursiveSearchQuery()
    {
        $query = static::find();
        $query->sql = 'WITH RECURSIVE categories AS (
          SELECT
            c1.*,
            (CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END) AS title,
            (CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END) AS description,
            (CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END) AS "urlKey",
            ARRAY[(CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END) :: TEXT] AS key_path
          FROM product_category c1
            LEFT JOIN product_category_translation t1 ON(t1.product_category_id = c1.id AND t1.language = :lang)
            LEFT JOIN product_category_translation t2 ON(
              (
                t2.product_category_id = c1.id
                AND t2.language != :lang
                AND t2.language = :fallback_lang
                AND t2.title IS NOT NULL
                AND t2.url_key IS NOT NULL
              )
              OR
              (
                t2.product_category_id = c1.id
                AND t2.language != :lang
                AND t2.title IS NOT NULL
                AND t2.url_key IS NOT NULL
              )
            )
          WHERE c1.parent_id is null
        
          UNION ALL
        
          SELECT
            c2.*,
            (CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END) AS title,
            (CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END) AS description,
            (CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END) AS "urlKey",
            ARRAY_APPEND(t0.key_path, (CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END) :: TEXT) AS key_path
          FROM product_category c2
            LEFT JOIN product_category_translation t1 ON(t1.product_category_id = c2.id AND t1.language = :lang)
            LEFT JOIN product_category_translation t2 ON(
              (
                t2.product_category_id = c2.id
                AND t2.language != :lang
                AND t2.language = :fallback_lang
                AND t2.title IS NOT NULL
                AND t2.url_key IS NOT NULL
              )
              OR
              (
                t2.product_category_id = c2.id
                AND t2.language != :lang
                AND t2.title IS NOT NULL
                AND t2.url_key IS NOT NULL
              )
            )
            INNER JOIN categories t0 ON t0.id = c2.parent_id
        )
        SELECT
          c.*,
          array_length(c.key_path, 1) - 1 as level,
          ARRAY_TO_JSON(c.key_path) as path
        FROM categories c
        WHERE c.status = :status ORDER BY c.parent_id NULLS FIRST, c.order ASC;';

        $query->params = [
            'lang' => $this->language->code,
            'fallback_lang' => Yii::$app->lang->source->code,
            'status' => CategoryStatus::VISIBLE
        ];

        return $query;
    }

	/**
	 * @return ActiveQuery
     * @throws yii\base\Exception
	 */
	protected function createSearchQuery()
	{
		$query = static::find();
        $query->select([
            'c.*',
            'CASE WHEN (t1.title IS NOT NULL) THEN t1.title ELSE t2.title END AS "title"',
            'CASE WHEN (t1.description IS NOT NULL) THEN t1.description ELSE t2.description END AS "description"',
            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.url_key ELSE t2.url_key END AS "urlKey"',
//            'CASE WHEN (t1.url_key IS NOT NULL) THEN t1.language ELSE t2.language END AS "urlLanguage"',
        ]);
		$query->from(['c' => ProductCategory::tableName()]);
		$query->where(['c.status' => CategoryStatus::VISIBLE]);

		$query->leftJoin(['t1' => ProductCategoryTranslation::tableName()], 't1.product_category_id = c.id AND t1.language = :lang');
		$query->leftJoin(['t2' => ProductCategoryTranslation::tableName()], [
		    'or',
            // fallback to default language
            [
                'and',
                't2.product_category_id = c.id',
                't2.language = :fallback_lang',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ],
            // or if there is no record with default language then any other language with content
            [
                'and',
                't2.product_category_id = c.id',
                't2.language != :lang',
                't2.title IS NOT NULL',
                't2.url_key IS NOT NULL'
            ]
        ]);
		$query->addParams([
			'lang' => $this->language->code,
			'fallback_lang' => Yii::$app->lang->source->code
		]);

		if ($this->topLevel) {
		    $query->andWhere('c.parent_id IS NULL');
        } elseif ($this->parent_id) {
            $query->andWhere(['c.parent_id' => $this->parent_id]);
        }

		return $query;
	}
}
