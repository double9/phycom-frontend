<?php

namespace Phycom\Frontend\Models\Product;

use yii\data\ActiveDataProvider;

/**
 * Class ProductDataProvider
 * @package Phycom\Frontend\Models\Product
 *
 * @property SearchProduct[] $models
 * @method SearchProduct[] getModels()
 */
class ProductDataProvider extends ActiveDataProvider
{

}
