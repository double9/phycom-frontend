<?php

namespace Phycom\Frontend\Models\Product;


use Phycom\Base\Models\Attributes\ProductVariantStatus;
use Phycom\Base\Models\CustomField;
use Phycom\Base\Models\Product\Product;
use Phycom\Base\Models\Product\ProductVariant;
use Phycom\Base\Models\Product\Variant;
use Phycom\Base\Models\Traits\ModelTrait;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ProductSelection
 *
 * @package Phycom\Frontend\Models\Product
 */
class ProductSelection extends Model
{
    use ModelTrait;

    /**
     * @var number
     */
    public $quantity;

    /**
     * @var number
     */
    public $units;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductVariant[]
     */
    protected $productVariants;

    /**
     * @var array - key value pairs where key is variant name and value is the option value
     */
    protected $options = [];

    /**
     * CartForm constructor.
     *
     * @param Product $product
     * @param array $config
     */
    public function __construct(Product $product, array $config = [])
    {
        $this->product = $product;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = [
            ['quantity', 'integer', 'min' => 1],
            ['units', 'number', 'min' => 0.01],
            ['units', 'default', 'value' => null]
        ];
        foreach ($this->getProductVariants() as $productVariant) {
//            if ($this->hasVariant($productVariant->name)) {
                switch ($productVariant->variant->type) {
                    case Variant::TYPE_BOOL:
                        $rules[] = [$productVariant->name, 'boolean'];
                        break;
                    case Variant::TYPE_NUMBER:
                        $rules[] = [$productVariant->name, 'number'];
                        break;
                    case Variant::TYPE_TEXT:
                        $rules[] = [$productVariant->name, 'string'];
                        break;
                    default:
                        $rules[] = [$productVariant->name, 'safe'];
                }
//            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(
            [
                'quantity' => Yii::t('phycom/frontend/main', 'Quantity'),
                'units'    => $this->product->price_unit->label
            ],
            ArrayHelper::map($this->getProductVariants(), 'name', 'label')
        );
    }

    public function init()
    {
        parent::init();
        $this->loadProductVariants();
        $this->assignDefaultValues();
    }

    public function assignDefaultValues()
    {
        if (!$this->quantity) {
            $this->quantity = 1;
        }
        if (!$this->units && $this->product->hasCustomPriceUnits()) {
            $this->units = $this->product->prices[0]->num_units;
        }
    }

    public function beforeValidate()
    {
        if ($this->units) {
            $this->units = (float) $this->units;
        }
        return parent::beforeValidate();
    }

    /**
     * If this is true we can show input field for unit selection
     * @return bool
     */
    public function canSelectUnits()
    {
        return $this->product->hasCustomPriceUnits();
    }

    /**
     * @return ProductVariant[]
     */
    public function getProductVariants()
    {
        return $this->loadProductVariants()->productVariants;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->loadProductVariants()->options;
    }

    /**
     * Returns an array of selected options where key is variant name and value is array of selected option keys.
     * @return array
     */
    public function getSelectedOptionKeys()
    {
        $variantOptionKeys = [];
        foreach ($this->getProductVariants() as $productVariant) {
            $optionValue = $this->getOptions()[$productVariant->name];
            if (null === $optionValue) {
                continue;
            }
            switch ($productVariant->variant->type) {
                case CustomField::TYPE_OPTION:
                    $optionKeys = [$optionValue];
                    break;
                case CustomField::TYPE_SELECT:
                    $optionKeys = is_array($optionValue) ? $optionValue : [$optionValue];
                    break;
                case CustomField::TYPE_NUMBER:
                    $optionKeys = [[$productVariant->options[0]->key, $optionValue]];
                    break;
                default:
                    $optionKeys = [$productVariant->options[0]->key];
            }
            $variantOptionKeys[$productVariant->name] = $optionKeys;
        }
        return $variantOptionKeys;
    }

    /**
     * @return array
     */
    public function getPrice()
    {
        $prices = $this->product->getPrice($this->units)->getTotalPrice($this->getSelectedOptionKeys());

        if ($this->quantity > 1) {
            foreach ($prices as $key => $price) {
                $prices[$key] = $price * $this->quantity;
            }
        }

        return $prices;
    }

    /**
     * Set options as regular properties
     *
     * @param string $name
     * @param mixed $value
     * @return mixed|void
     * @throws yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if ($this->hasVariant($name)) {
            return $this->options[$name] = $value;
        }
        return parent::__set($name, $value);
    }

    /**
     * Make options accessible as regular properties
     *
     * @param string $name
     * @return mixed
     * @throws yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if ($this->hasVariant($name)) {
            return $this->getOptions()[$name];
        }
        return parent::__get($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasVariant(string $name)
    {
        return array_key_exists($name, $this->getOptions());
    }


    /**
     * @param string $name
     * @return mixed|null
     */
    public function getVariant(string $name)
    {
        if ($this->hasVariant($name)) {
            return $this->getOptions()[$name];
        }
        return null;
    }

    /**
     * Loads the list of available product options
     */
    protected function loadProductVariants()
    {
        if (!$this->productVariants) {
            $this->productVariants = $this->product->getProductVariants()->andWhere(['status' => ProductVariantStatus::VISIBLE])->all();

            $variants = ArrayHelper::getColumn($this->productVariants, 'name');
            foreach ($this->options as $variantName => $value) {
                if (!in_array($variantName, $variants)) {
                    unset($this->options[$variantName]);
                }
            }
            foreach ($this->productVariants as $productVariant) {
                if (!array_key_exists($productVariant->name, $this->options)) {
                    $this->options[$productVariant->name] = null;
                }
            }
        }
        return $this;
    }

    /**
     * @param mixed $key
     */
    protected function unsetOption($key)
    {
        if (array_key_exists($key, $this->getOptions())) {
            $this->options[$key] = null;
        }
    }
}
