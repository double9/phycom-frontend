<?php

namespace Phycom\Frontend\Models;

use Phycom\Base\Models\File;
use Phycom\Base\Models\OrderItem;
use Phycom\Frontend\Models\Product\ProductSelection;

use Phycom\Base\Components\FileStorage;

use Yii;

/**
 * Class CartForm
 * @package Phycom\Frontend\Models
 */
class CartForm extends ProductSelection
{
    /**
     * @return string
     */
    public function getProductCode(): string
    {
        $productPrice = $this->product->getPrice($this->units)->getProductPrice();
        return $productPrice->sku ?: $this->product->sku;
    }


    /**
     * @return bool|\Phycom\Base\Models\OrderItem
     * @throws yii\base\Exception
     */
    public function createOrderItem()
    {
        if (!$this->validate()) {
            return false;
        }

        $orderItem = Yii::$app->modelFactory->getOrderItem();
        $orderItem->code = $this->getProductCode();
        $orderItem->product_id = $this->product->id;
        $orderItem->quantity = $this->quantity;
        $orderItem->num_units = $this->units;

        $price = $this->product->getPrice($this->units);
        $priceOptionKeys = $this->getSelectedOptionKeys();
        $prices = $price->getTotalPrice($priceOptionKeys);
        $orderItem->price = $prices[0];
        $priceData = [$price->getPrice()];

        foreach ($this->getOptions() as $variantName => $value) {
            if (is_null($value) || empty($value)) {
                continue;
            }
            if ($this->addOptionToOrderItem($orderItem, $variantName, $value)) {

                if (isset($priceOptionKeys[$variantName])) {
                    $optionPrice = 0;
                    foreach ($priceOptionKeys[$variantName] as $optionKey) {
                        if (is_array($optionKey)) {
                            [$optionKey, $optionValue] = $optionKey;
                        } else {
                            $optionValue = null;
                        }
                        $optionPrice += $price->getOptionPrice($variantName, $optionKey, $optionValue);
                    }
                    $priceData[$variantName] = $optionPrice;
                }

            } else {
                Yii::error('Error saving order item ' . $orderItem->code . ' variant ' . $variantName);
                return false;
            }
        }

        $orderItem->price_data = $priceData;
        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     * @param string $variantName
     * @param mixed $value
     * @return bool
     * @throws yii\base\Exception
     * @throws yii\base\InvalidConfigException
     */
    protected function addOptionToOrderItem(OrderItem $orderItem, string $variantName, $value)
    {
        if ($value instanceof File) {
            $filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . pathinfo($value->filename, PATHINFO_EXTENSION);
            $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_TEMP);
            $content = Yii::$app->fileStorage->getBucket($value->bucket)->getFileContent($value->filename);
            if (!$bucket->saveFileContent($filename, $content)) {
                $this->addError($variantName, Yii::t('phycom/frontend/main', 'Error creating temp file'));
                return false;
            }
            $orderItem->setOption($variantName, $filename);
        } elseif ($value instanceof yii\web\UploadedFile) {
            $filename = time() . '_' . Yii::$app->security->generateRandomString(32) . '.' . $value->extension;
            $bucket = Yii::$app->fileStorage->getBucket(FileStorage::BUCKET_TEMP);
            if (!$bucket->copyFileIn($value->tempName, $filename)) {
                $this->addError($variantName, Yii::t('phycom/frontend/main', 'Error saving uploaded file'));
                return false;
            }
            $orderItem->setOption($variantName, $filename);
        } else {
            $orderItem->setOption($variantName, $value);
        }
        return true;
    }
}
