<?php

namespace Phycom\Frontend\Models;

use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Attributes\UserStatus;
use Phycom\Base\Models\Attributes\UserType;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\User;
use Phycom\Base\Validators\PhoneInputValidator;
use Phycom\Base\Helpers\PhoneHelper;

use Phycom\Frontend\Models\Behaviors\HoneypotBehavior;
use yii\base\Model;
use yii\db\ActiveQueryInterface;
use Yii;


/**
 * Signup form
 */
class SignupForm extends Model
{
	use ModelTrait;

    public $email;
    public $firstName;
    public $lastName;
    public $phoneNumber;
    public $phone;
    public $companyName;
    public $password;

    public $name;

    public function behaviors()
    {
        return [
            'honeypot' => [
                'class'     => HoneypotBehavior::class,
                'attribute' => 'name'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName','lastName', 'companyName'], 'trim'],
	        [['firstName','lastName'], 'required'],
            [['firstName', 'lastName'], 'string', 'min' => 2, 'max' => 100],
            [['companyName'], 'string', 'min' => 2, 'max' => 200],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'min' => 6, 'max' => 254],
	        ['email', 'unique',
		        'targetClass' => Email::class,
		        'filter' => function ($query) {
			        /**
			         * @var yii\db\Query $query
			         */
			        return $query
				        ->andWhere('user_id IS NOT NULL')
				        ->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
	            },
		        'message' => 'This email address has already been taken.'
	        ],
            ['phoneNumber', 'required', 'when' => function(){
                    return strlen($this->phone);
                }
            ],
	        ['phone', 'trim'],
	        ['phone', PhoneInputValidator::class],
	        [
		        'phone',
		        'unique',
		        'targetAttribute' => 'phone_nr',
		        'targetClass' => Phone::class,
		        'message' => Yii::t('phycom/frontend/error', 'There is already an account registered with the entered phone number.'),
		        'filter' => function (ActiveQueryInterface $query) {
			        $query->where(['phone_nr' => \Phycom\Base\Helpers\PhoneHelper::getNationalPhoneNumber($this->phoneNumber)]);
                    $query->andWhere(['not', ['user_id' => Yii::$app->user->id]]);
			        $query->andWhere(['not', ['status' => ContactAttributeStatus::DELETED]]);
		        },
	        ],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['name', 'safe']
        ];
    }


    public function attributeLabels()
    {
        return [
            'email'       => Yii::t('phycom/frontend/user', 'Email'),
            'firstName'   => Yii::t('phycom/frontend/user', 'First name'),
            'lastName'    => Yii::t('phycom/frontend/user', 'Last name'),
            'phone'       => Yii::t('phycom/frontend/user', 'Phone'),
            'companyName' => Yii::t('phycom/frontend/user', 'Company name'),
            'password'    => Yii::t('phycom/frontend/user', 'Password')
        ];
    }

	/**
     * Signs user up.
     *
     * @return User|false the saved model or false if saving fails
     * @throws \Exception
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
	        $user = Yii::$app->modelFactory->getUser();
	        $user->first_name = $this->firstName;
	        $user->last_name = $this->lastName;
	        $user->type = UserType::CLIENT;
	        $user->status = UserStatus::PENDING_ACTIVATION;
	        $user->setPassword($this->password);
	        $user->generateAuthKey();

	        if ($this->companyName) {
	        	$user->company_name = $this->companyName;
	        }

			if (!$user->save()) {
				return $this->rollback($transaction, $user->errors);
			}

	        $email = new Email();
	        $email->user_id = $user->id;
	        $email->status = ContactAttributeStatus::UNVERIFIED;
	        $email->email = $this->email;

	        if (!$email->save()) {
		        return $this->rollback($transaction, $email->errors);
	        }

	        if ($this->phone) {
		        $phone = new Phone();
		        $phone->user_id = $user->id;
		        $phone->status = ContactAttributeStatus::UNVERIFIED;
		        $phone->phone_nr = PhoneHelper::getNationalPhoneNumber($this->phoneNumber);
		        $phone->country_code = PhoneHelper::getPhoneCode($this->phoneNumber);

		        if (!$phone->save()) {
			        return $this->rollback($transaction, $phone->errors);
		        }
	        }

	        $transaction->commit();
			return $user;

        } catch (\Exception $e) {
			$transaction->rollBack();
			throw $e;
        }
    }
}
