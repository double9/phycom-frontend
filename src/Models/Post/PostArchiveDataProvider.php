<?php

namespace Phycom\Frontend\Models\Post;

use yii\data\ActiveDataProvider;

/**
 * Class ArchiveDataProvider
 * @package Phycom\Frontend\Models\Post
 *
 * @property SearchPostArchive[] $models
 * @method SearchPostArchive[] getModels()
 */
class PostArchiveDataProvider extends ActiveDataProvider
{

}
