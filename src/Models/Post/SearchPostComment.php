<?php

namespace Phycom\Frontend\Models\Post;


use Phycom\Base\Interfaces\SearchModelInterface;
use Phycom\Base\Models\Attributes\CommentStatus;
use Phycom\Base\Models\Comment;
use Phycom\Base\Models\Post;
use Phycom\Base\Models\PostComment;
use Phycom\Base\Models\Traits\SearchQueryFilter;

use yii\data\Sort;


/**
 * Class SearchPostComment
 * @package Phycom\Frontend\Models\Post
 */
class SearchPostComment extends Comment implements SearchModelInterface
{
    use SearchQueryFilter;

    /**
     * @var static[]
     */
    public $children = [];

    protected $post;

    public function setPost(Post $post)
    {
        $this->post = $post;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function search(array $params = [])
    {
        $query = $this->createSearchQuery();
        $dataProvider = new PostCommentDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]],
            'pagination' => false
        ]);

        $this->sort($dataProvider->sort);
        return $dataProvider;
    }

    protected function sort(Sort $sort)
    {

    }

    protected function createSearchQuery()
    {
        $query = static::find();
        $query->alias('c');
        $query->select(['c.*']);
        $query->innerJoin(['pc' => PostComment::tableName()], [
            'and',
            'pc.comment_id = c.id',
            ['pc.post_id' => $this->post->id]
        ]);
        $query->where(['c.status' => [CommentStatus::APPROVED, CommentStatus::PENDING]]);

        return $query;
    }
}
