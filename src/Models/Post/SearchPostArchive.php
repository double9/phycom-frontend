<?php

namespace Phycom\Frontend\Models\Post;


use Phycom\Base\Models\ActiveRecord;
use Phycom\Base\Models\Attributes\PostStatus;
use Phycom\Base\Models\Attributes\PostType;
use Phycom\Base\Models\Post;

use yii\data\Sort;
use yii\db\Expression;

/**
 * Class PostArchive
 * @package Phycom\Frontend\Models\Post
 */
class SearchPostArchive extends ActiveRecord
{
    public $monthNumber;

    public $year;

    public static function tableName()
    {
        return Post::tableName();
    }

    public function getDateTime()
    {
        return \DateTime::createFromFormat('Yn', $this->year . $this->monthNumber);
    }

    public function getMonth($format = 'F')
    {
        return $this->getDateTime()->format($format);
    }

    public function getRoute()
    {
        return ['post/archive', 'year' => $this->year, 'month' => $this->getMonth('m')];
    }

    public function getLabel()
    {
        return $this->getMonth();
    }

    /**
     * @return PostArchiveDataProvider
     */
    public function search()
    {
        $dataProvider = new PostArchiveDataProvider([
            'query' => $this->createSearchQuery(),
            'sort' => ['defaultOrder' => ['published_at' => SORT_DESC]],
            'pagination' => $this->pagination(),
        ]);

        $this->sort($dataProvider->sort);
        return $dataProvider;
    }

    protected function pagination()
    {
        return ['pageSize' => 100];
    }

    protected function sort(Sort $sort)
    {
        $sort->attributes['published_at'] = [
            'asc' => [new Expression("date_trunc('month', p.published_at) ASC")],
            'desc' => [new Expression("date_trunc('month', p.published_at) DESC")],
        ];
    }

    protected function createSearchQuery()
    {
        return static::find()
            ->select([
                'date_part(\'month\', p.published_at) AS "monthNumber"',
                'date_part(\'year\', p.published_at) AS "year"',
            ])
            ->from(['p' => Post::tableName()])
            ->where(['p.type' => PostType::POST])
            ->andWhere(['p.status' => PostStatus::PUBLISHED])
            ->andWhere('p.published_at IS NOT NULL')
            ->groupBy([
                'monthNumber',
                'year',
                'date_trunc(\'month\', p.published_at)'
            ])
            ->orderBy([
                new Expression("date_trunc('month', p.published_at) DESC"),
            ]);
    }
}
