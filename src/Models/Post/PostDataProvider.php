<?php

namespace Phycom\Frontend\Models\Post;

use yii\data\ActiveDataProvider;

/**
 * Class PostDataProvider
 * @package Phycom\Frontend\Models\Post
 *
 * @property SearchPost[] $models
 * @method SearchPost[] getModels()
 */
class PostDataProvider extends ActiveDataProvider
{

}
