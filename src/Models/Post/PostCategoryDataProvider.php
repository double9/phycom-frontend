<?php

namespace Phycom\Frontend\Models\Post;

use yii\data\ActiveDataProvider;

/**
 * Class PostCategoryDataProvider
 * @package Phycom\Frontend\Models\Post
 *
 * @property SearchPostCategory[] $models
 * @method SearchPostCategory[] getModels()
 */
class PostCategoryDataProvider extends ActiveDataProvider
{

}
