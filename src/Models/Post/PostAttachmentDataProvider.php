<?php

namespace Phycom\Frontend\Models\Post;

use Phycom\Base\Models\Post;
use Phycom\Base\Models\PostAttachment;

use yii\data\ActiveDataProvider;

/**
 * Class ArchiveDataProvider
 * @package Phycom\Frontend\Models\Post
 *
 * @property PostAttachment[] $models
 * @method PostAttachment[] getModels()
 */
class PostAttachmentDataProvider extends ActiveDataProvider
{
    public static function create(Post $post)
    {
        return new static([
            'query'      => $post->getAttachments(),
            'sort'       => ['defaultOrder' => ['order' => SORT_ASC]],
            'pagination' => false
        ]);
    }
}
