<?php

namespace Phycom\Frontend\Models\Post;

use yii\data\ActiveDataProvider;

/**
 * Class PostCommentDataProvider
 * @package Phycom\Frontend\Models\Post
 *
 * @property SearchPostComment[] $models
 * @method SearchPostComment[] getModels()
 */
class PostCommentDataProvider extends ActiveDataProvider
{
    /**
     * @return SearchPostComment[]
     */
    public function getTree()
    {
        return $this->buildModelTree($this->getModels(), null);
    }

    /**
     * @param SearchPostComment[] $models
     * @param null $parent
     * @return SearchPostComment[]
     */
    protected function buildModelTree(array $models, $parent = null)
    {
        $tree = [];
        foreach ($models as $model) {
            if ($model->parent_id === $parent) {
                $model->children = $this->buildModelTree($models, $model->id);
                $tree[] = $model;
            }
        }
        return $tree;
    }
}
