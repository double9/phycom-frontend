<?php

namespace Phycom\Frontend\Models;


use Phycom\Base\Models\Attributes\ContactAttributeStatus;
use Phycom\Base\Models\Email;
use Phycom\Base\Models\Phone;
use Phycom\Base\Models\Traits\ModelTrait;
use Phycom\Base\Models\User;
use Phycom\Base\Validators\PhoneInputValidator;

use yii\base\Model;
use Yii;

/**
 * Class UserProfileForm
 * @package Phycom\Frontend\Models
 */
class UserProfileForm extends Model
{
    use ModelTrait;

    public $firstName;
    public $lastName;
    public $companyName;
    public $email;
    public $phone;
    public $phoneNumber;
    public $advertise;

    protected $user;

    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->populateValues();
    }

    public function rules()
    {
        return [
            [['firstName', 'lastName', 'companyName', 'email', 'phone'], 'trim'],
            [['firstName', 'lastName', 'companyName', 'email', 'phone'], 'string', 'max' => 255],
            [['firstName', 'lastName', 'email'], 'required'],
            [['email'], 'email'],
            [['advertise'], 'boolean'],
            [['phoneNumber'], 'required', 'when' => function () {
                    return isset($this->phone) && strlen($this->phone);
                }
            ],
            ['phone', 'trim'],
            ['phone', PhoneInputValidator::class],

        ];
    }

    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('phycom/frontend/main', 'First Name'),
            'lastName' => Yii::t('phycom/frontend/main', 'Last Name'),
            'companyName' => Yii::t('phycom/frontend/main', 'Company name'),
            'email' => Yii::t('phycom/frontend/main', 'Email'),
            'phone' => Yii::t('phycom/frontend/main', 'Phone'),
            'advertise' => Yii::t('phycom/frontend/main', 'I wish to receive special offers')
        ];
    }

    public function populateValues()
    {
        $this->firstName = $this->user->first_name;
        $this->lastName = $this->user->last_name;
        $this->companyName = $this->user->company_name;
        $this->email = (string) $this->user->email;
        $this->advertise = $this->user->settings->advertise ? '1' : '0';
        if ($this->user->phone) {
            $this->phone = $this->user->phone->phone_nr;
            $this->phoneNumber = $this->user->phone->fullNumber;
        }
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws yii\db\Exception
     */
    public function update()
    {
        if ($this->validate()) {

            $transaction = Yii::$app->db->beginTransaction();
            try {

                $this->user->first_name = $this->firstName;
                $this->user->last_name = $this->lastName;
                $this->user->company_name = $this->companyName;
                $this->user->settings->advertise = (bool) $this->advertise;

                if (!$this->user->update()) {
                    return $this->rollback($transaction, $this->user->errors);
                }

                if ($email = $this->user->hasEmail($this->email)) {
                    $email->updated_at = new \DateTime();
                } else {
                    $email = new Email();
                    $email->user_id = $this->user->id;
                    $email->email = $this->email;
                    $email->status = ContactAttributeStatus::UNVERIFIED;
                }

                if (!$email->save()) {
                    return $this->rollback($transaction, $email->errors);
                }

                if ($this->phone) {
                    if ($phone = $this->user->hasPhone($this->phoneNumber)) {
                        $phone->updated_at = new \DateTime();
                    } else {
                        $phone = Phone::create($this->phoneNumber);
                        $phone->user_id = $this->user->id;
                        $phone->status = ContactAttributeStatus::UNVERIFIED;
                    }
                    if (!$phone->save()) {
                        return $this->rollback($transaction, $phone->errors);
                    }
                }

                $transaction->commit();
                return true;

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return false;
    }

}
