<?php
$params = array_merge(
    require(PHYCOM_PATH . '/base/config/params.php'),
    require(__DIR__ . '/params.php'),
    require(ROOT_PATH . '/common/config/params.php'),
    require(ROOT_PATH . '/frontend/config/params.php')
);

return [
    'id'        => 'app-frontend',
    'aliases' => [
        '@Phycom/Frontend' => '@phycom/frontend/src'
    ],
    'basePath'  => ROOT_PATH . '/frontend',
    'bootstrap' => ['log', 'visitorAccess'],
    'controllerNamespace' => 'Frontend\Controllers',
    'container' => [
        'definitions' => [
            'alert' => \yii\bootstrap4\Alert::class
        ]
    ],
    'components' => [
        'modelFactory' => [
            'class' => \Phycom\Frontend\Components\ModelFactory::class
        ],
        'request' => [
            'csrfParam' => '_csrf_phycom_frontend',
        ],
        'user' => [
            'class'           => \Phycom\Base\Components\User::class,
            'identityClass'   => \Phycom\Base\Models\User::class,
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity_phycom_frontend'],
        ],
        'session' => [
            'name'         => 'phycom-frontend',            // this is the name of the session cookie used for login on the frontend
            'cookieParams' => ['lifetime' => 7 * 86400],    // one week lifetime
            'timeout'      => 7 * 86400,
        ],
        'geoip' => [
            'class' => \Phycom\Frontend\Modules\GeoIP\Components\GeoIP::class
        ],
        'visitorAccess' => [
            'class'   => \Phycom\Frontend\Components\VisitorAccess::class,
            'enabled' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => require(__DIR__ . '/url-manager.php'),
	    'cart' => [
            'class'   => \Phycom\Frontend\Components\Cart::class,
            'enabled' => true,
            'on eventAfterClear' => function ($e) {
                if (Yii::$app->session->get('order')) {
                    Yii::$app->session->remove('order');
                }
                if (Yii::$app->session->get('delivery_method')) {
                    Yii::$app->session->remove('delivery_method');
                }
                if (Yii::$app->session->get('delivery_area')) {
                    Yii::$app->session->remove('delivery_area');
                }
                if (Yii::$app->session->get('delivery_date')) {
                    Yii::$app->session->remove('delivery_date');
                }
                if (Yii::$app->session->get('delivery_time')) {
                    Yii::$app->session->remove('delivery_time');
                }
            },
		    'storageProvider' => \Phycom\Base\Models\Cart\Storage\SessionStorage::class
	    ],
        'wishlist' => [
            'class'        => \Phycom\Frontend\Components\Wishlist::class,
            'enabled'      => true,
            'storageProvider' => [
                'class' => \Phycom\Base\Models\Cart\Storage\SessionStorage::class,
                'key'   => 'wishlist'
            ]
        ],
        'deviceDetect' => [
            'class' => \alexandernst\devicedetect\DeviceDetect::class
        ],
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@translations',
                    'catalog'  => 'frontend'
                ],
                'phycom/frontend*' => [
                    'class'    => \Phycom\Base\Components\MessageSource::class,
                    'basePath' => '@phycom/frontend/translations',
                    'catalog'  => 'frontend'
                ]
            ]
        ]
    ],
    'params' => $params,
];
