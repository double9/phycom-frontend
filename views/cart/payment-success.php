<?php

/**
 * @var $this yii\web\View
 * @var $message string
 */

use yii\helpers\Url;

?>

<div id="payment-return" class="bb-mainpanel">

    <div class="container pd-b-15 pd-x-0">

        <div class="card payment-success">
            <div class="ccc card-body tx-size-sm">
                <div class="icon tx-success">
                    <ion-icon name="checkmark-circle-outline"></ion-icon>
                </div>
                <h4 class="tx-size-xs-40 tx-semibold tx-success"><?= Yii::t('phycom/frontend/main', 'Congratulations') ?></h4>
                <h6 class="tx-size-xs-24 tx-normal"><?= Yii::t('phycom/frontend/main', 'Your order has been placed!') ?></h6>
                <p class="mg-b-30 wd-lg-500 mg-lg-x-auto"><?= $message ?></p>
                <a href="<?= Url::toRoute(['product/index']) ?>" class="btn btn-success"><?= Yii::t('phycom/frontend/main', 'Back to Shopping') ?></a>
            </div><!-- card-body -->
        </div><!-- card -->

    </div>

</div>
