<?php
/**
 * @var $this yii\web\View
 */

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Html;

?>

<?php if (Yii::$app->user->isGuest): ?>
    <div class="check-login">

        <h5><?= Yii::t('phycom/frontend/main', 'Do you have an account?'); ?></h5>

        <?= Html::a(
            FAS::i(FAS::_SIGN_IN_ALT, ['class' => 'mg-r-10']) . Yii::t('phycom/frontend/main', 'Login now'),
            ['/site/login'],
            ['class' => 'btn btn-lg btn-outline-success tx-medium']
        );
        ?>

        <p class="small"><?= Yii::t('phycom/frontend/main', 'Or continue without registration by filling out the form'); ?></p>

    </div>
<?php endif; ?>


