<?php
/**
 * @var $this yii\web\View
 */
use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use rmrevin\yii\fontawesome\FAS;

$order = Yii::$app->session->get('order') ? Yii::$app->modelFactory->getOrder()::findOne(Yii::$app->session->get('order')) : null;
$model = Yii::$app->modelFactory->getOrderForm(Yii::$app->user->identity, $order);

?>

<h4 class="checkout-section-title">
    <?= FAS::i(FAS::_INFO_CIRCLE, ['class' => 'mg-r-10']) . Yii::t('phycom/frontend/main', 'Order details'); ?>
</h4>
<hr class="checkout-section-title-line">

<div class="system-messages"></div>

<?php $form = ActiveForm::begin(['id' => 'order-form']); ?>

<?php if (Yii::$app->user->isGuest): ?>

    <div class="row">
        <div class="col-md-8">

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'register')->customCheckbox(); ?>
                </div>
            </div>

            <div class="toggle-form" <?= ($model->sameAsRecipient ? 'style="display: none;"' : '') ?>>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'firstName', ['options' => ['class' => 'required form-group']])->textInput(); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'lastName', ['options' => ['class' => 'required form-group']])->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'companyName')->textInput(); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email', ['options' => ['class' => 'required form-group']])->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->phoneInput2(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<div class="row">
    <div class="col-md-8">
	    <?= $form->field($model, 'comment')->textarea(['rows' => 4]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div class="row">
    <div class="col-md-12 clearfix">
        <?= $this->render('/partials/terms', ['showCheckbox' => true]) ?>
    </div>
</div>

<hr style="margin-top: 5px;">

