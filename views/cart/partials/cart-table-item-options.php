<?php
/**
 * @var $this yii\web\View
 * @var $options array
 */
?>

<table class="table table-sm table-borderless order-item-options">
    <colgroup>
        <col width="20">
        <col>
    </colgroup>
    <tbody>
    <?php foreach ($options as $variantName => $value):
        $variant = Yii::$app->modelFactory->getVariant()::findByName($variantName);
        $option = $variant->getOptionByKey($value);
    ?>
        <tr>
            <td>
                <span class="badge badge-secondary"><?= $variant->label ?></span>
            </td>
            <td>
                <span class="value mg-l-5"><?= $option ? $option->label : $value ?></span>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
