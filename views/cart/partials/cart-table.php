<?php
/* @var $this yii\web\View */

use Phycom\Base\Helpers\f;
use Phycom\Base\Helpers\Currency;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Html;
use yii\helpers\Url;

?>

<table id="cart-table" class="cart-table" data-remove-url="<?= Url::toRoute(['/cart/remove'])?>" data-update-url="<?= Url::toRoute(['/cart/update'])?>">
    <colgroup>
        <col style="width: 150px;">
        <col >
        <col style="width: 350px;">
        <col style="width: 150px;">
        <col style="width: 150px;">
        <col style="width: 50px;">
    </colgroup>
    <thead>
        <tr>
            <th class="hidden-xs"><?= Yii::t('phycom/frontend/main', 'Code') ?></th>
            <th><?= Yii::t('phycom/frontend/main', 'Photo') ?></th>
            <th><?= Yii::t('phycom/frontend/main', 'Name') ?></th>
            <th><?= Yii::t('phycom/frontend/main', 'Qty') ?></th>
            <th><?= Yii::t('phycom/frontend/main', 'Total') ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>


    <?php foreach (Yii::$app->cart->getProductItems() as $item): ?>

        <?= Html::beginTag('tr', [
            'data-id' => $item->getUniqueId(),
            'data-type' => 'product',
            'data-code' => $item->getCode(),
            'data-quantity' => $item->getQuantity(),
            'data-price' => Currency::toDecimal($item->getPrice())
        ])
        ?>


        <td class="code"><?= f::text($item->getCode()); ?></td>
        <td class="photo">

        </td>
        <td class="title">
            <strong><?= f::text($item->getLabel()); ?></strong>
        </td>
        <td>
            <input type="number" class="form-control form-control-sm qty" style="max-width: 60px;" value="<?= f::integer($item->getQuantity()); ?>" step="1" min="1" max="99" />
        </td>
        <td class="price"><?= f::currency($item->getTotalPrice()); ?></td>
        <td class="clearfix">
            <button class="remove-row btn btn-secondary btn-sm btn-flat pull-right">
                <?= FAS::i(FAS::_TIMES); ?>
            </button>
        </td>


        <?= Html::endTag('tr') ?>
    <?php endforeach; ?>


    <?php foreach (Yii::$app->cart->getDiscountItems() as $item): ?>
        <?= Html::beginTag('tr', [
            'data-id' => $item->getUniqueId(),
            'data-type' => 'discount',
            'data-code' => $item->getCode(),
            'data-quantity' => 1,
            'data-price' => Currency::toDecimal($item->getPrice())
        ])
        ?>

        <td class="code"><?= f::text($item->getCode()); ?></td>
        <td class="photo"></td>
        <td class="title" colspan="2">
            <strong><?= f::text($item->getLabel()); ?></strong>
        </td>
        <td class="price"><?= f::currency($item->getPrice()); ?></td>
        <td class="clearfix">
            <button class="remove-row btn btn-secondary btn-sm btn-flat pull-right">
                <?= FAS::i(FAS::_TIMES); ?>
            </button>
        </td>

        <?= Html::endTag('tr') ?>
    <?php endforeach; ?>


    <?php foreach (Yii::$app->cart->getDeliveryItems() as $item): ?>
        <?= Html::beginTag('tr', [
            'data-id' => $item->getUniqueId(),
            'data-type' => 'delivery',
            'data-code' => $item->getCode(),
            'data-quantity' => 1,
            'data-price' => Currency::toDecimal($item->getPrice())
        ])
        ?>

        <td class="code"><?= f::text($item->getCode()); ?></td>
        <td class="photo"></td>
        <td class="title" colspan="2">
            <strong><?= f::text($item->getLabel()); ?></strong>
        </td>
        <td class="price" colspan="2" ><?= f::currency($item->getPrice()); ?></td>

        <?= Html::endTag('tr') ?>
    <?php endforeach; ?>


    </tbody>
    <tfoot>
        <tr>
            <td class="hidden-xs"></td>
            <td colspan="2"></td>
            <td><strong><?= Yii::t('phycom/frontend/main', 'Total'); ?></strong></td>
            <td colspan="2" style="padding: 4px 8px;"><span class="grand-total"><?= f::currency(Yii::$app->cart->getTotal()); ?></span></td>
        </tr>
    </tfoot>
</table>
