<?php

/**
 * @var $this yii\web\View
 * @var $message string
 */

use yii\helpers\Url;

?>

<div id="payment-return" class="bb-mainpanel">

    <div class="container pd-b-15 pd-x-0">

        <div class="card payment-failed">
            <div class="card-body tx-size-sm">
                <div class="icon tx-danger">
                    <ion-icon name="close-circle-outline"></ion-icon>
                </div>
                <h4 class="tx-size-xs-40 tx-semibold tx-danger"><?= Yii::t('phycom/frontend/main', 'Transaction Failed') ?></h4>
                <h6 class="tx-size-xs-24 tx-normal tx-gray-dark"><?= Yii::t('phycom/frontend/main', 'Your order was not placed!') ?></h6>
                <p class="mg-b-30 wd-lg-500 mg-lg-x-auto"><?= $message ?></p>
                <a href="<?= Url::toRoute(['cart/checkout']) ?>" class="btn btn-primary"><?= Yii::t('phycom/frontend/main', 'Back to Checkout') ?></a>
            </div><!-- card-body -->
        </div><!-- card -->

    </div>

</div>
