<?php

/* @var $this yii\web\View */


use frontend\assets\DatePickerAsset;

use Phycom\Frontend\Assets\CheckoutAsset;
use Phycom\Frontend\Assets\SpinnerAsset;

use Phycom\Base\Modules\Delivery\Module as DeliveryModule;
use Phycom\Base\Modules\Delivery\Widgets\MultiShippingBox;

use rmrevin\yii\fontawesome\FAS;
use rmrevin\yii\fontawesome\FAR;

use yii\helpers\Url;

$this->title = Yii::t('phycom/frontend/main', 'Checkout');
$this->params['progress'][] = $this->title;

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => Yii::t('phycom/frontend/seo/checkout', 'orders,checkout,make payment,shipping,promotion codes')
]);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => Yii::t('phycom/frontend/seo/checkout', 'Checkout your order')
]);
/**
 * @var DeliveryModule $delivery
 */
$delivery = Yii::$app->getModule('delivery');
$shippingBoxConfig = $delivery->getShippingBoxConfig([
    'loadStyles'      => false,
    'activeClassLink' => true
]);

CheckoutAsset::register($this);
SpinnerAsset::register($this);

$this->registerJs("cart.init('#cart-checkout');");

?>

<div id="cart-checkout" class="cart-index bb-mainpanel" data-submit-url="<?= Url::toRoute(['/cart/submit-order']) ?>" data-loading-msg="<?= Yii::t('phycom/frontend/main', 'Submitting your order, Please hold on...'); ?>">


    <div class="row">
        <div class="col-md-12">
            <h3 class="tx-inverse tx-size-40 tx-bold mg-b-20 mg-t-20"><?= Yii::t('phycom/frontend/main', 'Checkout'); ?></h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $this->render('partials/cart-table'); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 clearfix">
            <?= $this->render('/partials/promotion-code-form') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 clearfix">
            <?= $this->render('partials/check-login') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 clearfix">
            <div class="mg-y-40">

                <h4 class="checkout-section-title mg-b-40">
                    <?= FAS::i(FAS::_TRUCK, ['class' => 'mg-r-10']); ?><?= Yii::t('phycom/frontend/user', 'Delivery'); ?>
                </h4>

                <?= MultiShippingBox::widget($shippingBoxConfig); ?>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 clearfix">
            <?= $this->render('partials/order-details'); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 clearfix">
            <br />
            <br />
            <br />

            <?= $this->render('@Phycom/Base/Modules/Payment/views/methods.php', [
                'message'     => Yii::t('phycom/frontend/payment', 'Confirm your order by clicking one of the payment methods listed below.'),
                'icon'        => FAR::i(FAR::_CREDIT_CARD, ['class' => 'title-icon primary mg-r-10']),
                'titleTag'    => 'h4',
                'submitOrder' => true
            ]);
            ?>
        </div>
    </div>


</div>
