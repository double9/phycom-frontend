<?php

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model Phycom\Frontend\Models\PasswordResetForm */

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use rmrevin\yii\fontawesome\FAS;

use yii\helpers\Html;


$this->title = Yii::t('phycom/frontend/main','Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="reset-password container-wrapper">
    <div class="card">
        <h2><?= Html::encode($this->title) ?></h2>
        <p class="tx-size-sm mg-b-30"><?= Yii::t('phycom/frontend/main','Please choose your new password:'); ?></p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <?= $form->field($model, 'password')->passwordInput([
                'autofocus' => true,
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false)
            ?>
            <div class="form-group">
                <?= Html::submitButton(
                        FAS::i(FAS::_SIGN_IN_ALT, ['class' => 'pull-left mg-r-10']) .
                        Yii::t('phycom/frontend/main','Save'),
                        [
                            'class' => 'btn btn-flat btn-primary'
                        ]
                ) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
