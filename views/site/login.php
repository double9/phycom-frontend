<?php

/* @var $this yii\web\View */
/* @var $form Phycom\Frontend\Widgets\Bootstrap4\ActiveForm */
/* @var $model Phycom\Base\Models\LoginForm */

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use yii\helpers\Url;
use yii\helpers\Html;


$this->title = Yii::t('phycom/frontend/main','Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-page container-wrapper">
    <div class="card">
        <h2><?= Html::encode($this->title) ?></h2>
        <p class="tx-size-sm mg-b-30"><?= Yii::t('phycom/frontend/main','Please fill out the following fields to login:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput([
                'autofocus' => true,
                'placeholder' => $model->getAttributeLabel('username')
            ])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput([
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <p class="tx-size-xs">
                <?= Yii::t('phycom/frontend/main', 'If you forgot your password you can reset it {link}here{/link}', [
                    'link' => '<a href="' . Url::toRoute(['site/request-password-reset']) . '">',
                    '/link' => '</a>'
                ]) ?>
            </p>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('phycom/frontend/main', 'Login'), ['class' => 'btn btn-inverse', 'name' => 'login-button']) ?>
            </div>

            <?php if (($authModule = Yii::$app->getModule('auth')) && !empty($authModule->getMethods())) : ?>

                <p class="tx-size-xs tx-center">
                    <?= Yii::t('phycom/frontend/main', 'Or login with your social media account') ?>
                </p>

                <?php
                    /**
                     * @var \Phycom\Auth\Module $authModule
                     */
                    foreach ($authModule->getMethods() as $method) : ?>

                        <div class="form-group"><?= $method->render() ?></div>

                <?php endforeach; ?>
            <?php endif; ?>

        <?php ActiveForm::end(); ?>

        <p class="tx-size-xs tx-center">
            <?= Yii::t('phycom/frontend/main', 'Not yet a member? {link}Sign Up{/link}', [
                'link' => '<a href="' . Url::toRoute(['site/signup']) . '">',
                '/link' => '</a>'
            ]) ?>
        </p>

    </div><!-- card -->
</div><!-- container-wrapper -->
