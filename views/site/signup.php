<?php

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model Phycom\Frontend\Models\SignupForm */

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use yii\helpers\Url;
use yii\helpers\Html;


$this->title = Yii::t('phycom/frontend/main','Signup');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="signup-page container-wrapper">
    <div class="card">
        <h2><?= Html::encode($this->title) ?></h2>
        <p class="tx-size-sm mg-b-30"><?= Yii::t('phycom/frontend/main','Please fill out the following fields to signup:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>

            <?= $form->field($model, 'firstName')->textInput([
                'autofocus' => true,
                'placeholder' => $model->getAttributeLabel('firstName')
            ])->label(false) ?>

            <?= $form->field($model, 'lastName')->textInput([
                'placeholder' => $model->getAttributeLabel('lastName')
            ])->label(false) ?>

            <?= $form->field($model, 'email')->textInput([
                'type' => 'email',
                'placeholder' => $model->getAttributeLabel('email')
            ])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput([
                'placeholder' => $model->getAttributeLabel('password')
            ])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('phycom/frontend/main', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php if (($authModule = Yii::$app->getModule('auth')) && !empty($authModule->getMethods())) : ?>

                <p class="tx-size-xs tx-center">
                    <?= Yii::t('phycom/frontend/main', 'Or signup with your social media account') ?>
                </p>
                <?php
                /**
                 * @var \Phycom\Auth\Module $authModule
                 */
                foreach ($authModule->getMethods() as $method) : ?>

                    <div class="form-group"><?= $method->render() ?></div>

                <?php endforeach; ?>
            <?php endif; ?>

        <?php ActiveForm::end(); ?>

        <p class="tx-size-xs tx-center">
            <?= Yii::t('phycom/frontend/main', 'Already a member? {link}Login{/link}', [
                'link' => '<a href="' . Url::toRoute(['site/login']) . '">',
                '/link' => '</a>'
            ]) ?>
        </p>

    </div><!-- card -->
</div><!-- container-wrapper -->
