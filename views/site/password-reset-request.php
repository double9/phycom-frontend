<?php

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model Phycom\Frontend\Models\PasswordResetRequestForm */

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use rmrevin\yii\fontawesome\FAR;

use yii\helpers\Html;


$this->title = Yii::t('phycom/frontend/main','Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="request-password-reset container-wrapper">
    <div class="card">
        <h2><?= Html::encode($this->title) ?></h2>
        <p class="tx-size-sm mg-b-30"><?= Yii::t('phycom/frontend/main','Please fill out your email. A link to reset password will be sent there.') ?></p>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <?= $form->field($model, 'email')->textInput([
                    'autofocus' => true,
                    'placeholder' => $model->getAttributeLabel('email')
                ])->label(false)
            ?>
            <div class="form-group">
                <?= Html::submitButton(
                        FAR::i(FAR::_ENVELOPE, ['class' => 'pull-left mg-r-10']) .
                        Yii::t('phycom/frontend/main','Send'),
                        [
                            'class' => 'btn btn-flat btn-lg btn-primary'
                        ]
                ) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
