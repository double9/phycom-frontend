<?php

use Phycom\Frontend\Widgets\LinkPager;
use Phycom\Frontend\Widgets\ProductPrice;
use Phycom\Frontend\Helpers\ProductHelper;
use Phycom\Frontend\Helpers\PostHelper;

use yii2mod\rating\StarRating;

/* @var $this yii\web\View */


$productDataProvider = ProductHelper::featured(24);
$this->title = Yii::$app->name;
?>
<div id="index-page" class="site-index">

    <?php if ($landingPage = PostHelper::landingPage()): ?>
        <?= $this->render('/partials/hero', ['post' => $landingPage]) ?>
    <?php endif; ?>

    <?= $this->render('/partials/campaign') ?>


    <div class="featured py-5 bg-light">
        <div class="row">
            <div class="col">
                <h4 class="tx-bold mb-4 tx-center">Latest products</h4>
            </div>
        </div>
        <div class="row">

            <?php foreach ($productDataProvider->getModels() as $product): ?>

                <div class="col-md-4 col-lg-3 d-flex align-items-stretch">
                    <div class="shop-item card mb-4 shadow-sm">

                        <figure class="effect-fade">
                            <img class="bd-placeholder-img card-img-top" src="<?= $product->getImageUrl() ?>" width="100%" height="225" alt="<?= $product->translation->title ?>"/>
                            <figcaption class="overlay-block d-flex justify-content-center align-items-center">
                                <?= Yii::$app->cart->button($product) ?>
                                <?= Yii::$app->wishlist->button($product) ?>
                            </figcaption>
                        </figure>

                        <div class="card-body">

                            <h6 class="card-title">
                                <a href="" class="tx-inverse hover-pink"><?= $product->title ?></a>
                            </h6>

                            <?= ProductPrice::widget(['product' => $product]) ?>

                            <div class="card-content">

                                <p class="card-text"><?= $product->translation->outline ?></p>

                                <?= StarRating::widget([
                                    'name'          => 'product-' . $product->id . '-score',
                                    'value'         => $product->score,
                                    'options'       => ['class' => 'd-inline-block tx-yellow tx-size-8 mg-b-0'],
                                    'clientOptions' => ['readOnly' => true, 'starType' => 'i'],
                                ]);
                                ?>

                                <small class="review-count text-muted">(<?= $product->reviewCount ?>)</small>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>
        <div class="row">
            <div class="col">
                <?= LinkPager::create($productDataProvider->pagination, [], ['options' => ['class' => 'pagination pagination-inverse']]) ?>
            </div>
        </div>
    </div>


    <?= $this->render('/partials/campaign') ?>

</div>
