<?php

/**
 * @var \yii\web\View $this
 * @var string $title
 * @var string $message
 */
use yii\helpers\Html;

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <style>
        html, body {
            text-align: center;
            height: 100%;
        }
        .container {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }
        .center {
            flex: 1;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="center">
            <h2><?= $title; ?></h2>
            <p><?= $message ?></p>
        </div>
    </div>
</body>
</html>
