<?php

/**
 * @var \yii\web\View $this
 */
use Phycom\Frontend\Helpers\ProductCategoryHelper;

use yii\helpers\Url;
?>

<footer class="footer bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md">

                <small class="pull-left text-muted mg-b-20">&copy; <?= Yii::$app->name; ?> <?= date('Y') ?></small>
            </div>
            <div class="col-6 col-md">
                <h5><?= Yii::t('phycom/frontend/footer', 'Featured') ?></h5>
                <ul class="list-unstyled text-small">
                    <?php foreach (ProductCategoryHelper::featured(8)->getModels() as $category): ?>
                        <li><a class="text-muted" href="<?= $category->getUrl() ?>"><?= $category->title ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5><?= Yii::t('phycom/frontend/footer', 'Resources') ?></h5>
                <ul class="list-unstyled text-small">
                    <?php if (Yii::$app->blog->isEnabled()): ?>
                        <li><a class="text-muted" href="<?= Url::toRoute(['/post/index']) ?>"><?= Yii::t('phycom/frontend/footer', 'Blog') ?></a></li>
                    <?php endif; ?>
                    <?php if ($careers = Yii::$app->pages->getPage('careers')): ?>
                        <li><a class="text-muted" href="<?= $careers->url ?>"><?= $careers->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($events = Yii::$app->pages->getPage('events')): ?>
                        <li><a class="text-muted" href="<?= $events->url ?>"><?= $events->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($partners = Yii::$app->pages->getPage('partners')): ?>
                        <li><a class="text-muted" href="<?= $partners->url ?>"><?= $partners->title ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5><?= Yii::t('phycom/frontend/footer', 'Resources') ?></h5>
                <ul class="list-unstyled text-small">
                    <?php if ($faq = Yii::$app->pages->getPage('faq')): ?>
                        <li><a class="text-muted" href="<?= $faq->url ?>"><?= $faq->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($paymentInfo = Yii::$app->pages->getPage('payment-info')): ?>
                        <li><a class="text-muted" href="<?= $paymentInfo->url ?>"><?= $paymentInfo->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($guaranteeInfo = Yii::$app->pages->getPage('guarantee-info')): ?>
                        <li><a class="text-muted" href="<?= $guaranteeInfo->url ?>"><?= $guaranteeInfo->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($deliveryInfo = Yii::$app->pages->getPage('delivery-info')): ?>
                        <li><a class="text-muted" href="<?= $deliveryInfo->url ?>"><?= $deliveryInfo->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($orderInfo = Yii::$app->pages->getPage('order-info')): ?>
                        <li><a class="text-muted" href="<?= $orderInfo->url ?>"><?= $orderInfo->title ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-6 col-md">
                <h5><?= Yii::t('phycom/frontend/footer', 'About') ?></h5>
                <ul class="list-unstyled text-small">
                    <?php if ($team = Yii::$app->pages->getPage('team')): ?>
                        <li><a class="text-muted" href="<?= $team->url ?>"><?= $team->title ?></a></li>
                    <?php endif; ?>
                    <?php if (Yii::$app->commerce->isEnabled() && Yii::$app->commerce->shop->isEnabled()): ?>
                        <li><a class="text-muted" href="<?= Url::toRoute('/shops') ?>"><?= Yii::t('phycom/frontend/footer', 'Locations') ?></a></li>
                    <?php endif; ?>
                    <?php if ($privacy = Yii::$app->pages->getPage('privacy-policy')): ?>
                        <li><a class="text-muted" href="<?= $privacy->url ?>"><?= $privacy->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($terms = Yii::$app->pages->getPage('terms')): ?>
                        <li><a class="text-muted" href="<?= $terms->url ?>"><?= $terms->title ?></a></li>
                    <?php endif; ?>
                    <?php if ($contact = Yii::$app->pages->getPage('contact')): ?>
                        <li><a class="text-muted" href="<?= $contact->url ?>"><?= $contact->title ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>
