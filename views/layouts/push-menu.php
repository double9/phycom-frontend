<?php

use Phycom\Frontend\Assets\DefaultAsset;
use Phycom\Frontend\Assets\PushMenuAsset;
use Phycom\Frontend\Widgets\Bootstrap4\Nav;

use yii\helpers\Html;

PushMenuAsset::register($this);

$menuItems = [];

if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->displayName . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}

$menuItems[] = Yii::$app->wishlist->widget(['options' => ['class' => 'nav-link']]);
$menuItems[] = Yii::$app->cart->widget(['options'=> ['class' => 'nav-link']]);


$defaultAsset = DefaultAsset::register($this);

?>

<div class="head-panel fixed-top light navbar-light">
    <div class="container">

        <a class="brand" href="<?= Yii::$app->homeUrl ?>">
            <img src="<?= $defaultAsset->baseUrl . '/logo-10.svg' ?>" class="ht-100" />
        </a>

        <!-- menu for mobile, hidden in desktop -->
        <a href="" class="navbar-toggler">
            <span class="navbar-toggler-icon"></span>
        </a>

        <div class="menu-right">

            <!-- logo for mobile display only -->
            <div class="menu-head">
                <h4 class="tx-bold tx-spacing-neg-1 tx-size-18"><?= Yii::$app->name ?></h4>
            </div>

            <?= Nav::widget([
                    'options' => [
                        'class' => 'nav nav-gray tx-size-12 tx-uppercase tx-medium tx-spacing-2 flex-column flex-lg-row'
                    ],
                    'items' => $menuItems,
                ]);
            ?>
        </div>

    </div>
</div>
