<?php

/* @var $this \yii\web\View */
/* @var $content string */

use Phycom\Frontend\Assets\DefaultAsset;
use Phycom\Frontend\Widgets\Bootstrap4\FlashAlert;

use Phycom\Base\Helpers\c;

use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Json;
use yii\helpers\Html;

$messages = Json::encode([
    'error' => Yii::t('phycom/frontend/main', 'Error'),
]);

DefaultAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        var baseUrl = '<?= Yii::$app->urlManager->baseUrl; ?>';
        var i18n = JSON.parse('<?= $messages ?>');
    </script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap bg-light">
    <?= $this->render(c::param('header')) ?>
    <div class="container pb-0 main-panel">
        <div class="row-full bg-gray">
            <div class="container">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
            </div>
        </div>
        <div class="main-alert">
            <?= FlashAlert::widget() ?>
        </div>
        <?= $content ?>
    </div>
    <?= $this->render('footer'); ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
