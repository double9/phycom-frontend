<?php

/**
 * @var \yii\web\View $this
 */

use Phycom\Frontend\Widgets\Bootstrap4\NavBar;
use Phycom\Frontend\Widgets\Bootstrap4\Nav;

use yii\helpers\Html;

$menuItems = [];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}

$menuItems[] = Yii::$app->wishlist->widget(['options' => ['class' => 'nav-link']]);
$menuItems[] = Yii::$app->cart->widget(['options' => ['class' => 'nav-link']]);



    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-dark bg-dark navbar-expand-lg sticky-top',
        ],
        'containerOptions' => ['class' => 'container']
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto navbar-right tx-size-12 tx-uppercase tx-medium tx-spacing-2'],
        'items' => $menuItems,
    ]);



    //    echo Yii::$app->wishlist->widget([
    //        'options' => ['class' => 'nav-link pd-r-5']
    //    ]);
    //
    //    echo Yii::$app->cart->widget([
    //        'options' => ['class' => 'nav-link pd-r-5']
    //    ]);

    NavBar::end();

