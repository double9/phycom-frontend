<?php

use Phycom\Base\Interfaces\CartItemProductInterface;
/**
 * @var \yii\web\View $this
 * @var CartItemProductInterface[] $items
 */
?>

<div id="wishlist-page" class="wishlist-index">
    <h2><?= Yii::t('phycom/frontend/main', 'Wishlist') ?>:</h2>
    <ul class="wishlist-list">
    <?php foreach ($items as $item): ?>
        <li><?= $item->getLabel() ?></li>
    <?php endforeach; ?>
    </ul>
</div>
