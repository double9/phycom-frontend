<?php
/**
 * @var \yii\web\View $this
 * @var \Phycom\Base\Models\Post $post
 */
?>

<div class="row-full">
    <div id="hero" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <?php foreach ($post->attachments as $n => $postAttachment): ?>
                <li data-target="#hero" data-slide-to="<?= $n ?>" <?= ($n === 0 ? 'class="active"' : '') ?>></li>
            <?php endforeach; ?>
        </ol>

        <div class="carousel-inner">
            <?php foreach ($post->attachments as $n => $postAttachment): ?>
                <div class="carousel-item<?= ($n === 0 ? ' active' : '') ?>">
                    <img class="d-block w-100" src="<?= $postAttachment->file->getUrl() ?>" alt="<?= ucfirst(Yii::t('phycom/frontend/main', '{n,spellout,%spellout-ordinal} slide', ['n' => $n + 1])) ?>">
                </div>
            <?php endforeach; ?>

            <div class="carousel-caption d-none d-md-block">
                <h1><?= $post->translation->title ?></h1>
                <p><?= $post->translation->content ?></p>
                <a href="<?= ($post->translation->meta['link'] ?? '#') ?>" class="btn btn-outline-pink"><?= ($post->translation->meta['linkLabel'] ?? 'Main Call To Action') ?></a>
            </div>

        </div>

        <a class="carousel-control-prev" href="#hero" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only"><?= Yii::t('phycom/frontend/main', 'Previous') ?></span>
        </a>

        <a class="carousel-control-next" href="#hero" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only"><?= Yii::t('phycom/frontend/main', 'Next') ?></span>
        </a>

    </div>
</div>
