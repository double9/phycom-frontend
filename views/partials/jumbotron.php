<?php
/**
 * @var \yii\web\View $this
 * @var \Phycom\Base\Models\Post $post
 */
?>

<section class="jumbotron text-center">
    <h1><?= $post->translation->title ?></h1>
    <div class="lead text-muted"><?= $post->translation->content ?></div>
    <div class="pd-y-20">
        <a href="#" class="btn btn-primary tx-uppercase m-2">Main call to action</a>
        <!--                    <a href="#" class="btn btn-outline-secondary tx-uppercase bd-2 m-2">Secondary action</a>-->
    </div>
</section>
