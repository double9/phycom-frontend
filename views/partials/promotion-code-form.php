<?php

/**
 * @var \yii\web\View $this
 */

use Phycom\Frontend\Models\PromotionCodeForm;
use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;

use Phycom\Base\Helpers\Url;
use Phycom\Base\Helpers\c;

use yii\helpers\Html;


$model = new PromotionCodeForm();
$options = [];

if (count(Yii::$app->cart->getDiscountItems())) {
    $options['style'] = 'display: none;';
}

$this->registerJs("promotion.init('#promotion-code-form');")
?>

<?php if (c::param('promotionCodesEnabled', false)): ?>
    <?php
        $form = ActiveForm::begin([
            'id'      => 'promotion-code-form',
            'action'  => Url::toRoute(['/cart/apply-discount']),
            'options' => $options
        ]);
    ?>

    <div class="form-wrapper clearfix">
        <?= Html::button(Yii::t('phycom/frontend/discount-rule', 'Apply'), ['class' => 'btn btn-primary']); ?>
        <?= $form->field($model, 'code', [
                'template' => "{input}\n{error}\n{hint}",
                'options' => ['class' => ['field-container']]
            ])
            ->textInput(['placeholder' => $model->getAttributeLabel('code')])
            ->label(false);
        ?>
    </div>
    <hr>
    <?php ActiveForm::end(); ?>

<?php endif; ?>
