<?php

/**
 * @var $this yii\web\View
 */

use Phycom\Frontend\Widgets\Bootstrap4\ActiveForm;
use Phycom\Frontend\Models\TermsForm;

use yii\helpers\Html;

$showCheckbox = $showCheckbox ?? false;
/**
 * @var TermsForm|object $model
 */
$model = Yii::createObject(TermsForm::class);


if ($showCheckbox) {

    echo Html::beginTag('div', ['id' => 'terms-text']);
        $form = ActiveForm::begin(['id' => 'terms-form']);
            echo $form->field($model, 'agreeTerms')->customCheckbox();
        ActiveForm::end();
    echo Html::endTag('div');

} else {

    echo Html::beginTag('p', ['id' => 'terms-text']);
        echo Html::tag('div', $model->getTermsText(), ['style' => 'margin-top: 15px;']);
    echo Html::endTag('p');
}
