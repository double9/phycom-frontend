<?php

use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 */
?>
<div class="row-full">
    <div class="campaign-alert container-wrapper bg-gray-lightest pd-y-80">
        <div class="container d-md-flex align-items-center justify-content-between">
            <div class="media d-block d-md-flex align-items-center mg-b-20 mg-md-b-0">
                <i class="fas fa-award tx-size-64 lh-0 tx-inverse mg-x-10"></i>
                <div class="media-body mg-md-l-20 mg-t-10">
                    <h4 class="tx-pink tx-bold mg-b-3"><?= Yii::t('phycom/frontend/campaign', 'Claim your reward points. Up to {discount} off reward deals', ['discount' => '50%']) ?></h4>
                    <p class="tx-inverse mg-b-0"><?= Yii::t('phycom/frontend/campaign', 'Prices drop if you decided to become our member. Limited time only.') ?></p>
                </div>
            </div><!-- media -->

            <a href="<?= Url::toRoute('/site/signup') ?>" class="btn btn-outline-inverse bd-2 pd-y-12 pd-x-30">
                <span class="tx-uppercase tx-bold tx-size-12 tx-spacing-2"><?= Yii::t('phycom/frontend/campaign', 'Become a Member') ?></span>
            </a>
        </div><!-- container -->
    </div>
</div>
