<?php

/**
 * @var string $content
 * @var array $bodyOptions
 */

if (!isset($type)) {
	$type = 'info';
}
if (!isset($size)) {
	$size = 'md';
}
?>

<div id="<?= $id; ?>" class="modal fade <?= $type; ?>">
	<div class="modal-dialog modal-<?= $size; ?>">
		<div class="modal-content">
			<div class="panel panel-<?= $type; ?> no-margin">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong class="modal-title"><?= ($title ?: '&nbsp;'); ?></strong>
				</div>
				<div class="<?= $bodyOptions['class'] ?>">
					<?= $content; ?>
				</div>
			</div>
		</div>
	</div>
</div>