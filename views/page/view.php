<?php

use Phycom\Frontend\Models\Post\SearchPost;
/**
 * @var $this yii\web\View
 * @var $page SearchPost
 */

$this->title = $page->translation->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="page">
    <h2><?= $this->title; ?></h2>
    <?= $page->translation->content; ?>
</div>
