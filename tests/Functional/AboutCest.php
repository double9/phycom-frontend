<?php
namespace Phycom\Frontend\Tests\functional;

use Phycom\Frontend\Tests\FunctionalTester;

class AboutCest
{
    public function checkAbout(FunctionalTester $I)
    {
        $I->amOnRoute('site/about');
        $I->see('About', 'h1');
    }
}
